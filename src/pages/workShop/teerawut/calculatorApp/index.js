import { Layout } from "src/layouts/dashboard/layout";
import { Inter } from 'next/font/google'
import React, { useEffect, useState } from 'react';
import { Button, Container, Grid, Typography, Paper, createTheme, ThemeProvider, Box } from '@mui/material';
import { create, all } from 'mathjs';

const math = create(all);

const inter = Inter({ subsets: ['latin'] })

const evaluateExpression = (expression) => {
    try {
        const result = math.evaluate(expression);
        return result.toString();
    } catch (error) {
        console.error(error);
        return 'Error';
    }
};

const CalculatorButton = ({ label, onClick }) => (
    <Button
        color="inherit"
        className="col-span-1 text-gray-900 p-3"
        onClick={onClick}
    >
        {label}
    </Button>
);

const CalculatorApp = () => {
    const [expression, setExpression] = useState('');
    const [result, setResult] = useState('');

    const handleButtonClick = (value) => {
        if (expression === '' && /[\+\-\*\/]/.test(value)) {
            // If the expression is empty and the input is an operator, do not allow it
            return;
        }

        if (/[\+\-\*\/]$/.test(expression) && value === '.') {
            // If the last character is an operator and the next input is a dot, do not allow it
            return;
        }
    
        if (/[\+\-\*\/]$/.test(expression) && /\d/.test(value)) {
            // If the last character is an operator and the next input is a digit, allow it
            setExpression((prevExpression) => prevExpression + value);
            return;
        }
    
        if (expression === '0' && /[\d.]/.test(value)) {
            // If the expression is '0' and the next input is a digit or dot, replace '0' with the input
            setExpression(value);
            return;
        }

        // Handle percentage
        if (value === '%') {
            const newExpression = (evaluateExpression(expression) / 100).toString();
            setExpression(newExpression);
            setResult(newExpression);
            return;
        }

        // Handle positive/negative toggle
        if (value === '+/-') {
            const newExpression =
                evaluateExpression(expression) !== 0
                    ? (evaluateExpression(expression) * -1).toString()
                    : expression;
            setExpression(newExpression);
            setResult(newExpression);
            return;
        }

        // Handle operator
        if (/[\+\-\*\/]/.test(value)) {
            try {
                const newResult = evaluateExpression(expression);
                setResult(newResult);
                setExpression((prevExpression) => prevExpression + value);
            } catch (error) {
                console.error(error);
                setResult('Error');
            }
            return;
        }

        // Handle '.' button
        if (value === '.') {
            // Check if the last part of the expression already contains a dot
            const lastPart = expression.split(/[\+\-\*\/]/).pop();
            if (!lastPart.includes('.')) {
                setExpression((prevExpression) => prevExpression + value);
            }
            return;
        }

        // Handle other buttons (digits, '.', '00', '=')
        setExpression((prevExpression) => prevExpression + value);
    };

    const handleCalculate = () => {
        // Check if the last character in expression is an operator
        if (/[\+\-\*\/]$/.test(expression)) {
            // Remove the last operator and update expression
            setExpression((prevExpression) => prevExpression.slice(0, -1));
        }
    
        // Calculate the result and update expression and result
        try {
            const newResult = evaluateExpression(expression);
            setResult(newResult);
            setExpression(newResult);
        } catch (error) {
            console.error(error);
            setResult('Error');
        }
    };

    const handleClear = () => {
        setExpression('');
        setResult('');
    };

    const handleBackspace = () => {
        setExpression((prevExpression) => prevExpression.slice(0, -1));
    };

    useEffect(() => {
        const handleKeyPress = (event) => {
            const key = event.key;

            if (/[\d+\-*/%.]/.test(key)) {
                event.preventDefault();
                handleButtonClick(key);
            } else if (key === 'Enter') {
                event.preventDefault();
                handleCalculate();
            } else if (key === 'Backspace') {
                event.preventDefault();
                handleBackspace();
            } else if (key === 'c') {
                handleClear();
            }
        };

        window.addEventListener('keydown', handleKeyPress);

        return () => {
            window.removeEventListener('keydown', handleKeyPress);
        };
    }, [handleButtonClick, handleCalculate, handleBackspace]);

    return (
        <Container maxWidth="xs">
            <Box className="bg-white p-6 py-10 rounded-2xl shadow-md border border-gray-200/60">
                <Box className="text-right mb-2">
                    <Typography variant="caption" color="inherit" className="text-gray-600" fontWeight="normal">
                        {expression}
                    </Typography>
                </Box>
                <Box className="mb-6">
                    <Typography variant="h4" color="inherit" className="text-right text-gray-600" fontWeight="medium">
                        {result || '0'}
                    </Typography>
                </Box>
                <hr className="mb-6"/>
                <div className="grid grid-cols-4 gap-4">
                    <CalculatorButton label="C" onClick={handleClear} />
                    <CalculatorButton label="+/-" onClick={() => handleButtonClick('+/-')} />
                    <CalculatorButton label="%" onClick={() => handleButtonClick('%')} />
                    <CalculatorButton label="/" onClick={() => handleButtonClick('/')} />

                    {[1, 2, 3, '*'].map((value) => (
                        <CalculatorButton
                            key={value}
                            label={value.toString()}
                            onClick={() => handleButtonClick(value.toString())}
                        />
                    ))}
                    {[4, 5, 6, '-'].map((value) => (
                        <CalculatorButton
                            key={value}
                            label={value.toString()}
                            onClick={() => handleButtonClick(value.toString())}
                        />
                    ))}
                    {[7, 8, 9, '+'].map((value) => (
                        <CalculatorButton
                            key={value}
                            label={value.toString()}
                            onClick={() => handleButtonClick(value.toString())}
                        />
                    ))}
                    <CalculatorButton label="00" onClick={() => handleButtonClick('00')} />
                    <CalculatorButton label="0" onClick={() => handleButtonClick('0')} />
                    <CalculatorButton label="." onClick={() => handleButtonClick('.')} />
                    <button color='inherit' onClick={handleCalculate} className='rounded-full h-[3.5rem] w-[3.5rem] shadow-md text-2xl text-white bg-[#5699ff] translate-x-[.6rem]'>
                        <p className="text-3xl -translate-y-[.05em]">=</p>
                    </button>
                </div>
            </Box>
        </Container>
    );
};

CalculatorApp.getLayout = (CalculatorApp) => <Layout>{CalculatorApp}</Layout>;
export default CalculatorApp;