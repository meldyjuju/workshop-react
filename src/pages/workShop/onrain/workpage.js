import { Layout } from "src/layouts/dashboard/layout";
import Work from "src/components/orn/work";

export default function WorkPage(){
    return(
        <Work/>
    )
   
}
WorkPage.getLayout = (WorkPage) => <Layout>{WorkPage}</Layout>;