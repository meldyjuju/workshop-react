# Kunathip workshop 📚

# การใช้ Git 

## Fork project
- ทำการ fork project ใน gitlab มาก่อนที่ https://gitlab.com/H0RosH1/workshop-react

## Clone and connect repo
```cmd
git clone <my-link-to-github-remote-repo> 
cd <เข้าไปที่ folder>
git remote add upstream <tcc-link-to-github-remote-repo> 
```

## สร้าง branch ใหม่
- สร้าง `branch` ใหม่ทุกครั้งก่อนทำการ commit 
```
git branch <Branch-Name> //สร้างBranch ใหม่
git checkout <Branch-Name>//ไปยัง Branch ที่ต้องการ
```
## การใช้ git push
```
git add .
git commit -m "Branch-Name"
git push origin <Branch-Name>
```
- จากนั้นตรวจสอบ `branch` ใน gitlab เลือกbranchที่ pushขึ้นมาและกด create merge requests

## การใช้ git pull 
```
git checkout main //check ไป ที่ main เพื่อทำการ pull code 
git pull upstream main //pull code มาจาก repo ที่เรา remote ไว้ เพื่อ pull code ล่าสุดจาก repo main
```
- `checkout` ไป ที่ main เพื่อทำการ pull code  
- pull code มาจาก repo ที่เรา `remote` ไว้ เพื่อ pull code ล่าสุดจาก repo main

# Main page 👀
## Introduction 🔫
หน้า Profile Page ที่จะเเสดง ข้อความอธิบายเกี่ยวกับเจ้าของหน้าเพจ เเละ มีbutton ที่สามารถกดไปยังหน้า workshop อื่น 

## Import Libraries/Components การทำงาน

## Import ที่ต้องใข้:
```jsx
import { Layout } from "src/layouts/dashboard/layout";
import { styled } from "@mui/system";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Link from "next/link";
import Button from "@mui/material/Button";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";

```
1. `import { Layout } from "src/layouts/dashboard/layout"`;

    - `Layout` คือคอมโพเนนต์ที่ถูกนำเข้ามาจากไฟล์ "`src/layouts/dashboard/layout`"
    - คอมโพเนนต์นี้อาจจะเป็นโครงสร้างหลักหรือโครงร่างของหน้าเว็บทั้งหน้า ที่ให้โครงสร้างของหน้าแรกสำหรับเว็บไซต์ของคุณ

2. `import { styled } from "@mui/system"`;

    - `styled` คือฟังก์ชันที่ใช้สร้าง styled components จาก Material-UI ที่ใช้กำหนดรูปแบบสไตล์ที่กำหนดเอง

3. `import Box from "@mui/material/Box"`;

    - `Box` คือคอมโพเนนต์ที่ให้กล่องที่สามารถใช้ในการจัดวางข้อมูลหรือคอมโพเนนต์อื่น ๆ ได้

4. `import Paper from "@mui/material/Paper"`;

    - `Paper` คือคอมโพเนนต์ที่ให้พื้นที่ของกระดาษที่สามารถใช้แสดงข้อมูลหรือคอมโพเนนต์อื่น ๆ ได้

5. `import Grid from "@mui/material/Grid"`;

    - `Grid` คือคอมโพเนนต์ที่ให้ระบบกริด (grid system) สำหรับจัดวางองค์ประกอบต่าง ๆ ในหน้าเว็บ

6. `import Link from "next/link"`;

    - `Link` คือคอมโพเนนต์ที่ให้การนำทางระหว่างหน้าเว็บใน Next.js

7. `import Button from "@mui/material/Button"`;

    - `Button` คือคอมโพเนนต์ที่ให้ปุ่มที่สามารถกดได้

8. `import TableContainer from "@mui/material/TableContainer"`;

    - `TableContainer` คือคอมโพเนนต์ที่ให้คอนเทนเนอร์สำหรับตารางใน Material-UI

9. `import Table from "@mui/material/Table"`;

    - `Table `คือคอมโพเนนต์ที่ให้ตารางใน Material-UI

10. `import TableBody from "@mui/material/TableBody"`;

    - `TableBody` คือคอมโพเนนต์ที่ให้ส่วนของข้อมูลในตาราง

11. `import TableRow from "@mui/material/TableRow"`;

    - `TableRow` คือคอมโพเนนต์ที่ให้แถวในตาราง

12. `import TableCell from "@mui/material/TableCell"`;

    - `TableCell` คือคอมโพเนนต์ที่ให้เซลล์ในตาราง

13. `import React, { useState } from "react"`;

    - `React` คือไลบรารี React ที่ให้คอมโพเนนต์และฟังก์ชันของ React
    - `useState` คือฮุกที่ใช้ในการจัดการสถานะของคอมโพเนนต์

## Styled Component (Item)
```jsx
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
```
- Item เป็น styled component ที่ใช้ Material-UI's styled utility
- ทำการ styling ให้กับ Paper component โดยมี dynamic background color ขึ้นอยู่กับ mode ของ theme
- มี styling properties อื่น ๆ ที่ถูก spread มาจาก theme.typography ซึ่งรวมถึง padding, text alignment, และ text color

## Functional Component 

```jsx
const kunathip = () => {
  // วัตถุสำหรับ styling ของ elements ต่าง ๆ

  return (
    <>
      {/* JSX สำหรับสร้างหน้าโปรไฟล์ */}
    </>
  );
};
```
- kunathip เป็น functional component ที่ render หน้าโปรไฟล์
- มี styling objects สำหรับ elements ต่าง ๆ เช่น fontSize1, fontSize2, ฯลฯ

## JSX Structure

```jsx
<Box sx={{ flexGrow: 1, backgroundColor: "#303841", padding: "1em" }}>
  <Grid container spacing={2}>
    {/* ซ้ายมือพร้อมข้อมูล text */}
    <Grid item xs={12} sm={5} sx={{ color: "white", marginTop: "10em", marginLeft: "2em" }}>
      {/* ข้อมูล text และลิงก์ไปที่ workshops ต่าง ๆ */}
    </Grid>
    {/* ขวามือพร้อมรูปโปรไฟล์ */}
    <Grid item xs={12} sm={5} sx={{ marginTop: { xs: "2em", sm: "200px" }, alignItems: "right", marginLeft: "auto" }}>
      <img src="/imageknt/Profile.jpg" alt="Profile" style={imageProfile} />
    </Grid>
  </Grid>
</Box>

```
- Layout ของหน้าเว็บใช้ Material-UI's` Box` และ` Grid` components เพื่อสร้างโครงสร้างที่ response ขึ้นอยู่กับขนาดของหน้าจอเป็น 2 คอลัมน์
- คอลัมน์ทางซ้ายมีข้อมูล text เกี่ยวกับผู้ใช้และลิงก์ไปยัง workshops ต่าง ๆ
- คอลัมน์ทางขวาแสดงรูปโปรไฟล์

## Component TableGrid
```jsx
const TableGrid = ({ children }) => {
  return (
    <TableContainer component={Paper} style={{ border: "2px solid lightgray", padding: "1em" }}>
      <Table>
        <TableBody>{children}</TableBody>
      </Table>
    </TableContainer>
  );
};
```
- คอมโพเนนต์นี้ใช้สร้างโครงสร้างตาราง (`Table`) และส่วนของข้อมูลในตาราง (`TableBody`) ซึ่งถูกใช้เพื่อแสดงลิงก์ไปยังหน้างานที่ต่าง ๆ ของ "Kunathip"
## ลิงก์ไปยัง Workshops

```jsx
<div style={{ display: "flex", gap: "1em", boder: "2px solid red" }}>
  {/* ลิงก์ไปที่ workshops โดยใช้ Next.js Link และ Material-UI Table */}
</div>
```
- ลิงก์ไปที่ workshops ถูกแสดงใน flex container พร้อมกรอบสีแดง
- แต่ละลิงก์เป็น Next.js Link component ซึ่งครอบด้วย Material-UI Table

## Layout และการ Export

```jsx
kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default kunathip;
```
- Method getLayout ถูกเพิ่มเข้าไปใน kunathip component เพื่อกำหนด layout โดยใช้ higher-order component
- Component ถูก export ออกไปเป็น default export


 
# Workshop resume 👨‍🎓
- React component ที่ใช้สร้างหน้า Resume โดยใช้ Next.js พร้อม Material-UI เป็นไลบรารีในการทำ styling และการสร้าง component



## Import Libraries/Components ที่ต้องใข้
```jsx
import React from "react";
import { styled } from "@mui/system";
import { Button, Grid, Paper, Typography } from "@mui/material";
import { Layout } from "src/layouts/dashboard/layout";
import Footer from "src/components/Por/Footer";
import Box from "@mui/material/Box";
import Divider from '@mui/material/Divider';
import Navbar from "src/components/Por/Navbar";
```
## Import Libraries/Components การทำงาน

- React: Import ตัวแปร `React` ที่จะใช้ในการสร้าง React Components
- styled: Import ฟังก์ชัน `styled` จาก MUI เพื่อสร้าง Styled Components
- `Button`, `Grid`, `Paper`, `Typography`: Import components จาก Material-UI ที่จะใช้ในการสร้างหน้า Resume
- `Layout`: Import Layout component จาก "src/layouts/dashboard/layout" ที่อาจเป็นการกำหนดโครงสร้างหน้าในแบบที่กำหนดเอง
- `Footer`: Import Footer component จาก "src/components/Por/Footer" เพื่อนำมาแสดงท้ายหน้า Resume
- `Box`: Import Box component จาก Material-UI เพื่อใช้ในการจัดรูปแบบ layout
- `Divider`: Import Divider component จาก Material-UI เพื่อสร้างเส้นแบ่งส่วนต่างๆ ในหน้า Resume
- `Navbar`: Import Navbar component จาก "src/components/Por/Navbar" เพื่อให้หน้า Resume มี Navbar สำหรับการนำทาง

โดย styled เป็นฟังก์ชันที่ให้ MUI ทำการ style components ต่างๆ ซึ่งในที่นี้ใช้กับ Paper component ที่ถูกเรียกชื่อเป็น Item และ img ที่ถูกเรียกชื่อเป็น ProfileImage



## Styled Components

- `Item`: Styled component สำหรับ Paper element ที่ใช้ในการรวมข้อมูลของ Resume โดยมีการกำหนดสีพื้นหลัง, ขนาดตัวอักษร, และการจัดตำแหน่งข้อความ
- `ResumeContainer` และ ResumeContainer2: Styled component สำหรับกำหนด margin และ padding ของส่วนต่าง ๆ ใน Resume
- `ProfileImage`: Styled component สำหรับกำหนดตำแหน่ง, ขนาด, และลักษณะอื่น ๆ ของรูปโปรไฟล์
- `SectionContainer`: Styled component สำหรับกำหนด margin, text-align, และ white-space ของส่วนต่าง ๆ ใน Resume

## โครงสร้างหน้า

- `Navbar`: ไฟล์ Navbar ถูก import เพื่อใช้เป็น Navbar บนหน้า Resume
- `Footer`: ไฟล์ Footer ถูก import เพื่อใช้เป็น Footer ของหน้า Resume
- `Grid Layout`: ใช้ Grid component จาก Material-UI เพื่อแบ่งหน้า Resume เป็นสองส่วน: ภาพโปรไฟล์ทางซ้ายและข้อมูล Resume ทางขวา
- ข้อมูล Resume: รายละเอียดต่าง ๆ ของ Resume ถูกแสดงใน Item component ที่ถูก styled. ข้อมูลรวมถึงชื่อ, ตำแหน่ง, ประวัติส่วนตัว, การศึกษา, ประสบการณ์, และทักษะ
- ปุ่ม Download Resume: มีปุ่มที่สามารถดาวน์โหลด Resume ของผู้ใช้

## สรุป
นี่เป็นส่วนหนึ่งของหน้า Resume ซึ่งถูกสร้างขึ้นด้วย React, Next.js, และ Material-UI. มีการใช้ styled components เพื่อปรับแต่งสไตล์และการจัดวางข้อมูลในหน้า Resume. โค้ดมีโครงสร้างที่ชัดเจนและใช้ Grid layout เพื่อทำให้หน้าเว็บเป็น responsive ตามขนาดหน้าจอ. นอกจากนี้ยังมี Navbar และ Footer ที่ถูกนำเข้ามาใช้ในหน้า Resume เพื่อการนำทางและเพิ่มความสมบูรณ์ในการออกแบบ

# Workshop 1 Calculator 📱
## Introduction
- มีการใช้ React, Material-UI, และ Styled Components เพื่อสร้างหน้าต่าง Calculator แบบพื้นฐาน ซึ่งสามารถทำการคำนวณเบื้องต้นได้ โดยมีการจัดรูปแบบ UI ให้สวยงามด้วยสีพื้นหลังและปุ่มต่าง ๆ ที่ใช้ในการคำนวณ นอกจากนี้ยังมีการใช้ State ในการเก็บค่า input และผลลัพธ์การคำนวณของ Calculator ด้วย

## Import Libraries/Components ที่ต้องใข้
```jsx
import React, { useState } from "react";
import { Layout } from "src/layouts/dashboard/layout";
import Footer from "src/components/Por/Footer";
import Box from "@mui/material/Box";
import { styled } from "@mui/system";
import { Button, Container, Grid, Paper, Typography } from "@mui/material";
import Navbar from "src/components/Por/Navbar";
```

## Import Libraries/Components การทำงาน
- Import `React` และ `useState` hook จาก React library เพื่อใช้สร้าง state ใน component.

- Import component `Layout` จากที่อยู่ของไฟล์ layout ในโฟลเดอร์ dashboard ที่อยู่ในโฟลเดอร์ layouts ที่อยู่ในโฟลเดอร์ src.

- Import component `Footer` จากที่อยู่ของไฟล์ Footer ในโฟลเดอร์ Por ที่อยู่ในโฟลเดอร์ components ที่อยู่ในโฟลเดอร์ src.

- Import component `Box` จาก Material-UI เพื่อใช้ในการกำหนดที่วางและพื้นหลังของ element.

- Import `styled `จาก Material-UI เพื่อใช้ในการสร้าง styled component.

- Import Material-UI components ที่ใช้ในworkshop นี้ `Button`,` Container`, `Grid`, `Paper`, และ `Typography`.

- import `Navbar` from "src/components/Por/Navbar";: Import component Navbar จากที่อยู่ของไฟล์ Navbar ในโฟลเดอร์ Por ที่อยู่ในโฟลเดอร์ components ที่อยู่ในโฟลเดอร์ src.
## Styled Components

### `Item`
Styled Paper component ที่ใช้สำหรับการจัดรูปแบบ

### `CalculatorContainer`
Styled Container component ที่แทนตัวคอนเทนเนอร์หลักของเครื่องคิดเลข

### `Input`
Styled input element สำหรับการแสดงผลบนหน้าจอของเครื่องคิดเลข

### `ButtonGrid`
Styled Grid component สำหรับการจัดเรียงปุ่มของเครื่องคิดเลข

### `RoundButton`
Styled Button component สำหรับปุ่มของเครื่องคิดเลข

## Functional Component: `kunathip`

นี่คือ functional component หลักที่ render เครื่องคิดเลข มีการใช้ state hooks (`useState`) เพื่อจัดการค่า input และ result

## การจัดการสถานะการทำงาน

- `handleButtonClick(value)`: เพิ่มค่าของปุ่มลงใน input
- `handleClear()`: ลบทั้ง input และ result
- `handleCalculate()`: คำนวณค่า input และอัปเดต result
- `handlePercentage()`: คำนวณเปอร์เซ็นต์ของค่า input

## การโต้ตอบกับผู้ใช้:
- ผลลัพธ์การคำนวณและข้อมูล input ถูกแสดงผลเพื่อให้ผู้ใช้เห็น

- ปุ่ม "=" นำไปสู่การคำนวณผลลัพธ์

- ปุ่ม "C" ให้ความสามารถในการล้างข้อมูล

- ปุ่ม "%" ให้ความสามารถในการคำนวณเปอร์เซ็นต์

## JSX Structure

- รวม Navbar และ Footer components
- Layout ของเครื่องคิดเลขถูกครอบด้วย Box ที่ได้รับการจัดแบบ responsive
- ตัวคอนเทนเนอร์หลัก (`CalculatorContainer`) ประกอบด้วย Typography element สำหรับหัวเรื่อง, input display (`Input`), และตารางปุ่มของเครื่องคิดเลข (`ButtonGrid`)

## Export และ Layout

- มีการกำหนด `getLayout` method สำหรับการกำหนด layout
- Component ถูก export เป็น default

# Workshop 2 homco/404 📝
## Introduction
- หน้า workshop นี้เป็นการออกแบบตาม UI ให้มีความคล้ายคลึง UI ที่เลือกมายังไม่การใส่ลักษณะการทำงาน function ในหน้านี้

## Import ที่ต้องใข้:
```jsx
import React, { useState, useEffect } from "react";
import { Link } from "next/router";
import { Container, Row, Col, Card, ListGroup, ListGroupItem } from "reactstrap";
import Navbar from "src/components/Por/Navbarworkshop2/Navbar";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { Typography } from "@mui/material";
import Footer from "src/components/Por/Footerworkshop2/Footer";
```

## Import Libraries/Components การทำงาน
- Import React, useState, useEffect จาก React library
- Import Link จาก Next.js เพื่อให้สามารถใช้งาน routing ไปยังหน้าอื่น
- Import Material-UI components เช่น Container, Grid, Box, Paper, Typography
- Import styled component จาก Material-UI
- Import Navbar, Footer components ที่ได้นำมาใช้

## การจัดการสถานะการทำงาน
### สร้าง Styled Component `(Item)`:

- สร้าง Styled Paper component ที่ถูกใช้เป็น container หลักของ Blog

### ใช้ `useEffect` ในการดึงข้อมูล:

- ใช้ useEffect เพื่อดึงข้อมูลจาก /api/posts และนำข้อมูลที่ได้ไปอัพเดท state posts

### สร้าง `JSX` สำหรับหน้า `Blog`:

- สร้าง JSX ที่ประกอบไปด้วย Navbar, ส่วน Header ของ Blog, และ Grid ที่ใช้ในการแสดงข้อมูลบทความ

### `Grid` สำหรับแสดงข้อมูลบทความ:

- ใช้ Grid แสดงบทความที่ได้จาก state posts
- ในแต่ละ Grid item, มีการแสดงรูปภาพ, ชื่อบทความ, และรายละเอียดบทความ

### ส่วน `Footer`:

- นำ Component Footer มาใช้เพื่อแสดง Footer ของ Blog

### ส่วน `CSS Styling`:

- มีการใช้ Styled Components ในการกำหนดสไตล์ของหน้า Blog
- การใช้ Grid ในการจัดวางบทความเพื่อให้แสดงผล
- การใช้ Typography เพื่อแสดงข้อความและรูปภาพ

### การใช้ `Positioning`:

- มีการใช้ position: absolute ในบางส่วนเพื่อทำให้ข้อความและรูปภาพตำแหน่งได้ตามที่ต้องการ

### การใช้ `Effect`:

- การใช้ effect ในการดึงข้อมูลเมื่อ component ถูก mount

### การใช้ `Link`:

- ใช้ Link component จาก Next.js เพื่อทำให้สามารถเปลี่ยนหน้าได้


## การใช้ Styled Components:
- มีการใช้ styled components ของ Material-UI เช่น Paper, Typography เพื่อกำหนดสไตล์ของ element ต่าง ๆ

# Workshop 3 Login show log

## Introduction
workshop นี้เป็นการทำหน้า page login โดยจะใช้ api ตรวจสอบการ login จาก Melivecode.com โดยข้อมูลที่ต้องใส่คือ 

```json
{
  "username": "karn.yong@melivecode.com",
  "password": "melivecode"
}
```
เเละแสดงlog การทำงานของระบบlogin และสร้างไฟล์ .env
โดยlogจะมี info request error
และให้เก็บ jwt ลง localstorage


## สร้างไฟล์ .env 
```
NEXT_PUBLIC_API_URL=https://www.melivecode.com/api
```


## Import ที่ต้องใข้:
```jsx
import { useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, TextField, Button, Paper, createTheme, ThemeProvider } from '@mui/material';
import Navbar from 'src/components/Por/Navbar';
```

## Import Libraries/Components การทำงาน
- `useState`: Hook ใน React ที่ใช้ในการเก็บ state ใน functional component
- `axios`: Library สำหรับทำ HTTP requests
- `useRouter`: Hook ใน Next.js สำหรับการนำเสนอ Router
- `Container`, Typography, TextField, Button, Paper, createTheme, ThemeProvider: Components และ utilities จาก Material-UI
- `Navbar`: Component ที่นำมาใช้สำหรับ Navbar ซึ่งไม่ได้แสดงในหน้านี้ แต่ถูกนำมาใช้ตามที่ import มา

## การใช้ Hook และ State
```jsx
const [username, setUsername] = useState('');
const [password, setPassword] = useState('');
```
- ใช้ `useState` เพื่อเก็บข้อมูล `username` และ `password` ที่ใส่จาก input fields

## การใช้ Router ของ Next.js
```jsx
const router = useRouter();
```
- ใช้ `useRouter` เพื่อเรียกใช้ Router ใน Next.js สำหรับการทำ navigation


## ฟังก์ชัน Handle Login

```jsx
const handleLogin = async () => {
  try {
    // ทำ HTTP POST request ไปยัง API สำหรับ login
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
      username,
      password,
    });

    console.log('Response from server:', response.data);

    if (response.data.status === 'ok') {
      // บันทึก JWT ลงใน Local Storage
      localStorage.setItem('accessToken', response.data.accessToken);
      console.log('Login successful!');
      console.log('User data:', response.data.user);
      // ทำการ navigate ไปยังหน้า user
      router.push('/workShop/kunathip/workshop-login/user');
    } else {
      console.error('Login failed:', response.data.message);
    }
  } catch (error) {
    console.error('API login error:', error);
  }
};
```

ใน `handleLogin ()`
- ทำ HTTP POST request ไปยัง API สำหรับ login โดยใช้ `axios.post`
- ดึงข้อมูลจาก response และทำการ log
- ถ้า login สำเร็จ (`response.data.status === 'ok'`) จะบันทึก JWT ลงใน Local Storage และทำการ navigate ไปยังหน้า user
- ถ้า login ไม่สำเร็จจะแสดง error message ใน console

## การใช้ Material-UI Components

```jsx
<ThemeProvider theme={theme}>
  <Container component="main" maxWidth="xs">
    <Paper elevation={3} style={{ padding: 20, display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '10rem' }}>
      {/* ... */}
    </Paper>
  </Container>
</ThemeProvider>
```
- ใช้ Material-UI components เพื่อสร้าง UI สำหรับหน้า Login.
- `ThemeProvider `และ `createTheme `ใช้ในการกำหนด theme สีของ Material-UI.

## สรุป Workshop 3
code ยังต้องการ environment variables จากไฟล์ .env สำหรับ API URL, username, และ password ที่ใช้ในการเข้าสู่ระบบ. กรุณาตรวจสอบความถูกต้องของข้อมูลในไฟล์ .env และตรวจสอบการติดตั้ง dependencies ที่จำเป็น (axios).

# Workshop 4 Show infomation User from login API

## Introduction
workshop นี้เป็นการใช้ jwt ที่ได้จากlogin ที่ทำมาดึงข้อมูลจาก API นี้ https://www.melivecode.com/api/auth/user
ให้นำข้อมูลมาแสดงและเก็บลงlocalstorage เเละหน้า UI จะเเสดงข้อมูลของผู้ใช้ที่ login เข้ามาจาก Workshop 3 

## Import ที่ต้องใข้:
```jsx
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, Button, Avatar, CircularProgress, Paper } from '@mui/material';
import { styled } from '@mui/system';
import Navbar from 'src/components/Por/Navbar';
```

## Import Libraries/Components การทำงาน
- `React `, `useEffect`, และ `useState`: Hooks ของ React ที่ใช้ในการจัดการ lifecycle และ state ของ component.
- `axios`: Library สำหรับทำ HTTP requests.
- `useRouter`: Hook ใน Next.js สำหรับการนำเสนอ Router.
- `Components` และ utilities จาก Material-UI เช่น `Container`, `Typography`, `Button`, `Avatar`, `CircularProgress`, และ `Paper`.
- `styled: Utility` จาก Material-UI เพื่อให้สามารถกำหนด style ในรูปแบบของ CSS-in-JS.

## การใช้ Styled Components
```jsx
const StyledContainer = styled(Container)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2em',
});

const UserInfoContainer = styled(Paper)({
  textAlign: 'center',
  marginTop: '1em',
  padding: '2em',
});

const AvatarImage = styled(Avatar)({
  width: '150px',
  height: '150px',
  margin: '0 auto 1em',
});

const LogoutButton = styled(Button)({
  marginTop: '1em',
});

const LoadingSpinner = styled(CircularProgress)({
  margin: '2em',
});
```
- ใช้ `styled` จาก Material-UI เพื่อสร้าง Styled Components ที่กำหนด style ของตัวเอง.

## การใช้ Hook และ State
```jsx
const router = useRouter();
const [user, setUser] = useState(null);
```
- ใช้ `useRouter `เพื่อเรียกใช้ Router ใน Next.js สำหรับการทำ navigation.
- ใช้ `useState` เพื่อเก็บ state ของข้อมูลผู้ใช้.

## การใช้ Hook useEffect

```jsx
useEffect(() => {
  const fetchUserData = async () => {
    
  };

  fetchUserData();
}, []);
```
- ใช้ `useEffect` เพื่อทำงานหลังจาก component นี้ถูก render เป็นครั้งแรก.
- ทำการ fetch ข้อมูลผู้ใช้จาก API โดยใช้ `axios.get`.
- ข้อมูลผู้ใช้ถูกเก็บไว้ใน state ด้วย `setUser`.

## การ Render UI Elements

```jsx
<StyledContainer>
  <Typography variant="h4" gutterBottom>
    User Information
  </Typography>
  {user ? (
    <UserInfoContainer elevation={3}>
      {/* ... */}
    </UserInfoContainer>
  ) : (
    <LoadingSpinner />
  )}
</StyledContainer>
```
- ทำการ render UI elements ภายใน `<StyledContainer>`.
- ถ้ามีข้อมูลผู้ใช้ (`user `ไม่เป็น `null`) จะแสดงข้อมูลผู้ใช้.
- ถ้ายังไม่มีข้อมูลผู้ใช้ จะแสดง Loading Spinner.

## การ Render ข้อมูลผู้ใช้

```jsx
<AvatarImage alt="User Avatar" src={user.avatar} />
<Typography variant="h6" gutterBottom>
  ID: {user.id}
</Typography>
{/* ... */}
<LogoutButton variant="contained" color="primary" onClick={handleLogout}>
  Logout
</LogoutButton>
```

- ใช้ข้อมูลผู้ใช้ที่ได้จาก API ในการ render UI elements ที่เกี่ยวข้อง.

## การ Handle Logout
```jsx
const handleLogout = () => {
  localStorage.clear();
  router.push('/workShop/kunathip/workshop-login/login');
};
```

- ฟังก์ชันที่ใช้ในการ logout โดยลบข้อมูลทั้งหมดใน Local Storage และทำการ navigate ไปยังหน้า login.

## สรุป Workshop 4 

code ยังต้องการ environment variables จากไฟล์ .env สำหรับ API URL. กรุณาตรวจสอบความถูกต้องของข้อมูลในไฟล์ .env และตรวจสอบการติดตั้ง dependencies ที่จำเป็น (axios).


# Workshop 5 Users/Search
## Introduction


## Users Api Infomation
## Import ที่ต้องใข้:
```jsx
import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import { useState, useEffect } from "react";
import TableContainer from "@mui/material/TableContainer";
import Avatar from "@mui/material/Avatar";
import Link from "@mui/material/Link";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Navbar from "src/components/Por/Navbar";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import TextField from "@mui/material/TextField";
import { id } from "date-fns/locale";

```

## Import Libraries/Components การทำงาน
- นำเข้าทั้งหมดของ module `React` เพื่อให้สามารถใช้ React Hooks และ Components ทั้งหมดได้.
- นำเข้า Material-UI components ที่ใช้ในการสร้าง UI และ UX สำหรับเว็บแอปพลิเคชัน.
- นำเข้า React Hooks `useState` และ `useEffect` เพื่อให้สามารถใช้ state และ lifecycle methods ได้.
- นำเข้า module `id `จาก `date-fns/locale `ที่อาจถูกใช้ในการจัดรูปแบบวันที่.
- นำเข้า custom component `Navbar` ที่อยู่ในที่ตั้งที่ถูกกำหนด (น่าจะเป็นโฟลเดอร์ `src/components/Por/Navbar`).
- นำเข้า `createTheme `และ `ThemeProvider `เพื่อกำหนดรูปแบบธีมของ Material-UI.

## การใช้ State และ Effect Hooks
```jsx
const [items, setItems] = useState([]);
const [selectedUser, setSelectedUser] = useState(null);
const [openEditDialog, setOpenEditDialog] = useState(false);

useEffect(() => {
  UserGet();
}, []);
```
`items ` :State ที่ใช้เก็บข้อมูลผู้ใช้.
`selectedUser `:State ที่ใช้เก็บข้อมูลผู้ใช้ที่ถูกเลือกในการแก้ไข.
`openEditDialog `:State ที่ใช้เก็บสถานะของ Dialog การแก้ไขข้อมูลผู้ใช้.
`useEffect `:ใช้เพื่อเรียก `UserGet `เมื่อ component ถูก mount เพื่อดึงข้อมูลผู้ใช้

## function `UserGet`
```jsx
const UserGet = () => {
  // ...
};
```
- ฟังก์ชันที่ใช้เรียก API เพื่อดึงข้อมูลผู้ใช้.

## function `UserUpdate`
```jsx
const UserUpdate = (user) => {
  // ...
};

const handleUpdate = () => {
  // ...
};

```
- `UserUpdate`: ฟังก์ชันที่ใช้เตรียมข้อมูลผู้ใช้ที่ต้องการแก้ไข.
- `handleUpdate`: ฟังก์ชันที่ใช้ส่งข้อมูลผู้ใช้ที่แก้ไขไปยัง API.

## function `handleCloseDialog`
```jsx
const handleCloseDialog = () => {
  setOpenEditDialog(false);
};
```
- ฟังก์ชันที่ใช้ปิด Dialog การแก้ไข.

## function `UserDelete`
- ฟังก์ชันที่ใช้ส่งคำขอลบข้อมูลผู้ใช้ไปยัง API.

## Return (Render) UI
```jsx
return (
  <>
    {/* ... */}
  </>
);
```
- ส่วนที่ return มีโครงสร้างของ Material-UI components เพื่อแสดงรายการผู้ใช้.

## Dialog สำหรับการแก้ไขข้อมูลผู้ใช้
```jsx 
<Dialog open={openEditDialog} onClose={handleCloseDialog}>
  {/* ... */}
</Dialog>
```
- ใช้ Dialog ของ Material-UI เพื่อแสดงฟอร์มแก้ไขข้อมูลผู้ใช้.

## การใช้ Material-UI Components
- การใช้ Components ต่าง ๆ ของ Material-UI เช่น `Paper`,` Typography`,` Button`,` Card`, `Avatar`, `Dialog`, `TextField`

## การใช้ TextField ใน Dialog
```jsx
<TextField
  label="First Name"
  value={selectedUser?.fname}
  onChange={(e) => setSelectedUser({ ...selectedUser, fname: e.target.value })}
  fullWidth
/>
```
- การใช้ `TextField` ของ Material-UI สำหรับรับข้อมูลจากผู้ใช้ในฟอร์ม.

## การใช้ Button กดเพื่อให้ function ทำงาน
```jsx
    <CardActions className="btn-group">
                      <Button
                        className="button"
                        sx={{ backgroundColor: "#19a7ce", color: "white" }}
                        onClick={() => UserUpdate(row.id)}
                      >
                        Edit
                      </Button>
                      <Button
                        className="button"
                        sx={{ backgroundColor: "#19a7ce", color: "white" }}
                        onClick={() => UserDelete(row.id)}
                      >
                        Delete
                      </Button>
                    </CardActions>
```
- ปุ่ม Button edit & delete เมื่อกดปุ่มจะทำให้ส่งไปยัง `UserUpdate` เเละ `UserDelete` เเละจะทำงานทำคำสั่งใน function นั้น

# User create 
```jsx
 <Box>
                <Link href="UserCreate">
                  <Button className="button" sx={{ backgroundColor: "#19a7ce", color: "white" }}>
                    Create
                  </Button>
                </Link>
              </Box>
```
- เมื่อผู้ใช้กดปุ่ม Create จะทำการส่งไปยัง path ที่กำหนดเพื่อเข้าหน้า create user

## Import ที่ต้องใข้:
```jsx
import React, { useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { Grid, TextField, Typography, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
```
## Import Libraries/Components การทำงาน

- `CssBaseline`: Component ที่ให้การกำหนดค่า baseline สำหรับ CSS เพื่อให้การทำงานของเว็บไซต์เหมือนกันทั่วทุกระบบ.

- `Container`: Component ที่ให้ความสามารถในการจัดหน้าเว็บไซต์ให้สวยงามและสมดุล.

- `Grid`: Component ที่ให้ระบบ grid ในการจัดวางองค์ประกอบของ UI ในตำแหน่งที่ต้องการ.

- `TextField`: Component ที่ให้ input field สำหรับผู้ใช้ป้อนข้อมูล.

- `Typography`: Component ที่ให้การจัดรูปแบบข้อความและกำหนดรายละเอียดต่างๆ เช่น font size, color, และอื่นๆ.

- `Button`: Component ที่ให้ปุ่มสำหรับทำการกระทำต่างๆ.

- `useNavigate`: Hook ที่ให้การนำทาง (navigation) ในโครงการ React Router. ใช้เพื่อเปลี่ยนเส้นทางของแอปพลิเคชันเมื่อผู้ใช้ทำการ interact กับ UI หรือเหตุการณ์อื่นๆ.

## Component function 
```jsx
export default function UserCreate() {
  // function and component 
}
```
- คอมโพเนนต์แบบฟังก์ชัน (`UserCreate`): คอมโพเนนต์หลักของ React สำหรับการสร้างผู้ใช้ใหม่.


## สร้าง Sate เพื่อเก็บข้อมูลของผู้ใช้

```jsx
 const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");
```
- สถานะ (`useState`): ตัวแปรสำหรับเก็บข้อมูลของผู้ใช้.

## Hook ที่ใช้ในการนำทาง (navigation)
```jsx
const navigate = useNavigate();
```
- Navigation Hook (`useNavigate`): ให้ความสามารถในการนำทางระหว่างหน้า.

## function การส่งคำขอไปยัง API
```jsx
const handleSubmit = (event) => {
  //....
};
```
- ฟังก์ชันสำหรับการส่งฟอร์ม (`handleSubmit`): จัดการกับการส่งฟอร์ม, ส่งคำขอไปยัง API เพื่อสร้างผู้ใช้ใหม่.

## โครงสร้างของฟอร์ม
```jsx
return (
  <React.Fragment>
    <CssBaseline />
    <Container maxWidth="sm" sx={{ p: 2 }}>
      {/* องค์ประกอบ UI จะอยู่ที่นี่ */}
    </Container>
  </React.Fragment>
);
```
- โครงสร้างของ UI (`<Container>`): คลุมรอบองค์ประกอบ UI ด้วย Container ของ Material-UI.

## ช่องสำหรับป้อนข้อมูล
```jsx
<TextField
  id="fname"
  label="ชื่อ"
  variant="outlined"
  fullWidth
  required
  onChange={(e) => setFname(e.target.value)}
/>
```
- ช่องป้อนข้อมูล (`<TextField>`): ใช้เพื่อรวบรวมข้อมูลของผู้ใช้

## ปุ่มที่ใช้สำรหับการส่งข้อมูลไปยัง function ที่ใช้ fetch api

```jsx
<Button
  className="button"
  sx={{ backgroundColor: "#19a7ce", color: "white" }}
  type="submit"
  fullWidth
>
  //create 
</Button>
```

## Fnction ที่ใช้ส่งคำขอไปยัง API
```jsx
const createUrl = `${process.env.NEXT_PUBLIC_API_URL}/users/create`;
fetch(createUrl, requestOptions)
  .then((response) => {
    // log การเเสดงผล
  })
  .then((result) => {
    // log การเเสดงผล
  })
  .catch((error) => {
    // log การเเสดงผล
  });
```
- การส่งคำขอ API ผ่าน Fetch: ส่งคำขอไปยัง API เพื่อสร้างผู้ใช้

## Component เสริมที่ใช้ 
- Alert: ใช้แสดงข้อความตอบรับผลลัพธ์จากการสร้างผู้ใช้ในรูปแบบ `alert`.

- Console.log: ใช้เพื่อ debug และตรวจสอบสถานะการทำงานของโค้ด.

- Material-UI Components: ใช้สำหรับสร้างองค์ประกอบ UI ที่สวยงามและมีสไตล์ที่ทันสมัย.

# Search Users API 

## introduction
หน้านี้เป็นหน้าที่ดึงข้อมูลของ user เพื่อทดสอบเส้น api ของการ search ข้อมูลจาก api เเละการจัดเรียงหน้า page 
## Import ที่ต้องใข้:
```jsx
import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, Card, CardContent, CardMedia, Typography } from "@mui/material";
import styled from "@emotion/styled";
import Navbar from "src/components/Por/Navbar";
```
## Import Libraries/Components การทำงาน
- `React, { useState, useEffect } from "react"`: Import React และ hooks ที่ชื่อว่า useState และ useEffect จากไลบรารี React.

- `{ TextField, Button, Grid, Card, CardContent, CardMedia, Typography } from "@mui/material"`: Import Material-UI components ที่ใช้ในโค้ดนี้ ได้แก่ TextField, Button, Grid, Card, CardContent, CardMedia, Typography.

- `styled from "@emotion/styled"`: Import `styled-components` จาก Emotion ซึ่งจะถูกใช้ในการสร้าง styled components.

- `Navbar from "src/components/Por/Navbar"`: Import component `Navbar` จากที่อยู่ "src/components/Por/Navbar".

## การใช้ Styled Components
```jsx
const SearchContainer = styled.div`
 ....
  }
`;

const UserCard = styled(Card)`
  ...
`;

const NavigationButtons = styled.div`
  ....
`;

```
- `SearchContainer`: styled component สำหรับ div ที่ใช้เป็น Container สำหรับการค้นหา มี padding, จัดวางแบบ flex, และตกแต่งเพิ่มเติมในหน้าแสดงผล.

- `UserCard`: styled component สำหรับ Card ที่แสดงข้อมูลผู้ใช้ มีการจัดวางแบบ flex และกำหนด cursor เป็น pointer.

- `NavigationButtons`: styled component สำหรับ div ที่ใช้แสดงปุ่มการนำทาง มีการจัดวางแบบ flex.

## การใช้ Functional Component

```jsx 
const UserListPage = () => {
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");

  const fetchUsers = async (page, search = "") => {
    // ... (fetch user data from API)
  };

  const handleNextPage = () => {
    // ... (navigate to next page)
  };

  const handlePrevPage = () => {
    // ... (navigate to previous page)
  };

  const handleSearch = () => {
    // ... (initiate user search)
  };

  const handleCardClick = (userId) => {
    // ... (navigate to user details page)
  };

  useEffect(() => {
    // ... (fetch users on component mount and when currentPage or searchTerm changes)
  }, [currentPage, searchTerm]);

  return (
    <div>
      <Navbar />
      <SearchContainer>
        {/* ... (search input and button) */}
        <NavigationButtons>
          {/* ... (previous page and next page buttons) */}
        </NavigationButtons>
      </SearchContainer>

      <Grid container spacing={2}>
        {users.map((user) => (
          <Grid item key={user.id} xs={12} sm={6} md={4} lg={3}>
            <UserCard onClick={() => handleCardClick(user.id)}>
              {/* ... (user card content) */}
            </UserCard>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default UserListPage;
```
- UserListPage: functional component สำหรับการแสดงหน้าเว็บเพจรายการผู้ใช้ มีการใช้ state ที่เกี่ยวกับรายการผู้ใช้, หน้าปัจจุบัน, และจำนวนหน้าทั้งหมด.

- `fetchUsers`: ฟังก์ชันที่ใช้ async/await เพื่อดึงข้อมูลผู้ใช้จาก API.

- `handleNextPage` และ `handlePrevPage`: ฟังก์ชันที่ใช้ในการนำทางไปยังหน้าถัดไปและหน้าก่อนหน้า.

- `handleSearch`: ฟังก์ชันที่ใช้ในการค้นหาผู้ใช้.

- `nhandleCardClick`: ฟังก์ชันที่ใช้ในการนำทางไปยังหน้ารายละเอียดผู้ใช้.

- `useEffect`: ใช้เพื่อดึงข้อมูลผู้ใช้เมื่อ component ถูกโหลด และเมื่อ `currentPage` หรือ `searchTerm` เปลี่ยนแปลง.

# Workshop 5 Attractions

## Introductions
หน้านี้เป็น workshop ที่ดึง API ในส่วนของ service attraactions จาก Melivecode โดยในหน้านี้จะมีการ Get ข้อมูลของ api มาเเสดงยนหน้าเว็บเพจเเละมี api ที่ใช้สำหรับการค้นหาข้อมูลใน api เเละหน้าเเบ่งเเยกหน้าเพจในข้อมูลของ api นั้น 

## Import Libraries/Components ที่ต้องใข้
```jsx
import React, { useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Navbar from "src/components/Por/Navbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

```
## Import Libraries/Components การทำงาน
- React Hooks: `useEffect` และ `useState` ใช้สำหรับจัดการวงจรชีวิตของคอมโพเนนต์.

- Material-UI Components: `CssBaseline`,` Container`, `Typography`, `Card`, `CardContent`, `CardMedia`, `Grid`, `TextField`, และ `Button` เพื่อสร้าง UI ที่สวยงาม.

## การกำหนด Sate
```jsx
const [attractions, setAttractions] = useState([]);
const [search, setSearch] = useState("");
const [currentPage, setCurrentPage] = useState(1);
const [totalPages, setTotalPages] = useState(1);
```
- สถานะ (`useState`): เก็บข้อมูลเกี่ยวกับสถานที่ท่องเที่ยว, คำค้น, และสถานะหน้าปัจจุบันและทั้งหมด.

## การใช้ Effect Hook

```jsx
useEffect(() => {
  fetchAttractions();
}, [currentPage]);

```
- Effect Hook (`useEffect`): เรียก `fetchAttractions` เมื่อ `currentPage` เปลี่ยน.

## function การค้นหา
```jsx
const handleSearch = () => {
  setCurrentPage(1);
  fetchAttractions();
};
```
- ฟังก์ชันการค้นหา (`handleSearch`): ตั้ง` currentPage` เป็น 1 และเรียก `fetchAttractions` เพื่อค้นหา

## functiond การดึงข้อมูลใน api มาใช้
```jsx
const fetchAttractions = () => {
    // เรียก API เพื่อดึงข้อมูลสถานที่ท่องเที่ยว
    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/th/attractions?language=en&search=${search}&page=${currentPage}&per_page=10&sort_column=id&sort_order=desc`
    )
      .then((response) => response.json()) // รอการตอบกลับจาก API
      .then((data) => {
        // เมื่อได้ข้อมูลจาก API แปลงเป็น JSON
        console.log("Attractions data received:", data);
        // อัพเดทสถานะ attractions เพื่อแสดงข้อมูลที่ได้จาก API
        setAttractions(data.data);
        // อัพเดทสถานะ totalPages เพื่อให้ทราบจำนวนหน้าทั้งหมด
        setTotalPages(data.total_pages);
      })
      .catch((error) => {
        // กรณีเกิดข้อผิดพลาดในการดึงข้อมูล
        console.error("Error fetching attractions:", error);
      });
  };
  ```
  - ฟังก์ชันดึงข้อมูล (`fetchAttractions`): ส่งคำขอไปยัง API เพื่อดึงข้อมูลสถานที่ท่องเที่ยว search มูลสถานที่ท่องเที่ยว การดึงข้อมูล language ของสถานที่ท่องเที่ยว เเละcurrentPage ข้อมูลสถานที่ท่องเที่ยวทั้งหมด

  ## functiond หน้าถัดไปและหน้าก่อนหน้า
```jsx
const handleNextPage = () => {
  if (currentPage < totalPages) {
    setCurrentPage(currentPage + 1);
  }
};

const handlePreviousPage = () => {
  if (currentPage > 1) {
    setCurrentPage(currentPage - 1);
  }
};
```
- ฟังก์ชันหน้าถัดไปและหน้าก่อนหน้า (`handleNextPage` และ `handlePreviousPage`): อัพเดท `currentPage` เพื่อเปลี่ยนหน้าของการเเสดงข้อมูลสถานที่ท่องเที่ยว

## โครงสร้าง UI
```jsx
return (
  <React.Fragment>
    <Navbar />
    <CssBaseline />
    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
      {/* โครงสร้าง UI จะอยู่ที่นี่ */}
    </Container>
  </React.Fragment>
);
```
- โครงสร้าง UI (`<Container>`): ประกอบด้วย Navbar, และตัวคอมโพเนนต์ที่ใช้ในการแสดงรายการสถานที่ท่องเที่ยว.

## ช่องค้นหาเเละปุ่มกด search
```jsx
<div style={{ display: "flex", alignItems: "center", marginBottom: 2 }}>
  <TextField
    label="ค้นหา"
    variant="outlined"
    fullWidth
    value={search}
    onChange={(e) => setSearch(e.target.value)}
    sx={{ marginRight: 1, width: "20%" }}
  />
  <Button
    variant="contained"
    onClick={handleSearch}
    sx={{ backgroundColor: "#19a7ce", color: "white" }}
  >
    ค้นหา
  </Button>
</div>
```
- ช่องค้นหาและปุ่มค้นหา (`<TextField>` และ `<Button>`): ใช้สำหรับการป้อนข้อมูลค้นหาและเรียก `handleSearch` เพื่อค้นหา.

## เเสดงข้อมูลของเเหล่งท่องเที่ยวเเต่ละสถานที่ 
```jsx
{attractions.map((attraction) => (
  <Grid item key={attraction.id} xs={12} sm={6} md={4}>
    <Card>
      <CardMedia
        component="img"
        alt={attraction.name}
        height="140"
        image={attraction.coverimage}
      />
      <CardContent>
        <Typography variant="h6" component="div">
          {attraction.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {attraction.detail}
        </Typography>
      </CardContent>
    </Card>
  </Grid>
))}
```
- การแสดงรายการ (`<Grid>`): ใช้สำหรับแสดงรายการของสถานที่ท่องเที่ยว

## ปุ่มหน้าถัดไปและหน้าก่อนหน้า
```jsx
<div style={{ marginTop: "3rem" }}>
  {currentPage > 1 && (
    <Button
      variant="contained"
      onClick={handlePreviousPage}
      sx={{ marginRight: 2, backgroundColor: "#19a7ce", color: "white" }}
    >
      หน้าก่อนหน้า
    </Button>
  )}

  {currentPage < totalPages && (
    <Button
      variant="contained"
      onClick={handleNextPage}
      sx={{ backgroundColor: "#19a7ce", color: "white" }}
    >
      หน้าถัดไป
    </Button>
  )}
</div>

```
- ปุ่มหน้าถัดไปและหน้าก่อนหน้า (`<Button>`)เมื่อกดปุ่มจะเข้าสู่ function ` handleNextPage` เเละ `handlePreviousPage` สำหรับการเปลี่ยนหน้าแสดงข้อมูล

# Workshop 6 Shopping

## Introduction 
workshop นี้เป็นการสร้างหน้า shopping ของตัวเองโดย ผู้จัดทำได้ทำการใช้ api ของ https://fakestoreapi.com/docs เพื่อนำข้อมูลใน api เส้นนี้มาใช้ในหน้า shopping ของ workshop นี้ เเละเป็นระบบที่ ระบบสั่งสินค้ามีรายการสินค้าให้เลือกเก็บลงตะกร้ากดยืนยันแล้วแสดง popupรายการสินค้าที่เลือกบอกจำนวนเงิน และจำนวนสินค้า listเป็นรายการ มี Filter ค้นหาสินค้า เเยกหมวดหมู่สอนค้า เเละเรียงลำดับราคาของสินค้า ที่เเสดงบนหน้า UI 



