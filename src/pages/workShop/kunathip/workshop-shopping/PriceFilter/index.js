
import React from "react";
import { ButtonGroup, Button } from "@mui/material";

const PriceFilter = ({ sortBy, setSortBy }) => {
  return (
    <ButtonGroup color="primary" aria-label="outlined primary button group" style={{ marginBottom: "1rem" }}>
      <Button
        onClick={() => setSortBy("default")}
        variant={sortBy === "default" ? "contained" : "outlined"}
      >
        Default
      </Button>
      <Button
        onClick={() => setSortBy("price-low-to-high")}
        variant={sortBy === "price-low-to-high" ? "contained" : "outlined"}
      >
        Price Low to High
      </Button>
      <Button
        onClick={() => setSortBy("price-high-to-low")}
        variant={sortBy === "price-high-to-low" ? "contained" : "outlined"}
      >
        Price High to Low
      </Button>
    </ButtonGroup>
  );
};

export default PriceFilter;
