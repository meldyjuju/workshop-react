import React, { useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { Grid, TextField, Typography, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";

// สร้างคอมโพเนนต์ UserCreate ที่เป็นฟอร์มสำหรับการสร้างผู้ใช้
export default function UserCreate() {
  // ฟังก์ชันที่จะถูกเรียกเมื่อฟอร์มถูกส่ง
  const handleSubmit = (event) => {
    event.preventDefault();

    // สร้าง Header สำหรับ request
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    // สร้างข้อมูล JSON จาก state ของฟอร์ม
    var raw = JSON.stringify({
      fname: fname,
      lname: lname,
      username: username,
      email: email,
      avatar: avatar,
    });

    // ตั้งค่า request options
    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    // กำหนด URL สำหรับการสร้างผู้ใช้
    const createUrl = `${process.env.NEXT_PUBLIC_API_URL}/users/create`;

    // ส่ง request ไปที่ API
    fetch(createUrl, requestOptions)
      .then((response) => {
        console.log("Create User Response Status:", response.status);
        return response.json();
      })
      .then((result) => {
        console.log("Create User Result:", result);

        // แสดง Alert ด้วยข้อความที่ได้จาก API
        alert(result["message"]);

        // ตรวจสอบสถานะการสร้างผู้ใช้
        if (result["status"] === "ok") {
          console.log("Create User Success");
        }
      })
      .catch((error) => {
        console.log("Create User Error:", error);
      });
  };

  // สถานะ (state) สำหรับเก็บข้อมูลจากฟอร์ม
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom component="div">
          Create User
        </Typography>
        {/* ฟอร์มสำหรับการสร้างผู้ใช้ */}
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="fname"
                label="First Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setFname(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="lname"
                label="Last Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setLname(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="username"
                label="Username"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setUsername(e.target.value)}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                id="email"
                label="Email"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="avatar"
                label="Avatar"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setAvatar(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              {/* ปุ่มสำหรับส่งฟอร์ม */}
              <Button
                className="button"
                sx={{ backgroundColor: "#19a7ce", color: "white" }}
                type="submit"
                fullWidth
              >
                Create
              </Button>
              {/* ปุ่มสำหรับกลับไปที่เมนูหลัก */}
              <Button
                className="button"
                sx={{ backgroundColor: "#19a7ce", color: "white", marginTop: "10px" }}
                onClick={() => (window.location.href = "/workShop/kunathip/workshop-userapi/MediaCard")}
                fullWidth
              >
                Back to Menu
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
