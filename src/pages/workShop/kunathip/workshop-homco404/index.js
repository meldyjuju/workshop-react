import Grid from "@mui/material/Grid";
import React from "react";
import Navbar from "src/components/Por/Navbarworkshop2/Navbar";
import Footer from "src/components/Por/Footerworkshop2/Footer";
import { Typography } from "@mui/material";
import Button from "@mui/material/Button";
import { useRouter } from "next/router";

const Custom404 = () => {
  const router = useRouter();

  return (
    <div>
      <Navbar />
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        style={{ height: "100vh", backgroundColor: "#A9A9A9" }}
      >
        <Typography align="center">
          <Typography variant="h1" sx={{ fontSize: "5rem", color: "#FFF", fontWeight: "bold" }}>
            ERROR 404
          </Typography>
          <Typography variant="body1" sx={{ fontSize: "1em", color: "#FFF", fontWeight: "bold" }}>
            PAGE NOT FOUND, PLEASE GO BACK
          </Typography>
          <Button
            style={{ marginTop: "2em", backgroundColor: "#757575" }}
            variant="contained"
            onClick={() => router.push("/workShop/kunathip/workshop-homco")}
          >
            Go Back to Home
          </Button>
        </Typography>
      </Grid>

      <Footer />
    </div>
  );
};

export default Custom404;
