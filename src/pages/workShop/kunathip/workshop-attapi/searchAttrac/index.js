import React, { useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Navbar from "src/components/Por/Navbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

const AttractionsPage = () => {
  const [attractions, setAttractions] = useState([]);
  const [search, setSearch] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    fetchAttractions();
  }, [currentPage]);

  const handleSearch = () => {
    setCurrentPage(1);
    fetchAttractions();
  };

  // ฟังก์ชัน fetchAttractions
  const fetchAttractions = () => {
    // เรียก API เพื่อดึงข้อมูลสถานที่ท่องเที่ยว
    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/th/attractions?language=en&search=${search}&page=${currentPage}&per_page=10&sort_column=id&sort_order=desc`
    )
      .then((response) => response.json()) // รอการตอบกลับจาก API
      .then((data) => {
        // เมื่อได้ข้อมูลจาก API แปลงเป็น JSON
        console.log("Attractions data received:", data);
        // อัพเดทสถานะ attractions เพื่อแสดงข้อมูลที่ได้จาก API
        setAttractions(data.data);
        // อัพเดทสถานะ totalPages เพื่อให้ทราบจำนวนหน้าทั้งหมด
        setTotalPages(data.total_pages);
      })
      .catch((error) => {
        // กรณีเกิดข้อผิดพลาดในการดึงข้อมูล
        console.error("Error fetching attractions:", error);
      });
  };

  // ฟังก์ชัน handleNextPage
  const handleNextPage = () => {
    // ตรวจสอบว่าหน้าถัดไปยังไม่เกินหน้าทั้งหมด
    if (currentPage < totalPages) {
      // อัพเดท currentPage เพื่อไปยังหน้าถัดไป
      setCurrentPage(currentPage + 1);
    }
  };

  // ฟังก์ชัน handlePreviousPage
  const handlePreviousPage = () => {
    // ตรวจสอบว่าหน้าก่อนหน้ายังไม่ต่ำกว่าหน้า 1
    if (currentPage > 1) {
      // อัพเดท currentPage เพื่อไปยังหน้าก่อนหน้า
      setCurrentPage(currentPage - 1);
    }
  };

  return (
    <React.Fragment>
      <Navbar />
      <CssBaseline />
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Typography variant="h4" component="div" gutterBottom>
          Attractions
        </Typography>
        <div style={{ display: "flex", alignItems: "center", marginBottom: 2 }}>
          <TextField
            label="Search"
            variant="outlined"
            fullWidth
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            sx={{ marginRight: 1, width: "20%" }}
          />
          <Button
            variant="contained"
            onClick={handleSearch}
            sx={{ backgroundColor: "#19a7ce", color: "white" }}
          >
            Search
          </Button>
        </div>

        <Grid marginTop={1} container spacing={3}>
          {attractions.map((attraction) => (
            <Grid item key={attraction.id} xs={12} sm={6} md={4}>
              <Card>
                <CardMedia
                  component="img"
                  alt={attraction.name}
                  height="140"
                  image={attraction.coverimage}
                />
                <CardContent>
                  <Typography variant="h6" component="div">
                    {attraction.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {attraction.detail}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>

        <div style={{ marginTop: "3rem" }}>
          {currentPage > 1 && (
            <Button
              variant="contained"
              onClick={handlePreviousPage}
              sx={{ marginRight: 2, backgroundColor: "#19a7ce", color: "white" }}
            >
              Previous Page
            </Button>
          )}

          {currentPage < totalPages && (
            <Button
              variant="contained"
              onClick={handleNextPage}
              sx={{ backgroundColor: "#19a7ce", color: "white" }}
            >
              Next Page
            </Button>
          )}
        </div>
      </Container>
    </React.Fragment>
  );
};

export default AttractionsPage;
