import { useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, TextField, Button, Paper, createTheme, ThemeProvider } from '@mui/material';
import Navbar from 'src/components/Por/Navbar';

// Component สำหรับหน้า Login
const Login = () => {
  // สถานะ (state) สำหรับเก็บข้อมูล username และ password
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // Hook สำหรับการเปลี่ยนเส้นทาง (routing)
  const router = useRouter();

  // ฟังก์ชันสำหรับการทำ Login
  const handleLogin = async () => {
    try {
      // ทำการส่ง request ไปที่ API endpoint สำหรับ Login
      const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
        username,
        password,
      });

      console.log('Response from server:', response.data);

      // ตรวจสอบสถานะการ Login จาก response
      if (response.data.status === 'ok') {
        // เก็บ accessToken ลงใน localStorage
        localStorage.setItem('accessToken', response.data.accessToken);
        console.log('Login successful!');
        console.log('User data:', response.data.user);

        // เปลี่ยนเส้นทางไปที่หน้า user หลังจาก Login สำเร็จ
        router.push('/workShop/kunathip/workshop-login/user');
      } else {
        console.error('Login failed:', response.data.message);
      }
    } catch (error) {
      console.error('API login error:', error);
    }
  };

  // กำหนด theme สำหรับ Material-UI
  const theme = createTheme({
    palette: {
      primary: {
        main: '#19a7ce',
      },
    },
  });

  return (
    <>
      {/* Navbar component สำหรับการนำทาง */}
      <Navbar />
      {/* ThemeProvider เพื่อให้ Material-UI ใช้ theme ที่กำหนด */}
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          {/* Paper component สำหรับกล่อง login form */}
          <Paper elevation={3} style={{ padding: 20, display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '10rem' }}>
            <Typography component="h1" variant="h5" gutterBottom>
              Login
            </Typography>
            {/* TextField สำหรับกรอก username */}
            <TextField
              label="Username"
              variant="outlined"
              margin="normal"
              fullWidth
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            {/* TextField สำหรับกรอก password */}
            <TextField
              label="Password"
              variant="outlined"
              margin="normal"
              type="password"
              fullWidth
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            {/* Button สำหรับเรียกฟังก์ชัน handleLogin เมื่อถูกคลิก */}
            <Button
              variant="contained"
              color="primary"
              fullWidth
              style={{ marginTop: 20, color: 'white', fontWeight: 'bold' }}
              onClick={handleLogin}
            >
              Login
            </Button>
          </Paper>
        </Container>
      </ThemeProvider>
    </>
  );
};

export default Login;
