import React from "react";
import { styled } from "@mui/system";
import { Button, Grid, Paper, Typography } from "@mui/material";
import { Layout } from "src/layouts/dashboard/layout";
import Footer from "src/components/Por/Footer";
import Box from "@mui/material/Box";
import Divider from '@mui/material/Divider';
import Navbar from "src/components/Por/Navbar";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
  margin: "10em 10em 10em 10em",
}));

const ResumeContainer = styled("div")({
  marginTop: "5em",
  marginLeft: "0em",
  marginBottom: "5em",
  padding: "0px",
});

const ResumeContainer2 = styled("div")({
  
  marginLeft: "5em",
});

const ProfileImage = styled("img")({
  position: "absolute",
  top: "35%",
  left: "20%",
  // marginTop: "12em",
  // marginLeft: "5em",
  margin: "12em 1em",
  borderRadius: "2rem",
  width: "200px",
  // height: "400px",
  objectFit: "cover",
  marginBottom: "1em",
});

const SectionContainer = styled("div")({
  marginRight: "10em",
  marginTop: "2em",
  textAlign: "left",
  whiteSpace: "pre-line",
});

const Resume = () => {
  const profileImagePath = "https://firebasestorage.googleapis.com/v0/b/imagekunathip.appspot.com/o/self.jpeg?alt=media&token=57b83c9e-6d83-4d4d-9ab9-65071b5a3b63";
  const name = "Kunathip Padthaisong";
  const role = "Web Developer - frontend";
  const aboutMe =
    "Hello! I am a passionate web developer with expertise in front-end and back-end technologies. I am dedicated to creating responsive and user-friendly web applications.";
  const education = "- Degree in Computer Science, Khon Kaen University, Graduation Year 2024";
  const experience = "- Web Developer, XYZ Company, Start Date - End Date";
  const skills = "- JavaScript, React.js, Node.js, HTML, CSS, etc.";

  return (
    <>
    <Navbar/>
      <Box sx={{ flexGrow: 1, backgroundColor: "#F5F5F5" }}>
        <Grid container spacing={3}>
          <Grid item sx={{ }} xs={12} md={3} lg={2}>
            <ProfileImage src={profileImagePath} alt={name} />
          </Grid>
          <Grid item xs={12} md={9} lg={10}>
            <Item>
              <ResumeContainer >
                <Typography variant="h4" gutterBottom style={{ letterSpacing: ".2rem" }}>
                  {name}
                </Typography>
              
                <Typography variant="subtitle1" gutterBottom sx = {{marginBottom: '2em'}}>
                  {role}
                </Typography>
                <Divider />
              <ResumeContainer2>
                <SectionContainer>
                  <Typography variant="h5" gutterBottom>
                    About Me 👨🏻‍💻
                  </Typography>
                  <Typography variant="body1">{aboutMe}</Typography>
                </SectionContainer>

                <SectionContainer>
                  <Typography variant="h5" gutterBottom>
                    Education 📚
                  </Typography>
                  <Typography variant="body1">{education}</Typography>
                </SectionContainer>

                <SectionContainer>
                  <Typography variant="h5" gutterBottom>
                    Experience 💻
                  </Typography>
                  <Typography variant="body1">{experience}</Typography>
                </SectionContainer>

                <SectionContainer>
                  <Typography variant="h5" gutterBottom>
                    Skills 💎
                  </Typography>
                  <Typography variant="body1">{skills}</Typography>
                </SectionContainer>

                <Button variant="outlined" style={{ marginTop: "1.5em" }}>
                  Download Resume
                </Button>
                </ResumeContainer2>
              </ResumeContainer>
            </Item>
          </Grid>
        </Grid>
      </Box>
      <Footer />
    </>
  );
};

Resume.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default Resume;
