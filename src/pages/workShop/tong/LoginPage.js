import * as React from "react";
import LoginFrom from "src/components/Tong/Work3/LoginForm";
import MyLayout2 from "./MyLayout2";
const LoginPage = ({ children }) => {
  return (
    <>
      <MyLayout2>
        <br/>
        <LoginFrom/>
      </MyLayout2>
    </>
  );
};

// tong.getLayout = (tong) => <Layout>{tong}</Layout>;
export default LoginPage;
