import { Layout} from "src/layouts/dashboard/layout";

// import components
import Intro from "../../../components/johnny/intro";
import WorkshopTB from "src/components/johnny/workshopTB";
import Footer from "src/components/johnny/footer";
import Info from "src/components/johnny/info";
import End from "src/components/johnny/end"

const natpacanSri = () => {
    return(
        <div className="px-20">
            <Intro/>
            <WorkshopTB/>
            <Info/>
            <End/>
            {/* <Footer/> */}
        </div>
    )
}
natpacanSri.getLayout = (natpacanSri) => <Layout>{natpacanSri}</Layout>;
export default natpacanSri;