import React, { useState } from 'react'

import Cal from 'src/components/johnny/calculator/cal'
import Cvb from 'src/components/johnny/calculator/cvb'

export default function calculator() {

  const [showCalculator, setShowCalculator] = useState(true);

  const handleTabClick = (tab) => {
    setShowCalculator(tab === 'calculator');
  };

  return (

    <div className='min-h-screen bg-blue-100 flex'>
      <div className="m-auto">

        {/* tab option */}
        <ul className=" text-sm font-medium text-center text-gray-500 rounded-lg shadow flex mb-5">
          <li className={`w-full ${showCalculator ? 'border-r border-gray-200' : ''}`}>
            <button
              onClick={() => handleTabClick('calculator')}
              className={`inline-block w-full p-4 bg-white border-s-0 border-gray-200 rounded-s-lg hover:text-gray-700 hover:bg-gray-50`}
              aria-current={`${showCalculator ? '"page"' : ''}}`}
            >
              Calculator
            </button>
          </li>
          <li className={`w-full ${!showCalculator ? 'border-l border-gray-200' : ''}`}>
            <button
              onClick={() => handleTabClick('convertBase')}
              className={`inline-block w-full p-4 bg-white border-s-0 border-gray-200 rounded-e-lg hover:text-gray-700 hover:bg-gray-50`}
              aria-current={`${showCalculator ? '' : 'page'}}`}
            >
              Convert Base
            </button>
          </li>
        </ul>

      {showCalculator ? <Cal /> : <Cvb />}


      </div>
    </div>

  )
}