import React from 'react'
import { Container, Box, Typography } from '@mui/material'


//* import components
import Nav from "src/components/johnny/workshop2/nav"
import Footer from "src/components/johnny/workshop2/footer"

export default function layout({ children }) {
    return (
        <Box sx={{ padding: 0, margin: 0  }}>
            <Nav />
            {children}
            <Footer />
        </Box>
    )
}
