import React from 'react'

import {
    Box,
    Grid,
} from "@mui/material"

//* import components
import Layout from '../layout'
import Nav from "src/components/johnny/workshop2/nav"
import Banner from 'src/components/johnny/workshop2/banner'
import Card from 'src/components/johnny/workshop2/card'
import Footer from "src/components/johnny/workshop2/footer"


export default function () {

    const cardItems = [
        {   
            title:"10 TRENDS HOME DECORATION For Best House",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/57e1d14a4f52a514f1dc8460962e33791c3ad6e04e507441722a72dd9e44c0_640.jpg'
        },
        {
            title:"HOW TO CLEANING OWN HOME For Best Cleaning",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/55e2dc4a4b52a414f1dc8460962e33791c3ad6e04e5074417d2d73dc9444c7_640.jpg'

        },
        {
            title:"TIPS CHOICE BEST AGENCY For House Decoration",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/57e9d5434c54af14f1dc8460962e33791c3ad6e04e507440752f78d79e45c5_640.jpg'

        },
        {
            title:"HOW TO DRAW HOME DESIGN For Consystency",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/free-to-use-sounds-GKny6GKQ_QM-unsplash.jpg'

        },
        {
            title:"TIPS MODELING DECORATION HOME modern",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/52e0d4404e57ac14f1dc8460962e33791c3ad6e04e507440722d72d0914bc5_640.jpg'

        },
        {
            title:"HOW TO IMPROVE HOME DESIGN For Future",
            detail:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            cardPic:'https://randomwordgenerator.com/img/picture-generator/51e6dd454d4faa0df7c5d57bc32f3e7b1d3ac3e45551754877297cd69e_640.jpg'

        },
    ]

    return (
        <Layout>
            <Banner titleName="BLOG" />
            <Grid container spacing={{ xs: 4, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }} sx={{marginBottom:'-60px'}}>
            {cardItems.map((card,index) => (
                <Grid item xs={2} sm={4} md={4} key={index} >
                    <Card cardPic={card.cardPic} cardTitle={card.title} cardDetail={card.detail}  />
                </Grid>
            ))}
            </Grid>
        </Layout>
    )

}