import React from 'react'
import { Box, Typography, Button } from '@mui/material'

//* import components
import Layout from '../layout'
import Nav from "src/components/johnny/workshop2/nav"
import Footer from "src/components/johnny/workshop2/footer"


export default function index() {
    return (
        <>
            <Box sx={{backgroundColor:"#19A7CE"}}>
                <Layout >
            <Box
                sx={{
                    backgroundColor: "#19A7CE",
                    minHeight:'80vh',
                    display:'flex'
                }}
            >
                <Box sx={{margin:'auto', textAlign:'center'}}>
                    <Typography sx={{fontSize:'3.5em', fontWeight:"900",color:'white'}} >ERROR 404</Typography>
                <Typography sx={{fontWeight:"800",fontSize:'1.2em',color:'#e5e7eb'}}>Page not found, please go back</Typography>
                <Button href='./' variant="contained" sx={{ marginTop:'15px', borderRadius:"8px" }}>
                    Back to Home
                </Button>
                </Box>
                
            </Box>
        </Layout>
            </Box>
            
        
        </>
        

    )
}
