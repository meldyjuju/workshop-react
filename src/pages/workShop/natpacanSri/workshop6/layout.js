import React from 'react'
import {
  Container,
  Box
} from '@mui/material'
import Nav from 'src/components/johnny/workshop6/nav'


export default function layout({children}) {
  return (
    <Box>
        <Nav/>
        {children}
    </Box>
  )
}
