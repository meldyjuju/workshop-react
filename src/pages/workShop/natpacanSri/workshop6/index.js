import React from 'react'
//* import component
import Layout from './layout'
import Itemlist from 'src/components/johnny/workshop6/itemlist'

export default function index() {
    return (
        <>
        <Layout>
            <Itemlist/>
        </Layout>
        </>
    )
}
