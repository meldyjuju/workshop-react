import { AppBar, Avatar, Box, Button, Card, CardContent, Container, Grid, Link, Toolbar, Typography } from "@mui/material";
import BackButton from "src/components/waiwitz/backButton";
import LogoutIcon from '@mui/icons-material/Logout';
import Head from "next/head";
import { useEffect, useState } from "react";


const Logout = () => {
    localStorage.removeItem('Token');
    localStorage.removeItem('User');
    window.location.href = '/workShop/waiwitz/workshopAPI/workshopLogin';
}


const userProfile = () => {
    const jwt = localStorage.getItem('Token');
    const [userData, setUserData] = useState(null)
    try {
        useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_API_USER_URL}`, {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${jwt}`,
                    'Content-Type': 'application/json'
                },
            }).then(async (res) => {
                if (res.ok) {
                    const data = await res.json()
                    setUserData(true);
                    localStorage.setItem('User', JSON.stringify(data.user));
                    console.info('request :', res);
                    console.info('เข้าสู่ระบบแล้ว');
                    console.log(`ข้อมูลผู้ใช้ : `, await JSON.parse(localStorage.getItem('User')));
                } else {
                    localStorage.removeItem('Token');
                    localStorage.removeItem('User');
                    console.error('สถานะ: ', res.statusText)
                    if (!userData) console.info('ยังไม่ได้เข้าสู่ระบบหรือ token timeout')
                    // alert('ยังไม่ได้เข้าสู่ระบบ')
                }
            });
        }, [0])


        const user = JSON.parse(localStorage.getItem('User'));
        return (
            <>
                <Head>
                    <title>profile user || workshop 4</title>
                </Head>
                <BackButton />
                {userData ?
                    <>
                        <Box sx={{ flexGrow: 0.2 }}>
                            <AppBar sx={{ bgcolor: '#fff' }}>
                                <Toolbar>
                                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                                    </Typography>
                                    <Button color="inherit" onClick={Logout} sx={{ bgcolor: '#a70000', borderRadius: '0' }}>
                                        <LogoutIcon />
                                        Logout</Button>
                                </Toolbar>
                            </AppBar>
                        </Box>
                        <Container maxWidth='md'>
                            <Box>
                                <Card>
                                    <CardContent>
                                        <Grid container spacing={5} justifyContent={'space-evenly'}>
                                            <Grid item>
                                                <b>Username :</b> {user.fname} <br />
                                                <b>Name :</b> {user.fname} {user.lname} <br />
                                                <b>email :</b> {user.email}
                                            </Grid>
                                            <Grid item>
                                                <Avatar alt="profile" src={user.avatar} sx={{ width: '150px', height: '150px', margin: 'auto', boxShadow: '0 10px 20px #88888879' }} />
                                            </Grid>
                                        </Grid>
                                    </CardContent>
                                </Card>
                            </Box>
                        </Container>
                    </>
                    :
                    <Box margin={'auto'} textAlign={'center'}>
                        <Typography variant="h1">ยังไม่ได้เข้าสู่ระบบ 👁️👄👁️</Typography>
                        <Link href='/workShop/waiwitz/workshopAPI/workshopLogin'>
                            <Button variant="contained" sx={{ marginTop: '2em', width: '100%' }}>ไปหน้าเข้าสู่ระบบ</Button>
                        </Link>
                    </Box>
                }
            </>
        )

    } catch (error) {
        console.error(`มีข้อผิดพลาด : ${error}`)
    }
}

export default userProfile;