
# Chanoknan Miw 's Workshop 
    ยินดีเข้าสู่โปรเจค Chanoknan's Workshop  ของฉันที่มี workshop มากมายที่ได้ปฏิบัติเเละฝึกฝนตลอด 3 อาทิตย์ที่ผ่านมาค่ะ


# Gitlab Fork Project 

- `Fork` : เราจะ Fork โปรเจกต์จากเซิร์ฟเวอร์หลักไปยัง GitLab ของตัวเอง ฉะนั้นเราต้องเข้าไปที่ GitLab ของ เซิร์ฟเวอร์หลัก เเละกด Fork มันจะมีให้เลือก User ให้เลือกเป็นชื่อเราเเละกด Fork มา

- `Clone` : เปิด  Terminal หรือ Command Prompt บนเครื่องคอมพิวเตอร์ของคุณใช้คำสั่ง git clone <URL> เพื่อ Clone โปรเจกต์ลงในเครื่องคอมพิวเตอร์ของคุณ. URL คือลิงก์ที่คุณ Fork มา

- `cd <ชื่อโฟลเดอร์>` : เข้าสู่โฟลเดอร์ของโปรเจกต์ที่คุณ Clone มาโดยใช้คำสั่ง cd <ชื่อโฟลเดอร์>

- `Remote-v` : ตรวจสอบ Remote repository ที่คุณมี, ให้ใช้คำสั่ง git remote -v แน่ใจว่าคุณมี Remote เพียงพอสำหรับ Fetch และ Push ข้อมูล

- `Remote Add Upstream` : ใช้คำสั่ง git remote add upstream <URL> เพื่อติดตามการเปลี่ยนแปลงจากโปรเจกต์หลัก 

- `Branch` : เพื่อตรวจสอบ Branch ปัจจุบันที่คุณอยู่, ใช้คำสั่ง git branch

- `Branch New` : สร้าง Branch ใหม่โดยใช้คำสั่ง git branch <new>

- `Check Out` : เปลี่ยนไปยัง Branch ใหม่ที่คุณเพิ่งสร้างโดยใช้คำสั่ง git checkout <ชื่อที่ตั้ง>

**Edit Code**


- `Edit Code` : ทำการแก้ไขโค้ดใน branch ใหม่ที่เพิ่งสร้าง


- `Git Commit` : ทำการ Commit การเปลี่ยนแปลงที่คุณได้ทำไปโดยใช้คำสั่ง git commit เเละ comment สิ่งที่เราทำลงไป ก็จะเป็น git Commit -a -m "คอมเม้นสิ่งที่ทำไป"


- `Git Push Origin` : ทำการ Push การเปลี่ยนแปลงของคุณไปยัง Branch ใหม่ของตัวเองที่คุณเพิ่งสร้าง, ใช้คำสั่ง git push origin <ชื่อที่ตั้ง>


- `Merge requests` : ไปที่ GitLab และเปิด Merge Request จาก branch ใหม่เราไปยัง branch (main) เเละอธิบายสิ่งที่เราทำไป 
## Gitlab
 - [gitlab](https://about.gitlab.com/)

## Installation
 - [React](https://react.dev/learn/installation)
 - [Nextjs](https://nextjs.org/docs/getting-started/installation)
 - [Material UI](https://mui.com/material-ui/getting-started/installation/)
 - [Tailwind](https://tailwindcss.com/docs/guides/create-react-app)

### Install Project
    npm install
    // เพื่อติดตั้ง dependencies ที่ระบุใน package.json. ขั้นตอนนี้ควรทำหลังจากที่คุณ clone โปรเจคมาแล้ว
### Run Project
#### Next.js ใช้คำสั่ง:
    npm run dev
    //นำเบราว์เซอร์เข้าไปที่ http://localhost:3000 เพื่อดูโปรเจคที่ทำ

# Figma
- [@Figma](https://www.figma.com/file/JbV1tKPt8CZXQdgp5EghEd/Home-Interior-Design-Website-Wireframe-(Community)?type=design&node-id=0-1&mode=design)

# WorkShop2  BLOG 
#### มีทั้งหมด  5 component 

WorkShop นี้ จะดึง Component 4 ตัวมากับใช้ทั้ง 2 page ทั้ง BLOG เเละ SINGLE  POST ได้เเก่ Component 
    Navui
    Footui
    Bodyui1
    Bodyui2
มาเริ่มกันที่ 
### Navui ตกเเต่ง
**1. AppBar:**
```http
<AppBar position="static" sx={{ backgroundColor: "#9AA090 " }}>

//AppBar เป็น component ของ Material-UI ที่ใช้สร้าง Navbar ด้านบนของหน้า
```
**2. Toolbar:**
```http
<Toolbar>

//Toolbar เป็น component ของ Material-UI ที่ใช้เก็บ element ต่าง ๆ ที่อยู่ใน Navbar
```
**3. Logo "HOMCO":**
```http
<Typography variant="h6" sx={{ flexGrow: 1, fontSize: "2rem" }}>
  <WidgetsIcon
  sx={{ fontSize: '2.5rem' }}/>
  HOMCO
</Typography>

//ใช้ Typography สำหรับแสดงข้อความ "HOMCO" พร้อมกับไอคอน WidgetsIcon เป็น logo
```
**4. ปุ่ม Navigation:**
```http
<Button color="inherit" >
  HOME
</Button>
<Button color="inherit" >
  ABOUT ME
</Button>
<Button color="inherit" >
  OUR SERVICES
</Button>

//ปุ่ม Navigation สำหรับ Home, About Me, และ Our Services

```

**5. Dropdown สำหรับ Our Projects:**
```http
<Button color="inherit" onClick={handleClick}>
  OUR PROJECTS
  <IconButton
    size="small"
    edge="end"
    aria-label="dropdown"
    aria-controls="dropdown-menu"
    aria-haspopup="true"
    onClick={handleClick}
    color="inherit"
  >
    <ExpandMoreIcon />
  </IconButton>
</Button>

//ปุ่ม Our Projects มี Dropdown โดยใช้ IconButton และไอคอน ExpandMoreIcon เพื่อแสดงให้ทราบถึงการมีเมนูย่อย
```
**6. ปุ่ม Navigation สำหรับ PORTFOLIO:**
```http
<Button color="inherit" >
  PORTFOLIO
</Button>

```
**7. Dropdown สำหรับ PAGES:**
```http
<Button color="inherit" onClick={handleClick}>
  PAGES
  <IconButton
    size="small"
    edge="end"
    aria-label="dropdown"
    aria-controls="dropdown-menu"
    aria-haspopup="true"
    onClick={handleClick}
    color="inherit"
  >
    <ExpandMoreIcon />
  </IconButton>
</Button>

//ปุ่ม PAGES มี Dropdown ในลักษณะเดียวกับ Our Projects

```

**8. กล่องเมนู Dropdown:**
```http
<Menu id="dropdown-menu" anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
  <MenuItem onClick={handleClose}>Dropdown Item 1</MenuItem>
  <MenuItem onClick={handleClose}>Dropdown Item 2</MenuItem>
  <MenuItem onClick={handleClose}>Dropdown Item 3</MenuItem>
</Menu>

//Menu เป็น component ของ Material-UI ที่ใช้สร้างเมนู dropdown
//MenuItem สำหรับรายการเมนูที่จะแสดงใน dropdown
```
**9. MenuIcon:**
```http
<MenuIcon />

//การใช้ MenuIcon อาจจะไม่จำเป็นที่นี่ เนื่องจากมันไม่ถูกนำมาแสดงผลในที่นี้.
```
**10. ฟังก์ชัน handleClick และ handleClose:**
```http
const handleClick = (event) => {
  setAnchorEl(event.currentTarget);
};

const handleClose = () => {
  setAnchorEl(null);
};

//handleClick: ใช้เพื่อเปิด dropdown เมื่อปุ่มถูกคลิก
//handleClose: ใช้เพื่อปิด dropdown เมื่อเมนูถูกเลือกหรือทำปิด dropdown
```
**11. Export Component:**
```http
export default Navui;
ส่งออก Navui component เพื่อนำไปใช้ในส่วนอื่น ๆ 
```




### Footui ตกเเต่ง

**1. FooterContainer:**
```http
const FooterContainer = styled("footer")({
  backgroundColor: "#f2eddd",
  padding: "70px 0",
  marginTop: "auto",
});

//FooterContainer เป็น styled component ที่สร้างขึ้นจาก footer HTML element ด้วย CSS-in-JS styling
//กำหนดสีพื้นหลัง, padding, และ marginTop
```
**2. Footui Function Component:**
```http
const Footui = () => {
  return (
    <FooterContainer>
      <div style={{ width: "100%" }}>
        <Box sx={{ display: "grid", gridTemplateColumns: "repeat(3, 1fr)", marginX: "6rem", marginY: "7rem" }}>
          {/* Content for INFORMATION */}
          <Typography variant="h6">
            INFORMATION
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              <br />
              adipiscing elit.Ut elit tellus, luctus
              <br />
              consectetur adipiscing elit.
            </Typography>
          </Typography>

          {/* Content for NAVIGATION */}
          <Typography variant="h6">NAVIGATION</Typography>

          {/* Content for CONTACT US */}
          <Box>
            <Typography variant="h6">CONTACT US</Typography>
            <TextField label="Enter your email" variant="outlined" fullWidth margin="normal" color="primary"/>
            <Button variant="contained" color="primary" >
              Subscribe
            </Button>
          </Box>
        </Box>
      </div>
    </FooterContainer>
  );
};

//Footui เป็น Function Component ที่สร้าง footer ของเว็บไซต์.
//ใช้ FooterContainer เป็น wrapper สำหรับข้อมูลทั้งหมดใน Footer.
```
**3. ส่วนของ "INFORMATION":**
```http
<Typography variant="h6">
  INFORMATION
  <Typography>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
    <br />
    adipiscing elit.Ut elit tellus, luctus
    <br />
    consectetur adipiscing elit.
  </Typography>
</Typography>

//ในส่วนนี้แสดงข้อความ "INFORMATION" พร้อมกับข้อความรายละเอียด
```
**4. ส่วนของ "NAVIGATION":**
```http
<Typography variant="h6">NAVIGATION</Typography>

//ในส่วนนี้แสดงข้อความ "NAVIGATION"
```
**5. ส่วนของ "CONTACT US":**
```http
<Box>
  <Typography variant="h6">CONTACT US</Typography>
  <TextField label="Enter your email" variant="outlined" fullWidth margin="normal" color="primary"/>
  <Button variant="contained" color="primary" >
    Subscribe
  </Button>
</Box>

//ในส่วนนี้มีช่องให้กรอกอีเมล (TextField) และปุ่ม Subscribe (Button)
```
**6. Export Component:**
```http
export default Footui;
//ส่งออก Footui component เพื่อนำไปใช้ในส่วนอื่น ๆ 
```



### Bodyui1 ตกเเต่ง

**1. Bodyui1 Function Component:**
```http
const Bodyui1 = () => {
  return (
    <div style={{ backgroundColor: "#78909c", padding: "100px" }}>
      <Typography
        variant="h6"
        sx={{
          color: "white",
          margin: "10px",
          display: "block",
          textAlign: "left",
          fontWeight: "bold",
          fontSize: "50px",
        }}
      >
        LETS CHANGE YOUR OWN HOME <br />
        INTERIOR DESIGN NOW{" "}
      </Typography>
      <Button variant="contained" color="primary">
        CONTACT US
      </Button>
    </div>
  );
};

//Bodyui1 เป็น Function Component ที่รวมเนื้อหาของส่วนต่าง ๆ ของหน้าเว็บ
```

**2. ส่วนของข้อความ:**
```http
<Typography
  variant="h6"
  sx={{
    color: "white",
    margin: "10px",
    display: "block",
    textAlign: "left",
    fontWeight: "bold",
    fontSize: "50px",
  }}
>
  LETS CHANGE YOUR OWN HOME <br />
  INTERIOR DESIGN NOW{" "}
</Typography>

//ในส่วนนี้แสดงข้อความ "LET'S CHANGE YOUR OWN HOME INTERIOR DESIGN NOW" ในลักษณะ Heading ขนาดใหญ่
```
**3. ปุ่ม "CONTACT US":**
```http
<Button variant="contained" color="primary">
  CONTACT US
</Button>

//ปุ่ม "CONTACT US" ที่มีลักษณะเป็นปุ่มที่สีพื้นหลังเป็นสี primary
```
**4. Export Component:**
```http
export default Bodyui1;

ส่งออก Bodyui1 component เพื่อนำไปใช้ในส่วนอื่น ๆ 
```

### Bodyui2 ตกเเต่ง

**1. Bodyui2 Function Component:**
```http
const Bodyui2 = () => {
  return (
    <Box
      sx={{
        height: "100px",
        backgroundColor: "white",
        gridTemplateColumns: "repeat(4, 1fr)",
        display: "grid",
        paddingLeft: "10rem",
      }}
    >
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 1"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 2"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 3"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 4"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
    </Box>
  );
};


//Bodyui2 เป็น Function Component ที่รวมเนื้อหาของส่วนนี้
```

**2. Container สำหรับแสดงรูปภาพ:**
```http
const Bodyui2 = () => {
  return (
    <Box
      sx={{
        height: "100px",
        backgroundColor: "white",
        gridTemplateColumns: "repeat(4, 1fr)",
        display: "grid",
        paddingLeft: "10rem",
      }}
    >
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 1"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 2"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 3"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
      <img
        src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
        alt="Image 4"
        style={{ width: "25%", height: "100%", borderRadius: "20%" }}
      />
    </Box>
  );
};


//Box ถูกใช้เพื่อกำหนดคุณสมบัติต่าง ๆ ของ Container ที่ใช้ในการแสดงรูปภาพ
//ใช้ display: "grid" เพื่อให้รูปภาพถูกแสดงในลักษณะของ Grid
//gridTemplateColumns: "repeat(4, 1fr)" ให้แสดงเป็น Grid 4 คอลัมน์
```

**3. รูปภาพ:**
```http
<img
  src="https://i.pinimg.com/564x/f6/9e/92/f69e926757d7b8f14e995a8b934103c1.jpg"
  alt="Image 1"
  style={{ width: "25%", height: "100%", borderRadius: "20%" }}
/>
// ... (รูปภาพทั้ง 4)


///นำเข้ารูปภาพและกำหนดคุณสมบัติต่าง ๆ ของรูปภาพ เช่น width, height, borderRadius.
```

**4. Export Component:**
```http
export default Bodyui2;
//ส่งออก Bodyui2 component เพื่อนำไปใช้ในส่วนอื่น ๆ 
```


### Bodyui0
**1. Bodyui0 Function Component:**
```http
const Bodyui0 = () => {
  return (
    <>
      {/* กลุ่มแรกของ Card */}
      <div
        className="pt-16"
        style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}
      >
        {/* การ์ดที่ 1 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          <CardContent style={{ padding: "24px" }}>
            <Box
              sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
            ></Box>
            <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
              10 TRENDS HOME DECORATION <br />
              FOR BEST HOUSE
            </Typography>
            <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua.
            </Typography>
          </CardContent>
        </Card>

        {/* การ์ดที่ 2 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          {/* ... เนื้อหาในการ์ด 2 */}
        </Card>

        {/* การ์ดที่ 3 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          {/* ... เนื้อหาในการ์ด 3 */}
        </Card>
      </div>

      {/* กลุ่มที่ 2 ของ Card */}
      <div
        className="pt-16"
        style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}
      >
        {/* การ์ดที่ 4 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          {/* ... เนื้อหาในการ์ด 4 */}
        </Card>

        {/* การ์ดที่ 5 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          {/* ... เนื้อหาในการ์ด 5 */}
        </Card>

        {/* การ์ดที่ 6 */}
        <Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
          {/* ... เนื้อหาในการ์ด 6 */}
        </Card>
      </div>
    </>
  );
};

//Bodyui0 เป็น Function Component ที่รวมเนื้อหา
```
**2.  การ์ด:**
```http
<Card sx={{ width: "auto", mx: "auto", backgroundColor: "", margin: "0 8px" }}>
  <CardContent style={{ padding: "24px" }}>
    {/* กล่องสี่เหลี่ยมสีเทา */}
    <Box
      sx={{ width: "auto", margin: "auto", padding: "165px", backgroundColor: "#bdbdbd" }}
    ></Box>
    {/* ข้อความ */}
    <Typography variant="h6" sx={{ color: "#455a64", fontWeight: "bold", pt: 5 }}>
      10 TRENDS HOME DECORATION <br />
      FOR BEST HOUSE
    </Typography>
    <Typography sx={{ color: "#455a64", paddingTop: "16px" }}>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
      incididunt ut labore et dolore magna aliqua.
    </Typography>
  </CardContent>
</Card>

//การ์ดที่ 2 ถึง 6: เหมือนกันหมด 
```
**3. Export Component:**
```http
export default Bodyui0;
//ส่งออก Bodyui0 component เพื่อนำไปใช้ในส่วนอื่น ๆ 
```


## index.js BLOG page
**1. Navui:**
```http
<Navui />

//เรียกใช้ component Navui ที่เป็น Navbar ของแอปพลิเคชัน.
```
**2. ส่วน Header:**
```http

<div style={{ backgroundColor: "#f2eddd", padding: "100px" }}>
  <Typography
    variant="h1"
    sx={{
      color: "#455a64",
      margin: "10px",
      display: "block",
      textAlign: "left",
      fontWeight: "bold",
      fontSize: "80px",
    }}
    className="text-9xl "
  >
    BLOG
    <Typography variant="body1">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, lutus
      <br />
      nec ullamcorper mattis, pulvinar dapibus leo.
    </Typography>
  </Typography>
</div>

//ส่วนนี้คือส่วน Header ของหน้า UI โดยมีข้อความ "BLOG" และคำอธิบายที่เป็น Lorem Ipsum.
//ใช้ Typography สำหรับการแสดงข้อความ.
```

**3. Bodyui0:**
```http
<div className="pb-40">
  <Bodyui0 />
</div>

//เรียกใช้ Bodyui0 component.
```

**4. ส่วน Box สำหรับ Bodyui1 และ Bodyui2:**
```http
<Box
  sx={{
    position: "relative",
    width: "100%",
    height: "200px",
    backgroundColor: "#f2eddd",
  }}
>
  <Box
    sx={{
      marginX: "5rem",
      marginBottom: "5rem",
      position: "absolute",
      top: "0",
      left: "0",
      bottom: "0",
      right: "0",
      transform: "translate(0,-5em)",
      zIndex: 2,
    }}
  >
    <Bodyui1 />
  </Box>

  <Box
    sx={{
      marginX: "12rem",
      position: "absolute",
      top: "0",
      left: "0",
      bottom: "0",
      right: "0",
      transform: "translate(0,-8em)",
      zIndex: 30,
    }}
  >
    <Bodyui2 />
  </Box>
</Box>

//ส่วนนี้ใช้ Box component ของ Material-UI เพื่อจัดวาง Bodyui1 และ Bodyui2 ในตำแหน่งที่ต้องการ
```
**5. Footui:**
```http
<Footui />

//เรียกใช้ Footui component ที่เป็น Footer ของหน้า UI
```
**6. Export Component:**
```http
export default ui;
//ส่งออก ui component เพื่อนำไปใช้ในส่วนอื่น ๆ ของแอป
```

# WorkShop2  SINGLE  POST
### Bodyii0 ตกเเต่ง

**1. Bodyii0 Function Component:**
```http
    const Bodyii0 = () => {
  return (
    <>
      <Box style={{ marginX: "30", marginTop: 10 }}>
        <Box
          style={{
            backgroundColor: "white",
            display: "flex",
            alignItems: "center",
            padding: "20px", // Add padding for better spacing
          }}
        >
          {/* Image on the left */}
          <img
            src="https://i.pinimg.com/564x/1b/af/a9/1bafa9831e07cbea82784805d096c0e0.jpg"
            alt="img"
            style={{ width: "30%", marginRight: "20px" }}
          />

          {/* Content on the right */}
          <Grid>
            <Box
              variant="h1"
              sx={{
                color: "black",
                display: "block",
                textAlign: "left",
                fontWeight: "bold",
                fontSize: "40px",
              }}
            >
              LET'S CHANGE YOUR OWN HOME
            </Box>
            <Box
              variant="body1"
              sx={{
                color: "black",
                marginBottom: "8rem",
                display: "block",
                textAlign: "left",
                fontSize: "13px",
              }}
            >
              {/* Long content text */}
            </Box>
          </Grid>
        </Box>

        {/* Additional content */}
        <Box>
          <Typography variant="body1" sx={{ marginY: "1rem", marginX: "1rem", fontSize: "12px" }}>
            {/* Additional content text */}
          </Typography>
        </Box>
      </Box>
    </>
  );
};

```
**2. เนื้อหาของ Bodyii0 : มีโครงสร้างหลัก ๆ แบ่งเป็นส่วนย่อย ๆ**

  - ส่วนภาพทางซ้าย:
```http
<img
  src="https://i.pinimg.com/564x/1b/af/a9/1bafa9831e07cbea82784805d096c0e0.jpg"
  alt="img"
  style={{ width: "30%", marginRight: "20px" }}
/>

```

  - ส่วนเนื้อหาทางขวา:
```http
 <Grid>
  <Box
    variant="h1"
    sx={{
      color: "black",
      display: "block",
      textAlign: "left",
      fontWeight: "bold",
      fontSize: "40px",
    }}
  >
    LET'S CHANGE YOUR OWN HOME
  </Box>
  <Box
    variant="body1"
    sx={{
      color: "black",
      marginBottom: "8rem",
      display: "block",
      textAlign: "left",
      fontSize: "13px",
    }}
  >
    {/* Long content text */}
  </Box>
</Grid>
   
```
  - ส่วนเนื้อหาเพิ่มเติม:
```http
<Box>
  <Typography variant="body1" sx={{ marginY: "1rem", marginX: "1rem", fontSize: "12px" }}>
    {/* Additional content text */}
  </Typography>
</Box>
    
//โค้ดของ Bodyii0 ประกอบไปด้วยภาพทางซ้ายและเนื้อหาทางขวา ซึ่งประกอบด้วยข้อความ
```
**4. Export Component:**
```http
export default Bodyii0;

//ส่งออก Bodyii0 component เพื่อนำไปใช้ในส่วนอื่น ๆ
```



## index.js  SINGLE  POST page

**1. ui2 Function Component:**
```http
const ui2 = () => {
  return (
    <>
      <Navui />
      <div style={{ backgroundColor: "#f2eddd", padding: "100px" }}>
        <Typography
          variant="h1"
          sx={{
            color: "#455a64",
            margin: "10px",
            display: "block",
            textAlign: "left",
            fontWeight: "bold",
            fontSize: "80px",
          }}
          className="text-9xl "
        >
          SINGLE  POST
          <Typography variant="body1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, lutus
            <br />
            nec ullamcorper mattis, pulvinar dapibus leo.
          </Typography>
        </Typography>
      </div>
      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "200px",
          backgroundColor: "white",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-4em)",
            zIndex: 2,
          }}
        >
          <Bodyii0 />
        </Box>
      </Box>

      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "100px",
          backgroundColor: "white",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+32em)",
            zIndex: 2,
          }}
        >
          <Bodyui1 />
        </Box>

        <Box
          sx={{
            marginX: "12rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+30em)",
            zIndex: 30,
          }}
        >
          {" "}
          <Bodyui2 />
        </Box>
        <Box
          sx={{
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+46em)",
          }}
        >
          <Footui />
        </Box>
      </Box>
    </>
  );
};

export default ui2;
    
```
**2. เนื้อหาของ ui2:**
- ui2 เป็น Function Component ที่ประกอบไปด้วยหลายส่วนดังนี้:
- ส่วน Navbar (Navui):
```http
    <Navui />
```
- ส่วน Header:
```http
<div style={{ backgroundColor: "#f2eddd", padding: "100px" }}>
  {/* Header content */}
</div>
```
- ส่วนแสดงผล Body (Bodyii0, Bodyui1, Bodyui2):
```http
<Box
  sx={{
    position: "relative",
    width: "100%",
    height: "200px",
    backgroundColor: "white",
  }}
>
  {/* Bodyii0 component */}
</Box>

<Box
  sx={{
    position: "relative",
    width: "100%",
    height: "100px",
    backgroundColor: "white",
  }}
>
  {/* Bodyui1 component */}
  {/* Bodyui2 component */}
</Box>

```
- ส่วน Footer (Footui):
```http
<Box
  sx={{
    position: "absolute",
    top: "0",
    left: "0",
    bottom: "0",
    right: "0",
    transform: "translate(0,+46em)",
  }}
>
  {/* Footer content */}
</Box>

```
**4. ui2 Function Component:**
```http
    export default ui2;
//ส่งออก ui2 component เพื่อนำไปใช้ในส่วนอื่น ๆ
```


# WorkShop3

#### component ในหน้านี้  มีทั้งหมด  1 component ด้วยกัน
```http
import Login from "src/components/miw/Login";

const index = () => {
 
  return <>
  <Login/>
  </>
};

export default index;
```
- คำสั่ง Import:
```http
import Login from "src/components/miw/Login";
```
การนำเข้าคอมโพเนนต์ Login จากที่อยู่ไฟล์ที่ระบุ. มันถือว่า Login จะต้องอยู่ในไดเรกทอรี src/components/miw/.

- คอมโพเนนต์แบบฟังก์ชัน (Index):
```http
const index = () => {
  // แสดงผลคอมโพเนนต์ Login
  return (
    <>
      <Login />
    </>
  );
};
```
อันนี้คือคอมโพเนนต์แบบฟังก์ชันที่ชื่อ index. มันแสดงผลคอมโพเนนต์ Login. สัญลักษณ์ <> และ </> เป็นการย่อสำหรับ React fragment ซึ่งช่วยให้คุณรวมกลุ่มลูกโหลดหลาย ๆ ตัวโดยไม่ต้องเพิ่มโหนดเพิ่มเติมใน DOM
- คำสั่งส่งออก:
```http
export default index;
```
บรรทัดนี้ส่งออกคอมโพเนนต์ index เป็น default export. นี่หมายความว่าหากไฟล์อื่น ๆ นำเข้าจากไฟล์นี้ จะได้รับ index ในลักษณะค่าเริ่มต้น

 `<Login/>` เป็นฟังก์ชัน component สำหรับเเสดงหน้า Login 

#### ต่อไปนี้จะอธิบายเเสดงโค้ด component หน้า <Login/> ซึ่งแสดงถึงแบบฟอร์มเข้าสู่ระบบ (Login form) 
- การกำหนดค่าเริ่มต้นของ State :
```http
const [username, setUsername] = useState("");
const [password, setPassword] = useState("");
```
ใช้ Hook useState เพื่อสร้างตัวแปร state username และ password พร้อมกับฟังก์ชั่น setter ของแต่ละตัวแปรนั้น ๆ

- การกำหนดค่าเริ่มต้นของ Router:
```http
const { push } = useRouter();
```
ใช้ Hook useRouter จาก Next.js เพื่อรับฟังก์ชั่น push ซึ่งในภายหลังจะถูกใช้ในการเปลี่ยนหน้า

- ฟังก์ชั่นการจัดการการเข้าสู่ระบบ:

```http
const handleLogin = async () => {
  // ตรวจสอบว่าชื่อผู้ใช้หรือรหัสผ่านมีค่าว่างหรือไม่
  if (username === "" || password === "") {
    console.error("Error: กรุณากรอกชื่อผู้ใช้และรหัสผ่าน");
    return;
  }

```
```http
 try {
    // พยายามเข้าสู่ระบบโดยทำการ POST request ไปที่ API endpoint ที่ระบุ
    const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username,
        password,
      }),
    });
```
```http
    // ถ้า response ไม่สำเร็จ (เช่น การตรวจสอบสิทธิ์ล้มเหลว) จะจัดการกับข้อผิดพลาด
    if (!response.ok) {
      const errorData = await response.json();
      console.log(errorData);
      throw new Error(`เข้าสู่ระบบล้มเหลว: ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง ${response.status}`);
    }
```
```http
    // หากเข้าสู่ระบบสำเร็จ จะเก็บ JWT token ใน local storage
    const data = await response.json();
    localStorage.setItem("jwt", data.accessToken);
```
```http
    // เปลี่ยนหน้าไปที่หน้าที่กำหนดเมื่อเข้าสู่ระบบสำเร็จ
    push("/workShop/chanoknan/workshop4");
```   
```http
  } catch (error) {
    // บันทึกข้อมูลเพิ่มเติมเกี่ยวกับข้อผิดพลาด
    if (error instanceof Error) {
      console.error("Error Message:", error.message);
    } else {
      console.error("Error: เข้าสู่ระบบล้มเหลว", error);
    }
  }
};
```

ฟังก์ชั่นนี้จะถูกเรียกเมื่อคลิกที่ปุ่ม "Login" และทำการตรวจสอบว่าชื่อผู้ใช้และรหัสผ่านไม่ใช่ค่าว่าง จากนั้นจะทำการส่งคำขอ POST ไปยัง API endpoint ที่ระบุเพื่อทำการเข้าสู่ระบบ หากมีข้อผิดพลาดในการเข้าสู่ระบบ (เช่น ไม่สำเร็จทางด้านการตรวจสอบสิทธิ์) จะจัดการกับข้อผิดพลาดนั้น

- การ Render JSX : คือกระบวนการที่ React ใช้เพื่อแปลงโค้ด JSX (JavaScript XML) เป็นโค้ด JavaScrip 
```http
return (
  <Container component="main" maxWidth="xs">
    {/* ... */}
    <Button fullWidth variant="contained" color="primary" onClick={handleLogin}>
      เข้าสู่ระบบ
    </Button>
    {/* แสดงข้อมูลผู้ใช้หากมีข้อมูลที่ใช้ได้ */}
  </Container>
);
```
Component ทำการ render ฟอร์มที่มี input fields สำหรับ username และ password, รวมถึงปุ่ม login ซึ่งเมื่อคลิกจะเรียกใช้ฟังก์ชั่น handleLogin

โดยรวมแล้ว, component นี้ให้ความสามารถในการเข้าสู่ระบบพื้นฐาน โดยส่งคำขอ POST ไปยัง API, จัดการกับการตรวจสอบสิทธิ์, และเปลี่ยนหน้าเมื่อเข้าสู่ระบบสำเร็จ


 ### .env.local.
 ทำหน้าที่เป็นตัวกำหนดค่าตัวแปร (environment variables) สำหรับโปรเจคของคุณใน Next.js. ตัวแปรสภาพแวดล้อมเหล่านี้จะมีผลกระทบต่อการทำงานของโปรเจคของคุณ ซึ่งตัวอย่างที่ให้มีตัวแปรเดียวคือ NEXT_PUBLIC_API_URL.
```http
NEXT_PUBLIC_API_URL=https://www.melivecode.com/api
```
- `NEXT_PUBLIC_API_URL `คือชื่อตัวแปรสภาพแวดล้อม (environment variable) ที่ถูกกำหนดขึ้นมา
- `https://www.melivecode.com/api ` คือค่าที่ได้กำหนดให้กับตัวแปรนี้
การใช้ NEXT_PUBLIC_ นำหน้าชื่อตัวแปรสภาพแวดล้อม จะทำให้ตัวแปรนี้สามารถเข้าถึงได้จากโค้ดของเราในทุกระดับ (client-side, server-side, static generation)

เมื่อ Next.js ทำการ build โปรเจคหรือรันบนเซิร์ฟเวอร์, ตัวแปรนี้จะถูกนำเข้าใช้แทนที่ตัวแปรในโค้ดของคุณตามตำแหน่งที่คุณใช้ process.env.NEXT_PUBLIC_API_URL

# WorkShop4
#### component ในหน้านี้  มีทั้งหมด  1 component ด้วยกัน
```http
    // Import React library จาก "react"
import React from "react";

// Import คอมโพเนนต์ Workshop4 จากที่อยู่ที่ระบุ
import Workshop4 from "src/components/miw/workshop4";

// นิยาม คอมโพเนนต์ index แบบฟังก์ชัน
const index = () => {
  // แสดงผลคอมโพเนนต์ Workshop4
  return (
   <>
      <Workshop4 />
    </>
  );
};

// ส่งออก คอมโพเนนต์ index เป็น default export
export default index;

```
- คำสั่ง Import:
```http
import React from "react";
import Workshop4 from "src/components/miw/workshop4";

```
-บรรทัดแรกนี้นำเข้าไลบรารี React จาก "react"

-บรรทัดที่สองนำเข้าคอมโพเนนต์ Workshop4 จากที่อยู่ไฟล์ที่ระบุ มันถือว่า Workshop4 จะต้องอยู่ในไดเรกทอรี 'src/components/miw/'

- คอมโพเนนต์แบบฟังก์ชัน (Index):
```http
const index = () => {
  // แสดงผลคอมโพเนนต์ Workshop4
  return (
   <>
      <Workshop4 />
    </>
  );
};

```
นี่คือคอมโพเนนต์แบบฟังก์ชันที่ชื่อ index. มันแสดงผลคอมโพเนนต์ Workshop4. สัญลักษณ์ <> และ </> เป็นการย่อสำหรับ React fragment ซึ่งช่วยให้คุณรวมกลุ่มลูกโหลดหลาย ๆ ตัวโดยไม่ต้องเพิ่มโหนดเพิ่มเติมใน DOM
- คำสั่งส่งออก:
```http
export default index;
```
บรรทัดนี้ส่งออกคอมโพเนนต์ index เป็น default export นี่หมายความว่าหากไฟล์อื่น ๆ นำเข้าจากไฟล์นี้ จะได้รับ index ในลักษณะค่าเริ่มต้น

#### ต่อไปนี้จะอธิบายเเสดงโค้ด component หน้า <Workshop4/>
 
- State :
```http
const [userData, setUserData] = useState(null);
```
userData: เป็น state variable ที่ใช้เก็บข้อมูลผู้ใช้ที่ได้รับจากการเรียก API
setUserData: เป็นฟังก์ชันที่ใช้ในการอัปเดตค่าของ userData

- การใช้ Hook useEffect:
```http
useEffect(() => {
  // ฟังก์ชันที่จะถูกเรียกหลังจาก component ถูก render
  const showData = async () => {
    // เรียก API เพื่อดึงข้อมูลผู้ใช้
    // โดยใช้ token จาก local storage ใน Authorization header
    const userResponse = await fetch("https://www.melivecode.com/api/auth/user", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("jwt")}`,
      },
    });

    if (!userResponse.ok) {
      const errorData = await userResponse.json();
      throw new Error(`Failed to fetch user data: ${userResponse.status} - ${errorData.message}`);
    }

    const userData = await userResponse.json();
    // เก็บข้อมูลผู้ใช้ใน local storage และอัปเดต state
    localStorage.setItem("workshop4", JSON.stringify(userData));
    setUserData(userData);
  };

  // เรียกใช้ฟังก์ชัน showData เมื่อ component ถูก mount
  showData();
}, []);

```
useEffect ใช้เพื่อจัดการกับ side effect หรือการทำงานนอกเหนือจากการ render
ในที่นี้, useEffect ถูกใช้เพื่อดึงข้อมูลผู้ใช้เมื่อ component ถูก mount (ครั้งแรกที่ถูกเรียก)
การใช้ fetch ถูกทำแบบ asynchronous เพื่อดึงข้อมูลผู้ใช้จาก API
ข้อมูลผู้ใช้ถูกเก็บไว้ใน state userData และใน local storage


- ฟังก์ชัน logOut:
```http
const logOut = () => {
  // ลบ token และข้อมูลผู้ใช้ที่เก็บไว้ใน local storage
  localStorage.removeItem("jwt");
  localStorage.removeItem("workshop4");
  // นำผู้ใช้กลับไปยังหน้า Workshop3
  push("/workShop/chanoknan/workshop3");
};

```
logOut: เป็นฟังก์ชันที่ถูกเรียกเมื่อผู้ใช้คลิกที่ปุ่ม "Logout"
ลบ token และข้อมูลผู้ใช้ที่เก็บไว้ใน local storage
ใช้ push จาก useRouter ใน Next.js เพื่อเปลี่ยนเส้นทางไปยังหน้า Workshop3

- การแสดงผล JSX:
```http
return (
  <div>
    {userData && (
      <div>
        <Typography variant="h6">User Information:</Typography>
        <Typography>Username: {userData.user.username}</Typography>
        <Typography>Email: {userData.user.email}</Typography>
        <Typography>First Name: {userData.user.fname}</Typography>
        <Typography>Last Name: {userData.user.lname}</Typography>
        {/* แสดงรูปประจำตัวของผู้ใช้ */}
        {userData.user.avatar && (
          <img
            src={userData.user.avatar}
            alt="User Avatar"
            style={{ width: "300px", height: "300px" }}
          />
        )}

        <Button onClick={logOut}>Logout</Button>
      </div>
    )}
  </div>
);

```
JSX นี้เป็นส่วนที่ใช้ในการแสดงผลบนหน้าเว็บ
มีการใช้ {userData && (...)} เพื่อตรวจสอบว่ามีข้อมูลผู้ใช้หรือไม่ก่อนที่จะแสดง.
แสดงข้อมูลผู้ใช้รวมถึง username, email, ชื่อ-นามสกุล, และรูปประจำตัว (ถ้ามี)
ปุ่ม "Logout" ที่เมื่อคลิกจะเรียกใช้ฟังก์ชัน logOut

โดยทั้งหมดนี้รวมกันเป็นส่วนของ React component ที่ทำหน้าที่ดึงข้อมูลผู้ใช้จาก API และแสดงผลบนหน้าเว็บ, รวมถึงการจัดการการออกจากระบบ (Logout)




# API
- [@melivecode](https://www.melivecode.com/)
# WorkShop5
#### component ในหน้านี้  มีทั้งหมด  1 component ด้วยกัน 
```http
import Workshop5 from "src/components/miw/Workshop5";

const index = () => {
  return (
    <>
      <Workshop5 />
    </>
  );
};

export default index;
```
- คำสั่ง Import:
```http
import Workshop5 from "src/components/miw/Workshop5";

```
นำเข้าคอมโพเนนต์ Workshop5 จากที่อยู่ที่ระบุ

- คอมโพเนนต์แบบฟังก์ชัน (Index):
```http
const index = () => {
  return (
    <>
      <Workshop5 />
    </>
  );
};

```
'index': เป็น functional component ที่ไม่รับพารามิเตอร์.
มีการ return โค้ด JSX ที่ประกอบไปด้วย <Workshop5/> ซึ่งจะแสดงคอมโพเนนต์ Workshop5
- คำสั่งส่งออก:
```http
export default index;
```
บรรทัดนี้ส่งออกคอมโพเนนต์ index เป็น default export. นี่หมายความว่าหากไฟล์อื่น ๆ นำเข้าจากไฟล์นี้ จะได้รับ index ในลักษณะค่าเริ่มต้น

#### ต่อไปนี้จะอธิบายเเสดงโค้ด component หน้า <Workshop5/> ไฟล์ Workshop5.js นี้เป็น component ที่มีหน้าที่เรียกใช้ API ต่าง ๆ จาก process.env.NEXT_PUBLIC_API_URL ซึ่งถูกกำหนดในไฟล์ .env.local. ส่วนหลักของ component นี้มีการรวมฟังก์ชั่นต่าง ๆ ที่ทำ API calls ไปยัง server ที่ URL ที่กำหนด

 - Import และการกำหนด State:
 ```http
import { useEffect, useState } from "react";
import { Button, Container, Typography, CircularProgress } from "@mui/material";

const Workshop5 = () => {
const [loading, setLoading] = useState(false);

```
 - ฟังก์ชัน handleApiCall :
 ```http
 const handleApiCall = async (apiUrl, method, body) => {
  try {
    setLoading(true);
    const response = await fetch(apiUrl, {
      method: method,
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${localStorage.getItem ("jwt")}`,
      },
    });

    if (method === "GET") {
      console.log(`Request: ${method} `);
    } else {
      console.log(`Request:  ${method}`, body);
    }

    if (!response.ok) {
      const errorData = await response.json();
      throw new Error(`${response.status} - ${errorData.message}`);
    }

    const data = await response.json();

    console.info("Info: API Call Successful");
    console.log("Response:", data);
  } catch (error) {
    console.error("Error: API Call Failed", error);
    if (error instanceof Error) {
      console.error("Error Message:", error.message);
    }
  } finally {
    setLoading(false);
  }
};

```
` handleApiCall Function` :
```http
ฟังก์ชัน handleApiCall นี้ใช้ในการเรียก API โดยรับพารามิเตอร์ apiUrl (URL ของ API), method (HTTP method เช่น GET, POST, PUT, DELETE), และ body (ข้อมูลที่จะส่งให้กับ API ในกรณีที่ method คือ POST หรือ PUT)
ฟังก์ชันนี้จะทำการทำ API call และจัดการกับการโหลด (loading) ในขณะที่รอ response
ส่ง request และ log ข้อมูลที่เกี่ยวข้อง (method, body) เพื่อการ debug
ถ้า response ไม่ OK, จะ throw error พร้อมกับ log ข้อความ error และ error message ที่ได้จาก response
หาก response OK, จะ log ข้อมูล response เป็น JSON ไม่ว่า request จะสำเร็จหรือไม่, ฟังก์ชันจะทำการ set state ของ loading เป็น false เพื่อแสดงว่าการโหลดเสร็จสิ้น

```
 - ฟังก์ชันสำหรับ Users :
 ```http
 const UserList = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users", "GET"); //User list 1
};

const UserSearch = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users?search=karn"); //User List (SEARCH) 2
};

const UserPagination = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users?page=1&per_page=10", "GET"); //User (Pagination) 3
};

const UserSort = () => {
  handleApiCall(
    process.env.NEXT_PUBLIC_API_URL + "/users?sort_column=id&sort_order=desc",
    "GET"
  ); //User (Sort) 3
};

const UserSearchPaginationSort = () => {
  handleApiCall(
    process.env.NEXT_PUBLIC_API_URL +
      "/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc",
    "GET"
  ); //User (Search+Pagination+Sort) 4
};

const UserDetail = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/1", "GET"); //User (Detail) 5
};

const UserCreate = () => {
  const body = {
    fname: "w",
    lname: "w",
    username: "w.chat@melivecode.com",
    password: "w",
    email: "w.chat@melivecode.com",
    avatar: "https://www.melivecode.com/users/cat.png",
  };
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/create", "POST", body); //User (Create)
};

const UserUpdate = () => {
  const body = {
    id: 12,
    lname: "Gato",
  };
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/update", "PUT", body); // User (update)
};

const UserDelete = () => {
  const body = {
    id: 11,
  };
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/delete", "DELETE", body); // User (DELETE)
};
```
` User Functions` :
 ```http
UserList, 
UserSearch,
UserPagination, 
UserSort,
UserSearchPaginationSort,
UserDetail, 
UserCreate,
UserUpdate,
UserDelete
    คือฟังก์ชันที่เรียกใช้ handleApiCall เพื่อทำ API call ต่าง ๆ ที่เกี่ยวกับข้อมูลผู้ใช้ แต่ละฟังก์ชันทำ API call ต่าง ๆ โดยใช้ URL API ที่เกี่ยวข้องกับข้อมูลผู้ใช้และ method ที่เหมาะสม (GET, POST, PUT, DELETE)
ฟังก์ชัน UserCreate, UserUpdate, UserDelete จะมีการสร้าง body object เพื่อใส่ข้อมูลที่ต้องการส่งไปใน request

```

 - ฟังก์ชันสำหรับ Attractions :
 ```http
 const AttractionsList = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions", "GET"); //ATTRACTIONS (list)
};

const AttractionsSearch = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions?search=island", "GET"); //ATTRACTIONS (Search)
};

const AttractionPagination = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions?page=1&per_page=10", "GET"); //ATTRACTIONS (Pagination)
};

const AttractionSort = () => {
  handleApiCall(
    process.env.NEXT_PUBLIC_API_URL + "/attractions?sort_column=id&sort_order=desc",
    "GET"
  ); //ATTRACTIONS (Sort)
};

const AttractionSPS = () => {
  handleApiCall(
    process.env.NEXT_PUBLIC_API_URL +
      "/attractions?search=island&page=1&per_page=10&sort_column=id&sort_order=desc",
    "GET"
  ); //ATTRACTIONS (Search+Pagination+Sort)
};

const AttractionLanguage = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/th/attractions", "GET"); //ATTRACTIONS (Language)
};

const AttractionDetail1 = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions/1", "GET"); //ATTRACTIONS (Detail_1)
};

const AttractionDetail2 = () => {
  handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/th/attractions/1", "GET"); //ATTRACTIONS (Detail_2)
};

```
` Attractions Functions` :
```http
AttractionsList,
AttractionsSearch, 
AttractionPagination, 
AttractionSort, 
AttractionSPS, 
AttractionLanguage, 
AttractionDetail1, 
AttractionDetail2, 
Attractionstatic_paths 
    คือฟังก์ชันที่เรียกใช้ handleApiCall เพื่อทำ API call ต่าง ๆ ที่เกี่ยวข้องกับข้อมูลสถานที่ท่องเที่ยว (Attractions)
แต่ละฟังก์ชันทำ API call ต่าง ๆ โดยใช้ URL API ที่เกี่ยวข้องกับข้อมูลสถานที่ท่องเที่ยวและ method ที่เหมาะสม (GET)
ฟังก์ชัน AttractionsSearch, AttractionSPS จะมีการส่ง query parameters เพื่อทำการค้นหาหรือจัดเรียงข้อมูล
```
- Pet Sales Functions
```http
 const PetSalesSummary =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/pets/7days/2023-01-01", "GET"); //PetSalesSummary (7 Day)
  };
  
  const PetSalesDaily =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/pets/2023-01-01", "GET"); //PetSales (DAILY)
  };
```
` Pet Sales Functions` :
```http
PetSalesSummary และ PetSalesDaily คือฟังก์ชันที่เรียกใช้ handleApiCall เพื่อทำ API call เกี่ยวกับข้อมูลการขายสัตว์เลี้ยง
แต่ละฟังก์ชันทำ API call โดยใช้ URL API ที่เกี่ยวข้องกับข้อมูลการขายสัตว์เลี้ยง (Pets)

PetSalesSummary: เรียก API เพื่อดึงข้อมูลยอดขายสัตว์เลี้ยงย้อนหลัง 7 วัน
PetSalesDaily: เรียก API เพื่อดึงข้อมูลยอดขายสัตว์เลี้ยงรายวัน
```

 -  Render และ UI Components: :
```http
return (
  <Container component="main" maxWidth="xl">
    <div>
      <Typography variant="h6" sx={{ textAlign: "center" }}>
        Workshop5:
      </Typography>

      {/* ปุ่มสำหรับ Users */}
      <Typography
        sx={{ alignContent: "flex-start" }}
        display="block"
        flexDirection="row"
        justifyContent="space-around"
      >
        {/* ... (Buttons for Users) ... */}
      </Typography>

      {/* ปุ่มสำหรับ Attractions */}
      <Typography
        sx={{ alignContent: "flex-start" }}
        display="block"
        flexDirection="row"
        justifyContent="space-around"
      >
        {/* ... (Buttons for Attractions) ... */}
      </Typography>

      {/* ปุ่มสำหรับ Pet Sales */}
      <Button variant="contained" color="info" onClick={PetSalesSummary}>
        PetSalesSummary (7 Days)
      </Button>

      <Button variant="contained" color="info" onClick={PetSalesDaily}>
        PetSales(Daily)
      </Button>

      {/* แสดง CircularProgress ตามเงื่อนไข */}
      {loading && <CircularProgress />}
    </div>
  </Container>
);
};

export default Workshop5;
```
`การ Render และ UI Components` :
```http
ที่ส่วน render มีการใช้ Container, Typography, และ Button จาก Material-UI สำหรับสร้าง UI components
มีการ render ปุ่มต่าง ๆ สำหรับเรียกใช้งานฟังก์ชันที่เกี่ยวข้องกับข้อมูลผู้ใช้, สถานที่ท่องเที่ยว, และการขายสัตว์เลี้ยง
มีการแสดงผล CircularProgress เมื่อ loading เป็น true เพื่อแสดงตัวควบคุมการโหลด
```
โดยทั้งหมดนี้ทำให้ Workshop5 เป็นโมดูลที่ใช้หลายฟังก์ชัน API ที่เกี่ยวข้องกับข้อมูลผู้ใช้, สถานที่ท่องเที่ยว, และการขายสัตว์เลี้ยง 

โดยมีการจัดการรูปแบบการเรียก API และการแสดงผลผ่าน UI components ของ Material-UI





