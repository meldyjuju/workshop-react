import * as React from 'react';
import styles from "src/styles/waiwitzStyles.module.css";
import { Box, Button, Card, CardHeader, CardContent, Container, Typography, Paper, Grid, Link } from "@mui/material";

const workshop = [
    {
        name: 'Workshop Resume 🧾',
        link: 'waiwitz/resume/resumeAboutMe'
    },
    {
        name: 'Workshop 1 Calculator 🧮',
        link: 'waiwitz/workshop1/calculator'
    },
    {
        name: 'Workshop 2 Create Website 😇',
        link: 'waiwitz/workshop2/'
    },
    {
        name: 'Workshop 3 Login API 🗝️',
        link: 'waiwitz/workshopAPI/workshopLogin'
    },
    {
        name: 'Workshop 4 User API By token 👤',
        link: 'waiwitz/workshopAPI/workshopUser'
    },
    {
        name: 'Workshop 5 API 21 เส้น 😢',
        link: 'waiwitz/workshopFive'
    },
]

const MyWorkshop = () => {

    return (
        <>
            <Container maxWidth='xl' className={styles.spaceContainer}>
                <Card>
                    <CardHeader title='Workshop 💪' />
                    <CardContent>
                        <Grid container spacing={3}>
                            {workshop.map(((work) => (
                                <Grid item key={work.name} >
                                    <Link href={work.link}>
                                        <Button style={{ backgroundColor: '#eee', color: '#001' }}>
                                            {work.name}
                                        </Button>
                                    </Link>
                                </Grid>
                            )))}
                        </Grid>
                    </CardContent>
                </Card>
            </Container>
        </>
    )
}


export default MyWorkshop;