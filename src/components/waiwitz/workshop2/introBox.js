import { Box, Card, CardContent, Paper, Typography } from "@mui/material";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

const IntroBox = ({ title, detail, color }) => {
    return (
        <Paper sx={{ width: '440px', backgroundColor: '#fff', borderRadius: '0', background: `${color}` }} >
            <Box sx={{ padding: '2em 2em 1em', color: 'white' }}>
                <Typography variant="h5" sx={{ fontWeight: 'bold', height:'50px' }}>
                    {title}
                </Typography>
                <Box sx={{ margin: '1em 0' }}>
                    <Typography variant="p">
                        {detail}
                    </Typography>
                </Box>
                <ArrowRightAltIcon sx={{fontSize: '50px', cursor: 'pointer'}}/>
            </Box>
        </Paper>
    )
}

export default IntroBox;