import { Box, Button, Grid, Typography, Avatar } from "@mui/material";
import StarRateRoundedIcon from '@mui/icons-material/StarRateRounded';
import StarHalfRoundedIcon from '@mui/icons-material/StarHalfRounded';

const Introduce4 = ({ head1, head2, body }) => {
    return (
        <Grid container columns={{ xs: 4, sm: 8, md: 12 }} margin={'8em 0'} justifyContent={'center'}>
            <Grid item xs={6}>
                <Box sx={{ padding: '20px' }}>
                    <Typography sx={{ fontWeight: 'bold', color: '#757575' }}>{head1}</Typography>
                    <Typography variant="h3" sx={{ padding: '15px 0' }} gutterBottom>
                        {head2}
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        {body}
                    </Typography>
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Box sx={{ border: '1px solid #C4C4C4', width: '100%', height: 'fit-content', padding: '1.5em' }}>
                    <Box marginBottom={1}>  {Array.from(Array(4), (i) => {
                        return <StarRateRoundedIcon sx={{ color: '#3F3F3F' }} />
                    })}
                        <StarHalfRoundedIcon sx={{ color: '#3F3F3F' }} />
                    </Box>
                    <Typography variant="body1" gutterBottom>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</Typography>
                    <Box display={"flex"} margin={'1em 0'}>
                        <Avatar sx={{ width: 70, height: 70 }}></Avatar>
                        <Box marginLeft={2} alignSelf={'center'}>
                            <Typography variant="h6" gutterBottom>John De</Typography>
                            <Typography>Art Director</Typography>
                        </Box>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    )
}

export default Introduce4;