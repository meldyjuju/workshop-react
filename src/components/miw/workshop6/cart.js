import React from 'react';
import { List, ListItem, ListItemText, Button, Dialog, DialogTitle, DialogContent,Typography ,DialogActions} from '@mui/material';




const Cart = ({ cartItems, onClose, isOpen }) => {
  
  const calculateTotal = () => {
    return cartItems.reduce((total, item) => total + item.price * item.quantity, 0);
  };
  return (
      <Dialog open={isOpen} onClose={onClose}>
        <DialogTitle>Shopping Cart</DialogTitle>
        <DialogContent>
          <List>
            {cartItems.map((item) => (
              <ListItem key={item.id}>
                <ListItemText
                  primary={item.name}
                  secondary={`Quantity: ${item.quantity} | Price: ${item.price * item.quantity} ฿`}
                />
                <img src={"https://whiskydd.com/wp-content/uploads/2022/12/Hennessy-V.S.O.P-800x700-1-400x457.jpg"} alt={item.name} style={{ width: '50px', height: '50px', marginLeft: '10px' }} />
              </ListItem>
            ))}
          </List>
          <Typography variant="h6" style={{ marginTop: '10px' }}>
            Total: {calculateTotal()} ฿
          </Typography>
        </DialogContent>
        <Button onClick={onClose} variant="contained" color="primary" sx={{ marginX:"5rem" }}>
          Confirm Order
        </Button>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    );
  };

export default Cart;
