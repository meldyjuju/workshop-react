import React from 'react';
import { Card, CardContent, Typography, Button } from '@mui/material';

const Products = ({ product, onAddToCart }) => {
    

    
  return (
    <Card>
      <CardContent >
        <Typography variant="h6" >{product.name}  </Typography>
        <img
        src={"https://whiskydd.com/wp-content/uploads/2022/12/Hennessy-V.S.O.P-800x700-1-400x457.jpg"}
        alt="Whiskey"
        style={{ width: "100%", height: "300px", objectFit: "cover" }}
      />
        <Typography variant="body2" >Price: {product.price}฿</Typography>
        <Button variant="contained" onClick={() => onAddToCart(product)} >
          Add to Cart
        </Button>
      </CardContent>
    </Card>
  );
};

export default Products;
