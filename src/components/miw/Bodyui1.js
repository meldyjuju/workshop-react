import { Box, Container, Grid, ListItem, Paper ,Button } from "@mui/material";
import React from "react";
import Typography from "@mui/material/Typography";

const Bodyui1 = () => {
  return (
    <div style={{ backgroundColor: "#78909c", padding: "100px" }}>
      <Typography
        variant="h6"
        sx={{
          color: "white",
          margin: "10px",
          display: "block",
          textAlign: "left",
          fontWeight: "bold",
          fontSize: "50px",
        }}
      >
        LETS CHANGH YOUR OWN HOME <br />
        INTERIOR DESIGN NOW{" "}
      </Typography>
      <Button variant="contained" color="primary">
          CONTACT US
        </Button>
    </div>
  );
};

export default Bodyui1;
