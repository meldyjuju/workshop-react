import { AppBar, Toolbar, Typography } from "@mui/material";

const Navbar = () => {
  return (
    <AppBar position="static" style={{ backgroundColor: "#7FB3D5 " }}>
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          About Me 👀
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
