import React from 'react'
import Image from 'next/image'
import profilePic from '../../../../public/johnnnyProfile.JPG'

export default function profileName() {
    return (
        <div className="my-6">
            {/* รูป */}
            <div className="flex w-full">
                <Image
                src={profilePic}
                alt="Picture of the author"
                width={180}
                height={180}
                className='rounded-2xl mr-20'
            />

            {/* ชื่อ & ตำแหน่ง */}
            <div className="flex border-b-2 border-gray-400 w-full">
                <div className="my-auto text-5xl font-bold">
                    <p className='mb-7 text-gray-700 tracking-[.8rem]'>Natpacan Sribanhad</p>
                    <p className="text-4xl text-gray-500 font-semiBold tracking-[.2     rem]">Frontend Developer</p>
                </div>
            </div>
            </div>
            
        </div>
    )
}
