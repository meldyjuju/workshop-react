import React, { useState, useEffect } from 'react'
import { 
    CircularProgress,
    TextField,
} from '@mui/material'
//* import components
import CLayout from '../cLayout'
import UserTable from '../userTable';

export default function search() {

    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        // console.log(searchTerm)
        const fetchData = async () => {
            try {
                // const searchQuery = 'karn';
                const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users?search=${searchTerm}`);
                const data = await response.json();
                setUserData(data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        };

        fetchData();
    }, [searchTerm]);

    // const handleSearchChange = (event) => {
    //     setSearchTerm(event.target.value);
    //   };

    return (
        <CLayout title="Search">
            <TextField
                label="Search"
                variant="outlined"
                fullWidth
                value={searchTerm}
                onChange={e=>setSearchTerm(e.target.value)}
                sx={{
                    marginBottom:'20px'
                }}
            />
            {loading ? (
                <CircularProgress />
            ) : (
                <UserTable userData={userData} />
            )}
        </CLayout>
    )
}
