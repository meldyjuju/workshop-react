import React from 'react';
import { Button, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';

export default function DeleteUser() {
  const [loading, setLoading] = React.useState(false);

  const handleDelete = async () => {
    setLoading(true);

    try {
      const response = await fetch('https://www.melivecode.com/api/users/delete', {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: 20,
        }),
      });

      if (!response.ok) {
        throw new Error('Failed to delete user');
      }

      console.log('User deleted successfully');
      // Add any additional logic you need after successful deletion
    } catch (error) {
      console.error('Error deleting user:', error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <CLayout title='Delete User'>
      <Button
        onClick={handleDelete}
        disabled={loading}
        sx={{
          backgroundColor: 'black',
          color: 'white',
          borderRadius: '10px',
          '&:hover': {
            backgroundColor: '#373737',
          },
        }}
      >
        {loading ? <CircularProgress size={24} /> : 'Delete User'}
      </Button>
    </CLayout>  
  );
}
