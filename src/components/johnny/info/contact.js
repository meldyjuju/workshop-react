import React from 'react'
import { Fade, Bounce, Flip, Slide } from "react-awesome-reveal";


export default function contact() {

    


    const contacts = [

    // svg className="w-3 h-3 text-blue-800"
        
        {   
            color:"bg-teal-500",
            name :"Address",
            value:"915/18, Sila, Mueang, Khon kaen ,Thailand"
        },
        {   
            color:"bg-yellow-300",
            name :"Tel",
            value:"093-374-1160"
        },
        {   
            color:"bg-indigo-500",
            name :"Email",
            value:"natpacan.sri@gmail.com"
        },
        {   
            color:"bg-purple-500",
            name :"Github",
            value:"github.com/NatpacanSri"
        },
    ]

    return (
        <div className="p-6 bg-white border border-gray-200 rounded-xl shadow-md ">

            <div className="flex">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="w-8 h-8 text-gray-500  mb-3 mr-3">
                    <path fillRule="evenodd" d="M4.5 3.75a3 3 0 00-3 3v10.5a3 3 0 003 3h15a3 3 0 003-3V6.75a3 3 0 00-3-3h-15zm4.125 3a2.25 2.25 0 100 4.5 2.25 2.25 0 000-4.5zm-3.873 8.703a4.126 4.126 0 017.746 0 .75.75 0 01-.351.92 7.47 7.47 0 01-3.522.877 7.47 7.47 0 01-3.522-.877.75.75 0 01-.351-.92zM15 8.25a.75.75 0 000 1.5h3.75a.75.75 0 000-1.5H15zM14.25 12a.75.75 0 01.75-.75h3.75a.75.75 0 010 1.5H15a.75.75 0 01-.75-.75zm.75 2.25a.75.75 0 000 1.5h3.75a.75.75 0 000-1.5H15z" clipRule="evenodd" />
                </svg>
                <h5 className="mb-2 text-2xl font-semibold tracking-tight text-gray-900 ">Contact me</h5>
            </div>
            <div className="mb-3 font-normal text-gray-500 ">
                {/* list */}
                <ol className="relative border-s border-gray-300 ">

                    {contacts.map((contact)=>(
                        <li className="mb-3 ms-6">
                        <span className={`absolute flex items-center justify-center w-5 h-5 rounded-full -start-3 ring-3 ring-white ${contact.color} `}></span>
                        <h3 className="mb-1 text-md font-semibold text-gray-900 ">{contact.name}: <span className="text-gray-500">{contact.value}</span></h3>
                    </li>
                    ))}

                    
                    
                </ol>

            </div>
        </div>
    )
}
