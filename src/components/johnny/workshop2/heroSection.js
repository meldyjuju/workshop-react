import React from 'react'
import Image from 'next/image'
import {
    Container,
    Box,
    Typography,
    Button,
    Grid,
} from "@mui/material"

//* import components
import logoHomePic from '../../../../public/imgJohnny/picHeroSection.png'

export default function heroSection() {
    return (

        <Box
            sx={{
                height: '91vh',
                backgroundColor: "#19A7CE",
            }}
        >
            <Container maxWidth='full' sx={{ height: '100%', display: 'flex' }} >
                <Grid container sx={{ margin: "auto 0" }}>
                    <Grid item lg={8} sx={{ display: 'flex', }} >
                        <Box sx={{ margin: 'auto 0' }}>
                            <Typography
                                sx={{
                                    color: "#AFD3E2",
                                    fontSize: '1.4em'
                                }}
                            >WELCOME TO HOMCO</Typography>
                            <Typography
                                sx={{
                                    color: 'white',
                                    fontSize: '5em',
                                    fontWeight: '900',
                                    lineHeight: "100%",
                                    margin: '20px 0'
                                }}
                            >BUILD YOUR <br /> ELEGAN DREAM <br /> HOME INTERIOR</Typography>
                            <Typography
                                sx={{
                                    color: 'white',
                                    width: '40vw'
                                }}
                            >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                            <Button variant="contained" sx={{ marginTop: '15px', borderRadius: "8px" }}>
                                Our Project
                            </Button>
                        </Box>

                    </Grid>
                    <Grid item lg={4} sx={{ display: 'flex', }}>
                        <Image
                            src={logoHomePic}
                            // src="https://img.freepik.com/free-photo/3d-view-house-model_23-2150761168.jpg?t=st=1700474521~exp=1700478121~hmac=9c5fedd5622eb305496bde7bc088162638c5d2bde6668793c125914301b4b520&w=740"
                            height={400}
                            width={500}
                            style={{
                                margin: 'auto',
                            }}
                        />
                    </Grid>
                </Grid>
            </Container>

        </Box>
    )
}
