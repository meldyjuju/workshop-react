import React, { useState } from 'react'
import * as math from 'mathjs';

export default function cal() {

    const [result, setResult] = useState('');
    const [answer, setAnswer] = useState('')
    const [selectedOperator, setSelectedOperator] = useState('');

    const handleClick = (e) => {

        const buttonName = e.target.name;

        if (isNaN(buttonName)) {
            // If an operator button is clicked
            if (selectedOperator && selectedOperator !== buttonName) {
                // If a different operator is already selected, update the selected operator
                setSelectedOperator(buttonName);
                setResult(result.slice(0, -1).concat(buttonName));
            } else if (!selectedOperator) {
                // Otherwise, select the operator and append it to the result
                setSelectedOperator(buttonName);
                setResult(result.concat(buttonName));
            }
        } else {
            // If a number button is clicked
            if (selectedOperator) {
                // If an operator is already selected, append the number to the result after the operator
                if (result.slice(-1) === selectedOperator) {
                    setResult(result.concat(buttonName));
                } else {
                    setResult(result + selectedOperator + buttonName);
                }
                setSelectedOperator('');
            } else {
                // Otherwise, simply append the number to the result
                setResult(result.concat(buttonName));
            }
        }
    };

    const clear = () => {
        setResult('');
        setSelectedOperator('');
    };

    const backspace = () => {
        setResult(result.slice(0, -1));
        setSelectedOperator('');
    };

    const toggleSign = () => {
        // Toggle the sign of the current result
        setResult((prevResult) => {
            const updatedResult = prevResult.startsWith('-') ? prevResult.slice(1) : `-${prevResult}`;
            return updatedResult;
        });
    };

    const percen = () => {
        try {
            let answer = math.evaluate(result)
            setAnswer(answer / 100);
            setSelectedOperator('');
        } catch (err) {
            setAnswer('Error');
        }
    }

    const factorial = () => {
        try {
            const resultNumber = parseFloat(result);
            if (!isNaN(resultNumber) && Number.isInteger(resultNumber) && resultNumber >= 0) {
                const factResult = math.factorial(resultNumber);
                setResult(result + "!")
                setAnswer(factResult.toString());
            } else {
                setResult('Error');
                setAnswer('Error');
            }
            setSelectedOperator('');
        } catch (err) {
            setResult('Error');
            setAnswer('Error');
        }
    }

    const calculate = () => {
        // console.log(math.evaluate(result).toString())
        try {
            setAnswer(math.evaluate(result).toString());
            setSelectedOperator('');
        } catch (err) {
            setAnswer('Error');
        }
    };

    const inputPads = [
        { name: "clear", id: "backspace", onClick: backspace, value: "C", style: "text-black font-semibold" },
        { name: "toggleSign", id: "sign", onClick: toggleSign, value: "-/+", style: "text-black font-semibold" },
        { name: "%", id: "percen", onClick: percen, value: "%", style: "text-black font-semibold" },
        { name: "/", id: "divide", onClick: handleClick, value: "÷", style: "text-blue-500 text-[1.7em] font-semibold" },
        { name: "7", id: "7", onClick: handleClick, value: "7" },
        { name: "8", id: "8", onClick: handleClick, value: "8" },
        { name: "9", id: "9", onClick: handleClick, value: "9" },
        { name: "*", id: "multiply", onClick: handleClick, value: "×", style: "text-blue-500 text-[1.7em] font-semibold" },
        { name: "4", id: "4", onClick: handleClick, value: "4" },
        { name: "5", id: "5", onClick: handleClick, value: "5" },
        { name: "6", id: "6", onClick: handleClick, value: "6" },
        { name: "-", id: "subtract", onClick: handleClick, value: "−", style: "text-blue-500 text-[1.7em] font-semibold" },
        { name: "1", id: "1", onClick: handleClick, value: "1" },
        { name: "2", id: "2", onClick: handleClick, value: "2" },
        { name: "3", id: "3", onClick: handleClick, value: "3" },
        { name: "+", id: "add", onClick: handleClick, value: "+", style: "text-blue-500 text-[1.7em] font-semibold" },
        { name: "!", id: "doubleZero", onClick: factorial, value: "!" },
        { name: "0", id: "zero", onClick: handleClick, value: "0" },
        { name: ".", id: "decimal", onClick: handleClick, value: "." },
        { name: "=", id: "result", onClick: calculate, value: "=", style: "text-white text-[1.4em] bg-blue-500 hover:bg-blue-400 active:bg-blue-600 shadow-lg" },
    ];


    return (

        <div className="bg-white max-w-xl m-auto rounded-2xl overflow-hidden shadow-xl" >
            <form className='border-gray-200 border-b-2'>
                <input
                    className='block min-w-full pt-4 px-9 text-end text-base text-gray-500'
                    type="text" value={result} />
                <input
                    className='block min-w-full py-2.5 px-9 max-w-[200px] p-2 text-end text-3xl '
                    type="text" value={answer} />
            </form>
            <div className="grid grid-cols-4 p-5">
                {inputPads.map((inputPad) => (
                    <button className={`p-3 m-1 rounded-full text-lg hover:bg-gray-100 active:bg-gray-200 text-gray-500 ${inputPad.style} `}
                        onClick={inputPad.onClick}
                        id={inputPad.id}
                        name={inputPad.name}
                    >{inputPad.value}</button>
                ))}
            </div>
        </div>


    )
}