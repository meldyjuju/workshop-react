import { useState } from 'react'
import { Dialog } from '@headlessui/react'
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline'
import Image from 'next/image'
import profile from '../../../public/johnnnyProfile.JPG'
import { Link as ScrollLink, animateScroll as scroll } from "react-scroll";
import { Fade, Bounce, Flip, Slide } from "react-awesome-reveal";


export default function intro() {

    return (
        <div className="bg-white">
            <div className="relative isolate">
                <div
                    className="absolute inset-x-0 -top-40 -z-10 transform-gpu overflow-hidden blur-3xl sm:-top-80"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%-11rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 rotate-[30deg] bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%-30rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
                        {/* mx-20 */}
                <div className="py-52">

                    {/* arrow */}
                    {/* <div className="hidden sm:mb-8 sm:flex">
                        <div className="relative rounded-full px-3 py-1 text-sm leading-6 text-gray-600 ring-1 ring-gray-900/10 hover:ring-gray-900/20">
                            My workshop. {' '}
                            <a href="#" className="font-semibold text-indigo-600">
                                <span className="absolute inset-0" aria-hidden="true" />
                                Read more <span aria-hidden="true">&rarr;</span>
                            </a>
                        </div>
                    </div> */}

                    <div  className="grid grid-cols-2">
                        <Slide triggerOnce direction='right' duration={1000}  className='flex'>
                            <div className='my-auto'>
                                <h1 className="text-3xl font-bold tracking-[.13em] italic text-gray-900 sm:text-6xl ">
                                Johnnnyツ
                            </h1>
                            <p className="text-2xl  leading-8 text-gray-600">
                                Natpacan Sribanhad
                            </p>
                            <div className="mt-4 flex gap-x-6">
                                <ScrollLink
                                    to="workshopTB"
                                    smooth={true}
                                    duration={1000}
                                    className="rounded-md bg-indigo-600 px-3.5 py-2.5 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 cursor-pointer"
                                >
                                    My workshop
                                </ScrollLink>
                                <ScrollLink
                                    to="aboutme"
                                    smooth={true}
                                    duration={1000}
                                    className="text-sm font-semibold leading-6 text-gray-900 my-auto cursor-pointer">
                                    about me <span aria-hidden="true">→</span>
                                </ScrollLink>
                            </div>
                            </div>
                            
                        </Slide>
                        <Fade
                            triggerOnce
                            className='flex'>
                            <Image src={profile} alt='profile' height={250} width={250} 
                                className='rounded-3xl ml-auto'
                            />
                        </Fade>
                    </div>
                </div>
                <div
                    className="absolute inset-x-0 top-[calc(100%-13rem)] -z-10 transform-gpu overflow-hidden blur-3xl sm:top-[calc(100%-30rem)]"
                    aria-hidden="true"
                >
                    <div
                        className="relative left-[calc(50%+3rem)] aspect-[1155/678] w-[36.125rem] -translate-x-1/2 bg-gradient-to-tr from-[#ff80b5] to-[#9089fc] opacity-30 sm:left-[calc(50%+36rem)] sm:w-[72.1875rem]"
                        style={{
                            clipPath:
                                'polygon(74.1% 44.1%, 100% 61.6%, 97.5% 26.9%, 85.5% 0.1%, 80.7% 2%, 72.5% 32.5%, 60.2% 62.4%, 52.4% 68.1%, 47.5% 58.3%, 45.2% 34.5%, 27.5% 76.7%, 0.1% 64.9%, 17.9% 100%, 27.6% 76.8%, 76.1% 97.7%, 74.1% 44.1%)',
                        }}
                    />
                </div>
            </div>
        </div>
    )
}
