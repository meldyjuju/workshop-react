import React, { useState, useEffect } from 'react';
import Itemcard from './itemcard';
import {
  Container,
  Grid,
  Typography,
  CircularProgress,
} from '@mui/material'

export default function Itemlist() {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  const handleAddToCart = (item) => {
    const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    const newItem = {...item,quantity: 1, totalPrice:item.price}
    const updatedCart = [...cartItems, newItem];
    localStorage.setItem('cartItems', JSON.stringify(updatedCart));
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        setLoading(false)
        setItems(data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
  
    fetchData();
  }, []);

  
  

  return (
    <Container sx={{marginTop:'30px'}}> 
      <Typography variant="h3" sx={{marginBottom:'30px'}}>Products</Typography>
      <Grid container spacing={3}>
      {loading ?
        <CircularProgress sx={{margin:'40px'}} />
      : 
      items.map((item,index) => (
        <Grid key={index} item xs={3}> 
          <Itemcard key={item.id} product={item} onAddToCart={handleAddToCart} />
        </Grid>
        ))
      }
      </Grid>
      
    </Container>
  );
}
