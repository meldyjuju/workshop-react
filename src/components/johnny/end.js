import React from 'react'

export default function end() {
    return (
        <div className='flex pb-16'>
            <div className='m-auto'>
                <div className='flex'>
                    <span className="flex w-5 h-5 me-5 bg-gray-200 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-gray-900 rounded-full dark:bg-gray-700"></span>
                    <span className="flex w-5 h-5 me-5 bg-blue-600 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-green-500 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-red-500 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-purple-500 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-indigo-500 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-yellow-300 rounded-full"></span>
                    <span className="flex w-5 h-5 me-5 bg-teal-500 rounded-full"></span>
                </div>
            </div>
        </div>
    )
}
