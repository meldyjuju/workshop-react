import React, { useState } from 'react';
import { Tabs, Tab, Paper, Typography, Button, Tooltip, TextField } from '@mui/material';
import { Box } from '@mui/system';
import Link from 'next/link';
import FileCopyIcon from '@mui/icons-material/FileCopy';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';

export default function Workshop({ workshops }) {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    // Function format date to YYYY-MM-DD
    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        const formattedDate = new Date(dateString).toLocaleDateString(undefined, options);
        return formattedDate;
    };

    return (
        <Paper className='px-8 py-4 mb-8'>
            <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="auto"
                allowScrollButtonsMobile
                aria-label="Workshop List"
                className='mb-8'
            >
                {/* Generate Tab components using map */}
                {workshops.map((workshop, index) => (
                    <Tab key={index} label={workshop.tabName} />
                ))}
            </Tabs>

            {/* Generate TabPanel components using map */}
            {workshops.map((workshop, index) => (
                <TabPanel key={index} value={value} index={index}>
                    <Box className="flex flex-col md:flex-row justify-between mb-6">
                        <Box>
                            <Typography variant="h6" sx={{ mb: 1 }}>{workshop.title}</Typography>
                            <Typography variant="subtitle1">{workshop.subtitle}</Typography>
                        </Box>
                        <Box className="mt-2 md:mt-0 md:text-right">
                            <Typography variant="body2">Due Date: {formatDate(workshop.dueDate)}</Typography>
                            <Typography variant="body2">Submision Date: {formatDate(workshop.date)}</Typography>
                        </Box>
                    </Box>
                    <img src={workshop.thumbnail} className='h-1/2 md:h-[36em] w-full object-cover mb-8 rounded-md ring-1 ring-offset-4 ring-gray-200' alt={workshop.title} />
                    <ReposPanel reposLink={workshop.reposLink} reposHTTPS={workshop.reposHTTPS} demoHref={workshop.demoHref} />
                </TabPanel>
            ))}
        </Paper>
    );
}

export function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && <div>{children}</div>}
        </div>
    );
}

export function ReposPanel({ reposLink, reposHTTPS, demoHref, playgrounds }) {
    const [isCopied, setIsCopied] = useState(false);
    const handleCopy = () => {
        setIsCopied(true);
        navigator.clipboard.writeText(reposLink);
        setTimeout(() => setIsCopied(false), 3000);
    };
    return (
        <Paper elevation={0} className='flex flex-col md:flex-row gap-4 justify-between'>
            <Box className='flex flex-col md:flex-row justify-between gap-4 flex-1'>
                <Box>
                    <Typography variant="h6" gutterBottom>
                        Repository Link
                    </Typography>
                    <Typography variant="body1" paragraph>
                        Visit my GitHub repository:
                        {' '}
                        <Link href="" target="_blank" rel="noopener noreferrer">
                            {reposLink}
                        </Link>
                    </Typography>
                </Box>
                <TextField
                    label="Clone with HTTPS"
                    className='w-full md:w-1/3'
                    defaultValue={reposHTTPS}
                    InputProps={{
                        readOnly: true,
                    }}
                />
            </Box>
            {/* COPY BUTTON */}
            <Box className='flex flex-row gap-2'>
                <Tooltip title={!isCopied ? 'Copy to Clipboard' : 'Copied'} arrow>
                    <Button variant="contained" color={playgrounds ? "success" : "primary"} onClick={handleCopy} className='h-14 w-full md:w-auto'>
                        {!isCopied ? (
                            <FileCopyIcon />
                        ) : (
                            <ContentCopyIcon />
                        )}
                    </Button>
                </Tooltip>
                <Tooltip title="Demo" arrow>
                    <Button variant="outlined" color={playgrounds ? "success" : "primary"} href={`./teerawut${demoHref}`} className='h-14 w-full md:w-auto' disabled={!demoHref ? true : false}>
                        Demo
                    </Button>
                </Tooltip>
            </Box>
        </Paper>
    );
}