import { useCallback, useEffect, useState } from 'react';
import Head from 'next/head';
import NextLink from 'next/link';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
  Alert,
  Box,
  Button,
  CircularProgress,
  FormHelperText,
  Link,
  Stack,
  Tab,
  Tabs,
  TextField,
  Typography,
  Unstable_Grid2 as Grid
} from '@mui/material';
import { Logo } from 'src/components/logo';


const baseUrl = '/workShop/teerawut/loginApp';

const LoginPage2 = ({ handleUser, setOpenSnackbar, setSnackbarMessage }) => {
  const [method, setMethod] = useState('email');
  const storedUserData = localStorage.getItem('userData');
  const userData = storedUserData ? JSON.parse(storedUserData) : null;
  const [loading, setLoading] = useState(false);

  const handleMethodChange = useCallback(
    (event, value) => {
      setMethod(value);
    },
    []
  );

  const formik = useFormik({
    initialValues: {
      username: '',
      password: '',
    },
    validationSchema: Yup.object({
      username: Yup.string().email('Invalid email address').required('Required'),
      password: Yup.string().required('Required'),
    }),
    onSubmit: async (values) => {
      try {
        setLoading(true);
        const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/login`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(values),
        });

        const data = await response.json();

        if (response.ok) {
          // Successful login
          localStorage.setItem('accessToken', data.accessToken);
          localStorage.setItem('userData', JSON.stringify(data.user)); // Store user data
          setSnackbarMessage({ status: 'success', title: 'Login successful' });
          console.info('[INFO] Login successful');
          fetchUserData(data.accessToken);
        } else {
          // Login failed
          formik.setErrors({ submit: 'Incorrect email or password' });
          setSnackbarMessage({ status: 'failed', title: 'Incorrect email or password' });
          console.warn('[WARNING] Incorrect email or password');
        }
        setOpenSnackbar(true);
      } catch (error) {
        console.error('[ERROR] Error during login:', error);
      } finally {
        setLoading(false);
      }
    },
  });

  const handleSkip = () => {
    // Handle skipping authentication (if needed)
  };


  const fetchUserData = async (accessToken) => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/auth/user`, {
        // Use the environment variable here
        method: 'GET',
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      const userData = await response.json();

      if (response.ok) {
        // User details fetched successfully
        handleUser(userData.user);
        console.info("[INFO] Fetch userData successfully")
      } else {
        // Unable to fetch user details
        console.error('[ERROR] Unable to fetch user details');
      }
    } catch (error) {
      console.error('[ERROR] Error fetching user details:', error);
    }
  };

  useEffect(() => {
    // Check if the user is already logged in with a valid access token
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      fetchUserData(accessToken);
      console.info(`[INFO] ${userData.fname} ${userData.lname} is already logged in with a valid access token`)
    }
  }, []);

  return (
    <>
      <Head>
        <title>Login | Playground 2</title>
      </Head>
      <Box
        sx={{
          backgroundColor: 'background.paper',
          flex: '1 1 auto',
          alignItems: 'center',
          display: 'flex',
          justifyContent: 'center'
        }}
      >
        <Box
          sx={{
            maxWidth: 550,
            px: 3,
            py: '100px',
            width: '100%'
          }}
        >
          <div>
            <Stack
              spacing={1}
              sx={{ mb: 3 }}
            >
              <Typography variant="h4">
                Login
              </Typography>
              <Typography
                color="text.secondary"
                variant="body2"
              >
                Don&apos;t have an account?
                &nbsp;
                <Link
                  component={NextLink}
                  href={`${baseUrl}/signup`}
                  underline="hover"
                  variant="subtitle2"
                >
                  Register
                </Link>
              </Typography>
            </Stack>
            <Tabs
              onChange={handleMethodChange}
              sx={{ mb: 3 }}
              value={method}
            >
              <Tab label="Email" value="email" />
              <Tab label="Phone Number" value="phoneNumber" />
            </Tabs>
            {method === 'email' && (
              <form noValidate onSubmit={formik.handleSubmit}>
                <Stack spacing={3}>
                  <TextField
                    error={!!(formik.touched.username && formik.errors.username)}
                    fullWidth
                    helperText={formik.touched.username && formik.errors.username}
                    label="Email Address"
                    name="username"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    type="email"
                    value={formik.values.username}
                  />
                  <TextField
                    error={!!(formik.touched.password && formik.errors.password)}
                    fullWidth
                    helperText={formik.touched.password && formik.errors.password}
                    label="Password"
                    name="password"
                    onBlur={formik.handleBlur}
                    onChange={formik.handleChange}
                    type="password"
                    value={formik.values.password}
                  />
                </Stack>
                <FormHelperText sx={{ mt: 1 }}>
                  Optionally, you can skip.
                </FormHelperText>
                {formik.errors.submit && (
                  <Typography
                    color="error"
                    sx={{ mt: 3 }}
                    variant="body2"
                  >
                    {formik.errors.submit}
                  </Typography>
                )}
                <Button
                  fullWidth
                  size="large"
                  sx={{ mt: 3 }}
                  type="submit"
                  variant="contained"
                >
                  {loading ? <CircularProgress color='inherit' size={24} /> : 'Continue'}
                </Button>
                <Button
                  fullWidth
                  size="large"
                  sx={{ mt: 3 }}
                  onClick={handleSkip}
                >
                  Skip authentication
                </Button>
                <Alert
                  color="primary"
                  severity="info"
                  sx={{ mt: 3 }}
                >
                  <div>
                    You can use <b>karn.yong@melivecode.com</b> and password <b>melivecode</b>
                  </div>
                </Alert>
              </form>
            )}
            {method === 'phoneNumber' && (
              <div>
                <Typography
                  sx={{ mb: 1 }}
                  variant="h6"
                >
                  Not available in the demo
                </Typography>
                <Typography color="text.secondary">
                  To prevent unnecessary costs, we disabled this feature in the demo.
                </Typography>
              </div>
            )}
          </div>
        </Box>
      </Box>
    </>
  );
};

export const LayoutLogin = ({ children }) => (
  <Box
    component="main"
    sx={{
      display: 'flex',
      flex: '1 1 auto'
    }}
  >
    <Grid
      container
      sx={{ flex: '1 1 auto' }}
    >
      <Grid
        xs={12}
        lg={6}
        sx={{
          backgroundColor: 'background.paper',
          display: 'flex',
          flexDirection: 'column',
          position: 'relative'
        }}
      >
        <Box
          component="header"
          sx={{
            left: 0,
            p: 3,
            position: 'fixed',
            top: 0,
            width: '100%'
          }}
        >
          <Box
            component={NextLink}
            href="/"
            sx={{
              display: 'inline-flex',
              height: 32,
              width: 32
            }}
          >
            <Logo />
          </Box>
        </Box>
        {children}
      </Grid>
      <Grid
        xs={12}
        lg={6}
        sx={{
          alignItems: 'center',
          background: 'radial-gradient(50% 50% at 50% 50%, #122647 0%, #090E23 100%)',
          color: 'white',
          display: 'flex',
          justifyContent: 'center',
          '& img': {
            maxWidth: '100%'
          }
        }}
      >
        <Box sx={{ p: 3 }}>
          <Typography
            align="center"
            color="inherit"
            sx={{
              fontSize: '24px',
              lineHeight: '32px',
              mb: 1
            }}
            variant="h1"
          >
            Welcome to{' '}
            <Box
              component="a"
              sx={{ color: '#15B79E' }}
              target="_blank"
            >
              FreshyKit
            </Box>
          </Typography>
          <Typography
            align="center"
            sx={{ mb: 3 }}
            variant="subtitle1"
          >
            A professional kit that comes with ready-to-use MUI components.
          </Typography>
          <img
            alt=""
            src="/assets/auth-illustration.svg"
          />
        </Box>
      </Grid>
    </Grid>
  </Box>
);

export default LoginPage2;
