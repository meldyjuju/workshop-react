import { Box, Button, FormControl, InputLabel, MenuItem, Pagination, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";

const UserSearchPaginationSort = () => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(1);
    const [perPage, setPerPage] = useState(10);
    const [sortColumn, setSortColumn] = useState("id");
    const [sortOrder, setSortOrder] = useState("asc");
    const [totalPages, setTotalPages] = useState(0);

    const fetchData = async () => {
        setLoading(true);

        try {
            const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users?search=${search}&page=${page}&per_page=${perPage}&sort_column=${sortColumn}&sort_order=${sortOrder}`);
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            const result = await response.json();

            console.info('[INFO] Fetched user, search, pagination, sort successfully')
            console.log('UserSearchPaginationSort', result);
            setData(result.data);
            setTotalPages(result.total_pages);
        } catch (error) {
            console.error("Error fetching data:", error);
        } finally {
            setLoading(false);
        }
    };

    const handleSearchChange = (event) => {
        setSearch(event.target.value);
    };

    const handlePageChange = (event, newPage) => {
        setPage(newPage);
    };

    const handleRowsPerPageChange = (event) => {
        setPerPage(parseInt(event.target.value, 10));
        setPage(1); // Reset page to 1 when changing rows per page
    };

    const handleSortColumnChange = (event) => {
        setSortColumn(event.target.value);
    };

    const handleSortOrderChange = (event) => {
        setSortOrder(event.target.value);
    };

    useEffect(() => {
        fetchData();
    }, [search, page, perPage, sortColumn, sortOrder]);

    return (
        <>
            <Box sx={{ display: 'flex', gap: 2, justifyContent: 'end', mt: 2 }}>
                <FormControl>
                    <InputLabel htmlFor="sort-column">Sort Column</InputLabel>
                    <Select
                        value={sortColumn}
                        onChange={handleSortColumnChange}
                        inputProps={{
                            name: "sort-column",
                            id: "sort-column",
                        }}
                    >
                        <MenuItem value="id">ID</MenuItem>
                        <MenuItem value="fname">First Name</MenuItem>
                        <MenuItem value="lname">Last Name</MenuItem>
                        <MenuItem value="username">Username</MenuItem>
                        {/* Add more options for other columns */}
                    </Select>
                </FormControl>

                <FormControl>
                    <InputLabel htmlFor="sort-order">Sort Order</InputLabel>
                    <Select
                        value={sortOrder}
                        onChange={handleSortOrderChange}
                        inputProps={{
                            name: "sort-order",
                            id: "sort-order",
                        }}
                    >
                        <MenuItem value="asc">Ascending</MenuItem>
                        <MenuItem value="desc">Descending</MenuItem>
                    </Select>
                </FormControl>
                <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItem: 'center', gap: 2 }}>
                    <TextField
                        label="Search"
                        value={search}
                        onChange={handleSearchChange}
                        variant="outlined"
                        fullWidth
                    />

                    <Button variant="contained" color="primary" onClick={fetchData}>
                        Search
                    </Button>
                </Box>
            </Box>

            <TableContainer component={Paper} style={{ marginTop: 20 }}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Avatar</TableCell>
                            <TableCell>First Name</TableCell>
                            <TableCell>Last Name</TableCell>
                            <TableCell>Username</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {loading ? (
                            <TableRow>
                                <TableCell colSpan={5}>Loading...</TableCell>
                            </TableRow>
                        ) : (
                            data.map((user) => (
                                <TableRow key={user.id}>
                                    <TableCell>{user.id}</TableCell>
                                    <TableCell>
                                        <img src={user.avatar} alt={`Avatar of ${user.fname}`} style={{ width: 50, height: 50, borderRadius: "50%" }} />
                                    </TableCell>
                                    <TableCell>{user.fname}</TableCell>
                                    <TableCell>{user.lname}</TableCell>
                                    <TableCell>{user.username}</TableCell>
                                </TableRow>
                            ))
                        )}
                    </TableBody>
                </Table>
            </TableContainer>

            <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', mt: 2, gap: 2 }}>
                <Box sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 2 }}>
                    <Typography variant="button">
                        Row per page:
                    </Typography>
                    <Select value={perPage} onChange={handleRowsPerPageChange} label="Records Per Page">
                        <MenuItem value={5}>5</MenuItem>
                        <MenuItem value={10}>10</MenuItem>
                        <MenuItem value={20}>20</MenuItem>
                    </Select>
                </Box>
                <Pagination
                    count={totalPages}
                    page={page}
                    onChange={handlePageChange}
                    color="primary"
                    style={{ marginTop: 20, display: "flex", justifyContent: "center" }}
                />
            </Box>
        </>
    );
};

export default UserSearchPaginationSort;