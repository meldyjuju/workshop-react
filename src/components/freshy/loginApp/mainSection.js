import { Button, Paper, Typography, Box, CircularProgress, Divider, Grid, Tabs, Tab, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, TextField, Select, MenuItem, Pagination, FormControl, InputLabel, TablePagination } from "@mui/material";
import { BorderColorRounded as BorderColorRoundedIcon, LogoutRounded as LogoutRoundedIcon, Circle as CircleIcon, ChevronRight } from "@mui/icons-material";
import { TabPanel } from "../workshop";
import { useEffect, useState } from "react";
import SignupComponent from "./signupComponent";
import UserUpdate from "./userUpdate";
import UserDelete from "./userDelete";
import UserSearchPaginationSort from "./userSearchPaginationSort";
import UserList from "./usetList";
import UserSearch from "./userSearch";
import UserPagination from "./userPagination";
import UserSort from "./userSort";
import UserDetail from "./userDetail";

const schema = [
    {
        tabsName: 'User',
        apiList: [
            { id: 0, name: 'USER LIST', url: '/users' },
            { id: 1, name: 'USER LIST (SEARCH)', url: '/users?search={search}' },
            { id: 2, name: 'USER LIST (PAGINATION)', url: '/users?page={page}&per_page={per_page}' },
            { id: 3, name: 'USER LIST (SORT)', url: '/users?sort_column={sort_column}&sort_order={sort_order}' },
            {
                id: 4,
                name: 'USER LIST (SEARCH + PAGINATION + SORT)',
                url: '/users?search={search}&page={page}&per_page={per_page}&sort_column={sort_column}&sort_order={sort_order}',
            },
            { id: 5, name: 'USER DETAIL', url: '/users/{id}' },
            { id: 6, name: 'USER CREATE', url: '/users/create' },
            { id: 7, name: 'USER UPDATE', url: '/users/update' },
            { id: 8, name: 'USER DELETE', url: '/users/delete' },
        ],
    },
    // Add more tabs as needed
];

export default function main({ user, handleLogout, loading, setSnackbarMessage, setOpenSnackbar }) {
    const [tabValue, setTabValue] = useState(0);
    const [apiValue, setApiValue] = useState(null);
    const [openSidebar, setOpenSidebar] = useState(false);

    const handleTabChange = (event, newValue) => {
        setTabValue(newValue);
    };

    const handleSidebar = (newValue) => {
        if (openSidebar && apiValue === newValue) {
            setOpenSidebar(false);
            setApiValue(null);
        } else {
            setOpenSidebar(true)
            setApiValue(newValue);
        }
    }

    const switchApi = (tab, api) => {
        if (tab === 0 && api === 0) return <UserList />
        if (tab === 0 && api === 1) return <UserSearch />
        if (tab === 0 && api === 2) return <UserPagination />
        if (tab === 0 && api === 3) return <UserSort />
        if (tab === 0 && api === 4) return <UserSearchPaginationSort />
        if (tab === 0 && api === 5) return <UserDetail />
        if (tab === 0 && api === 6) return <SignupComponent setOpenSnackbar={setOpenSnackbar} setSnackbarMessage={setSnackbarMessage}/> 
        if (tab === 0 && api === 7) return <UserUpdate />
        if (tab === 0 && api === 8) return <UserDelete />
    }

    useEffect(() => {
        console.log('tabs:', tabValue);
        console.log('api', apiValue);
    }, [apiValue, tabValue]);

    return (
        <Box sx={{
            display: 'flex',
            flexDirection: { xs: 'column', lg: 'row' },
            gap: 4,
            justifyContent: 'center',
            alignItems: { xs: 'center', md: 'start' },
            width: '100%',
            boxSizing: 'border-box',
            py: 8,
            height: '100%',
            bgcolor: '#EDF3F8',
        }}>
            <Paper
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    p: 6,
                    minWidth: '380px',
                    borderRadius: 2,
                    maxWidth: { xs: '100%', sm: '80%', md: '50%', lg: '30%' }
                }}
            >
                <Box sx={{ display: 'flex', gap: 4 }}>
                    <Box sx={{ position: 'relative' }}>
                        <img src={user?.avatar} alt='Avatar' style={{
                            width: '120px', height: '120px',
                            borderRadius: '50%', outline: '1px solid #62626240',
                            outlineOffset: '5px'
                        }} />
                        <CircleIcon sx={{ position: 'absolute', right: 0, bottom: 6, color: '#23c369', border: '1px solid #62626240', outline: '3px solid white', borderRadius: '50%', outlineOffset: -4, fontSize: '1.75em' }} />
                    </Box>
                    <Box>
                        <Typography variant="button">{`ID: ${user?.id}`}</Typography>
                        <Typography variant="h6" mb={1}>{user?.fname} {user?.lname}</Typography>
                        <Typography variant="body1">{user?.email}</Typography>
                        <Box sx={{ display: 'flex', justifyContent: 'flex-start', gap: 2, mt: 1, transform: 'translateX(-.4em)' }}>
                            <Button
                                variant="text"
                                color="primary"
                                size='small'
                                startIcon={<BorderColorRoundedIcon />}
                            >
                                Edit
                            </Button>
                            <Button
                                variant="text"
                                color="error"
                                size='small'
                                startIcon={!loading && <LogoutRoundedIcon />}
                                onClick={handleLogout}>
                                {loading ? <CircularProgress color="error" size={16} /> : 'Logout'}
                            </Button>
                        </Box>
                    </Box>
                </Box>

                {/* Tabs */}
                <Tabs
                    variant="scrollable"
                    scrollButtons="auto"
                    allowScrollButtonsMobile
                    value={tabValue}
                    onChange={handleTabChange}
                    sx={{ mt: 3 }}>
                    {schema.map((tab, index) => (
                        <Tab key={index} label={tab.tabsName} />
                    ))}
                </Tabs>

                <Divider sx={{ mb: 2 }} />

                {/* Tab Panels */}
                <Box sx={{
                    maxHeight: { xs: '120px', md: '380px', lg: '480px' },
                    overflowY: { xs: 'scroll', lg: 'hidden' },
                }}>
                    {schema.map((tab, index) => (
                        <TabPanel key={index} value={tabValue} index={index}>
                            {tab.apiList.map((api) => (
                                <Button
                                    key={api.id}
                                    onClick={() => handleSidebar(api.id)}
                                    endIcon={<ChevronRight />}
                                    fullWidth
                                    variant={apiValue === api.id ? 'contained' : 'text'}
                                    sx={{
                                        textAlign: 'left',
                                        display: 'flex',
                                        justifyContent: 'space-between',
                                    }}
                                >
                                    {api.name}
                                </Button>
                            ))}
                        </TabPanel>
                    ))}
                </Box>
            </Paper>

            {/* Sidebar */}
            {openSidebar &&
                <Paper
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        p: 6,
                        width: { xs: '90%', md: 'auto' },
                        maxHeight: '690px',
                        borderRadius: 2,
                        transition: 'ease-in-out 1s',
                        animationName: 'slideInFromLeft',
                        animationDuration: '1s'
                    }}>
                    <Typography variant="h6" mb={1}>
                        {schema[tabValue].apiList[apiValue].name}
                    </Typography>
                    <Typography variant="button" sx={{ maxWidth: '40vw' }}>
                        END POINT: {schema[tabValue].apiList[apiValue].url}
                    </Typography>
                    {switchApi(tabValue, apiValue)}
                </Paper>
            }
        </Box>
    );
}