import { Autocomplete, TextField } from "@mui/material";
import { Suspense, useEffect, useState } from "react";

const apiKey = 'df43a3f1-23cd-485e-a31a-2ef59b5eced5';

const CountryAutoComplete = ({ handleCountry, selectedCountry }) => {
    const [countries, setCountries] = useState([]);
    // const myCountries = countries?.map((country) => country.country) || [];

    useEffect(() => {
        const fetchCountries = async () => {
            const apiUrl = `http://api.airvisual.com/v2/countries?key=${apiKey}`;

            try {
                const response = await fetch(apiUrl);
                const result = await response.json();
                console.log(result);
                setCountries(result.data?.map((country) => country.country) || []); // Ensure result.data is an array
            } catch (error) {
                console.error('Error fetching countries:', error);
            }
        };

        fetchCountries();
    }, []);
    

    return (
        <Suspense fallback={<p>Loading Country...</p>}>
            <Autocomplete
                disablePortal
                sx={{my:2}}
                options={countries}
                value={selectedCountry}
                onChange={(event, newValue) => {
                    handleCountry(newValue);
                }}
                renderInput={(params) =>
                    <TextField {...params} label="Select Country" variant="outlined" />
                }
            />
        </Suspense>
    );
};

export default CountryAutoComplete;
