import React, { useState, useEffect, Suspense } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

const apiKey = 'df43a3f1-23cd-485e-a31a-2ef59b5eced5';

const StateAutoComplete = ({ country, handleState, selectedState }) => {
    const [states, setStates] = useState([]);
    // const myStates = states?.map((state) => state.state) || [];

    useEffect(() => {
        const fetchStates = async () => {
            const apiUrl = `http://api.airvisual.com/v2/states?country=${country}&key=${apiKey}`;

            try {
                const response = await fetch(apiUrl);
                const result = await response.json();
                console.log(result);
                setStates(result.data?.map((state) => state.state) || []);
            } catch (error) {
                console.error('Error fetching states:', error);
            }
        };

        if (country) {
            fetchStates();
        }
    }, [country]);

    return (
        <Suspense fallback={<p>Loading State...</p>}>
            <Autocomplete
                disablePortal
                sx={{my:2}}
                options={states}
                value={selectedState}
                onChange={(event, newValue) => {
                    handleState(newValue);
                }}
                renderInput={(params) => (
                    <TextField {...params} label="Select State" variant="outlined" />
                )}
            />
        </Suspense>
    );
};

export default StateAutoComplete;
