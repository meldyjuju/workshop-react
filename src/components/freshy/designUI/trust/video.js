// pages/index.js
import React from 'react';

const TrustVideo = () => {
    return (
        <iframe
            width="100%"
            height="100%"
            style={{minWidth: 720, minHeight: 480}}
            src="https://www.youtube.com/embed/hTIUvfvhMak?si=KIZ0AWN34mUqRfvh"
            title="Interior Preview"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreen>
        </iframe>
    );
};

export default TrustVideo;
