import { ListItem, ListItemIcon, ListItemText } from "@mui/material";

const CheckListItem = ({ startIcon, primary, iconColor, textColor }) => (
  <ListItem disableGutters>
    <ListItemIcon
      sx={{ minWidth: "fit-content", pr: 1, color: iconColor }}
    >
      {startIcon}
    </ListItemIcon>
    <ListItemText primary={primary} sx={{ color: textColor }} />
  </ListItem>
);

export default CheckListItem;