import { Grid, Typography } from "@mui/material";

const InsightItem = ({ title, subtitle }) => (
  <Grid item xs={4} textAlign={{ xs: "left", md: "center" }}>
    <Typography variant="h1" sx={{ fontSize: { xs: '2em', sm: '3em', md: '3.5em' } }}>
      {title}
    </Typography>
    <Typography variant="subtitle1">{subtitle}</Typography>
  </Grid>
);

export default InsightItem;