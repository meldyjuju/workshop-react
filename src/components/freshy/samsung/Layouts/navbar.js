import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import SamsungLogo from 'public/assets/freshy/samsung/Samsung_Orig_Wordmark_BLACK_RGB.png'
import { PersonOutline, SearchOutlined, ShoppingCartOutlined } from '@mui/icons-material';
import Link from 'next/link';
import Image from 'next/image';
import { Badge } from '@mui/material';
import Cart from '../products/cart';

const pages = ['ช็อป', 'โทรศัพท์มือถือ', 'โทรทัศน์ เครื่องเสียง', 'เครื่องใช้ไฟฟ้าภายในบ้าน', 'จอมอนิเตอร์', 'อุปกรณ์เสริม', 'SmartThings'];
const services = ['บริการช่วยเหลือ', 'เพื่อธุรกิจ'];
const buttons = [<SearchOutlined />, <ShoppingCartOutlined />, <PersonOutline />];


function SamsungAppBar() {
    const [anchorElNav, setAnchorElNav] = React.useState(null);
    const [anchorElUser, setAnchorElUser] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <AppBar elevation={1} position="sticky" color='inherit'>
            <Container maxWidth='xl' sx={{ py: 1 }}>
                <Toolbar disableGutters
                    sx={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center'
                    }}>
                    <Button href='./samsung' sx={{
                        my: 2, color: 'black',
                        display: 'block', borderRadius: 0,
                        fontFamily: 'Noto Sans Thai, sans-serif', p: 0,
                        minHeight: 0, minWidth: 0, border: '2px solid #ffffff00',
                        ":hover": { bgcolor: 'white' },
                        ":focus": { border: '2px dotted #000000' }
                    }}>
                        <Image src={SamsungLogo} style={{ width: 120, height: 32 }} alt='Logo' />
                    </Button>

                    <Box sx={{ display: { xs: 'none', lg: 'flex' }, gap: .5 }}>
                        {pages.map((page) => (
                            <Button
                                key={page}
                                onClick={handleCloseNavMenu}
                                sx={{
                                    my: 2, color: 'black',
                                    display: 'block', borderRadius: 100,
                                    fontFamily: 'Noto Sans Thai, sans-serif', py: .5, minHeight: 0,
                                    fontWeight: 'medium', fontSize: 14, px: 1, minWidth: 0, border: '2px solid #ffffff00',
                                    ":hover": { bgcolor: 'black', color: 'white' },
                                    ":focus": { border: '2px dotted #ffffff', bgcolor: 'black', color: 'white' }
                                }}>
                                {page}
                            </Button>
                        ))}
                    </Box>
                    <Box sx={{ display: { xs: 'none', lg: 'flex' }, gap: .5 }}>
                        {services.map((service) => (
                            <Button
                                key={service}
                                onClick={handleCloseNavMenu}
                                sx={{
                                    my: 2, color: 'black',
                                    display: 'block', borderRadius: 100,
                                    fontFamily: 'Noto Sans Thai, sans-serif', py: .5, minHeight: 0, border: '2px solid #ffffff00',
                                    ":hover": { bgcolor: 'black', color: 'white' },
                                    ":focus": { border: '2px dotted #ffffff', bgcolor: 'black', color: 'white' }
                                }}>
                                {service}
                            </Button>
                        ))}
                    </Box>
                    <Box sx={{ display: 'inline' }}>
                        <IconButton
                            size="large"
                            onClick={handleCloseNavMenu}
                            color="inherit">
                            <SearchOutlined />
                        </IconButton>
                        <Cart/>
                        <IconButton
                            size="large"
                            onClick={handleCloseNavMenu}
                            color="inherit">
                            <PersonOutline />
                        </IconButton>

                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit"
                            sx={{ display: { lg: 'none' } }}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: { xs: 'block', lg: 'none' },
                            }}
                        >
                            {pages.map((page) => (
                                <MenuItem key={page} onClick={handleCloseNavMenu}>
                                    <Typography textAlign="center" sx={{ fontFamily: 'Noto Sans Thai, sans-serif' }}>{page}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
}
export default SamsungAppBar;
