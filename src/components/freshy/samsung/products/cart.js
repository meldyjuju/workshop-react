import { CloseOutlined, ShoppingCartOutlined } from "@mui/icons-material";
import {
    Badge,
    Box,
    Drawer,
    Fade,
    IconButton,
    List,
    ListItem,
    ListItemText,
    Paper,
    Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import ProductInCart from "./productInCart";

const Cart = () => {
    const [cartItem, setCartItem] = useState([]);
    const [isDrawerOpen, setIsDrawerOpen] = useState(false);

    useEffect(() => {
        // Retrieve the cart from localStorage
        const cart = JSON.parse(localStorage.getItem("Cart"));

        // Update the state with the total quantity
        setCartItem(cart);
    }, []); // Run this effect only once when the component mounts

    const handleDrawerOpen = () => {
        setIsDrawerOpen(true);
    };

    const handleDrawerClose = () => {
        setIsDrawerOpen(false);
    };

    return (
        <>
            <IconButton size="large" color="inherit">
                <Badge
                    badgeContent={cartItem.length}
                    onClick={handleDrawerOpen}
                    color="primary"
                    overlap="rectangular"
                    sx={{ ".MuiBadge-colorPrimary": { bgcolor: "black" } }}
                >
                    <ShoppingCartOutlined />
                </Badge>
            </IconButton>
            <Drawer
                anchor="right"
                open={isDrawerOpen}
                onClose={handleDrawerClose}
                sx={{ fontFamily: "Noto Sans Thai, sans-serif" }}
                transitionDuration={500}
            >
                <Paper
                    square
                    sx={{
                        px: 4,
                        py: 2.75,
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                        gap: 4,
                        borderBottom: '1px solid #70707050'
                    }}
                >
                    <Typography variant="h5" fontFamily="inherit" fontWeight="semibold" fontSize={22}>
                        ตะกร้าของคุณ ({cartItem.length} ชิ้น)
                    </Typography>
                    <IconButton
                        onClick={handleDrawerClose}
                        sx={{
                            color: "black",
                            bgcolor: "white",
                            my: 0.3,
                            display: "block",
                            borderRadius: 50,
                            px: 0.5,
                            fontSize: 16,
                            fontFamily: "Noto Sans Thai, sans-serif",
                            py: 0.5,
                            minHeight: 0,
                            border: "2px solid #ffffff00",
                            ":hover": { border: "2px dotted #000000", bgcolor: "white", color: "black" },
                            ":focus": { border: "2px dotted #000000", bgcolor: "white", color: "black" },
                        }}
                    >
                        <CloseOutlined color="inherit" />
                    </IconButton>
                </Paper>
                <Box sx={{ height: "100%", overflowY: "Scroll" }}>
                    <Box sx={{ px: 2 }}>
                        <List>
                            {cartItem.map((item) => (
                                <ProductInCart key={item.id} model={item} />
                            ))}
                        </List>
                    </Box>
                </Box>
                <Paper
                    square
                    sx={{
                        px: 4,
                        py: 4,
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "space-between",
                        alignItems: "center",
                        gap: 1,
                        borderTop: '1px solid #70707050'
                    }}
                >
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center",
                            gap: 4,
                            width: "100%",
                        }}
                    >
                        <Typography variant="h5" fontFamily="inherit" fontWeight="semibold" fontSize={22}>
                            ยอดรวม (รวม VAT)
                        </Typography>
                        <Typography variant="h5" fontWeight="semibold" fontSize={22}>
                            ฿999,999.00
                        </Typography>
                    </Box>
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center",
                            gap: 4,
                            width: "100%",
                            color: '#242424'
                        }}
                    >
                        <Typography variant="subtitle1" fontFamily="inherit" fontWeight="semibold">
                            ส่วนลดทั้งหมด
                        </Typography>
                        <Typography variant="subtitle1" fontWeight="semibold" color='black'>
                            ฿20
                        </Typography>
                    </Box>
                </Paper>
            </Drawer>
        </>
    );
};

export default Cart;
