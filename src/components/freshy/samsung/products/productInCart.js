import { Circle } from '@mui/icons-material'
import { Avatar, Box, ListItem, ListItemAvatar, ListItemText, Typography } from '@mui/material'
import Image from 'next/image'
import React from 'react'
import QuantityController from './quantityController'
import { formatCurrency, getRAMValue, getStorageValue } from './productCard'

const ProductInCart = ({ model, key }) => {
    return (
        <ListItem key={key} sx={{ py: 4, borderBottom: '1px solid #70707020', flexDirection: 'row', alignItems: 'start', gap: 3 }}>
            <ListItemAvatar sx={{ bgcolor: '#f9f9f9', p: 1.5, borderRadius: 1 }}>
                <Avatar sx={{ width: 80, height: 80 }} variant="square">
                    <Image src={model?.img} width={200} height={200} alt={model?.name} />
                </Avatar>
            </ListItemAvatar>
            <Box sx={{ display: 'flex', gap: 3, flexDirection: 'column', justifyContent: 'center' }}>
                <ListItemText>
                    <Box>
                        <Typography variant='overline' fontWeight='regular' sx={{ fontFamily: 'Noto Sans Thai, sans-serif' }}>
                            ID: {model?.id}
                        </Typography>
                        <Typography variant='h6' sx={{ fontFamily: 'Noto Sans Thai, sans-serif' }}>
                            {model?.name}
                        </Typography>
                        <Typography variant='subtitle1' sx={{ fontFamily: 'Noto Sans Thai, sans-serif', fontWeight: 'regular', textTransform: 'capitalize', mt: 1 }}>
                            สี: {model?.color?.name} <Circle sx={{ color: model?.color?.hex, fontSize: 16 }} />, แรม: {getRAMValue(model?.description)}, หน่วยความจำ: {getStorageValue(model?.description)}
                        </Typography>
                    </Box>
                </ListItemText>
                <Box sx={{ display: 'flex', gap: 3, alignItems: 'center', justifyContent: 'space-between' }}>
                    <QuantityController model={model} />
                    <Typography variant='h5' sx={{fontSize: 22}}>
                        ฿{formatCurrency(model?.price)}.00
                    </Typography>
                </Box>
            </Box>
        </ListItem>
    )
}

export default ProductInCart