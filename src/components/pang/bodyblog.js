import React from "react";
import Container from "@mui/material/Container";

const Blog = () => {
  return (
    <Container
      maxWidth="full"
      style={{
        backgroundColor: "#00796b",
        height: "25em",
        display: "flex",
        flexDirection: "column",
        alignItems: "left",
      }}
    >
      <div
        style={{
          fontSize: "50px",
          fontWeight: "bold",
          color: "#FFFFFF",
          paddingTop: "150px",
          paddingLeft: "20px",
        }}
      >
        BLOG
      </div>
      <div style={{ fontSize: "15px", color: "#FFFFFF", textAlign: "left", whiteSpace: "nowrap", paddingLeft: "20px", paddingTop:"20px" }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
      </div>
      <div style={{ fontSize: "15px", color: "#FFFFFF", textAlign: "left", whiteSpace: "nowrap" , paddingLeft: "20px" }}>
        Ut elit tellus, luctus nec
        ullamcorper mattis, pulvinar dapibus leo.
      </div>
    </Container>
  );
};

export default Blog;
