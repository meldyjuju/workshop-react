import React from "react";
import Container from "@mui/material/Container";

const Body = () => {
  return (
    <Container maxWidth="full" style={{ backgroundColor: "#616161", height: "50em", display: "flex", flexDirection: "column",  alignItems: "center" }}>
      <div
        style={{
          fontSize: "130px",
          fontWeight: "bold",
          color: "#FFFFFF",
          textAlign: "center",
          paddingTop: "200px",
          
        }}
      >
       ERROR 404
      </div>
      <div
        style={{
          fontSize: "30px",
          fontWeight: "bold",
          color: "#FFFFFF",
          textAlign: "center",
          letterSpacing: 2,
        }}
      >
        PAGE NOT FOUND, PLEASE GO BACK
      </div>
      <button
        style={{
          fontSize: "16px",
          fontWeight: "semibold",
          color: "#FFFFFF",
          backgroundColor: "#757575",
          textAlign: "center",
          width: "200px",
          height: "80px",
          marginTop: "30px",
        }}
      >
        ORDER NOW
      </button>
    </Container>
  );
};

export default Body;
