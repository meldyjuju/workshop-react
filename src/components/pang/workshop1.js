import React, { useState } from "react";
import Link from "next/link"; 

const TwoBoxesComponent = () => {
 
  return (
    <div style={{ display: "flex", justifyContent: "center", gap: "20px", padding: "20px" }}>

  {/* /////workshop ที่ 1  */}
      <div
        style={{
          width: "250px",
          height: "65px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
      >
        <div>
        <Link href="/workShop/meldyjuju/cal">WORKSHOP1</Link>

        </div>
        <p style={{ color: "#000000" }}></p>
      </div>

      {/* ////workshop ที่ 2*/}

      <div
        style={{
          width: "250px",
          height: "65px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
      >
        <div>
        <Link href="/workShop/meldyjuju/ui">WORKSHOP2</Link>

        </div>
        <p style={{ color: "#000000" }}></p>
      </div>

        {/* ////workshop ที่ 3*/}

      <div
        style={{
          width: "250px",
          height: "65px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
      >
        <div>
        <Link href="/workShop/meldyjuju/login">WORKSHOP3</Link>

        </div>

        <p style={{ color: "#000000" }}></p>
      </div>

          {/* ////workshop ที่ 4*/}
      <div
        style={{
          width: "250px",
          height: "65px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
      >
        <div>
        <Link href="/workShop/meldyjuju/home">WORKSHOP4</Link>

        </div>

        <p style={{ color: "#000000" }}></p>
      </div>


      {/* ////workshop ที่ 5*/}
      <div
        style={{
          width: "250px",
          height: "65px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
        
      >
        <div>
        <Link href="/workShop/meldyjuju/workapi">WORKSHOP5</Link>
        </div>

        <p style={{ color: "#000000" }}></p>
      </div>

    </div>
  );
};

export default TwoBoxesComponent;
