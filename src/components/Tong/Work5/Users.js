// pages/login.js
import { useState } from "react";
import {
  Container,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";

const Users = () => {
  const [users, setUsers] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const [usersData, setUserData] = useState(null);
  const [userIdToDelete, setUserIdToDelete] = useState("");
  const [userIdupdate, setUserIdUpdate] = useState("");
  const [deleteStatus, setDeleteStatus] = useState(null);

  const handleFetchUser = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users`, {
        method: "GET",
        headers: {},
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();

      console.log("User Data:", usersData);

      // อัปเดต state users
      setUsers(usersData);

      // บันทึกข้อมูลผู้ใช้ลงใน Local Storage
      localStorage.setItem("user", JSON.stringify(usersData));

      //   console.log("status:", usersData.status);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleSearch = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/users?search=${searchTerm}`,
        {
          method: "GET",
          headers: {},
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch search results");
      }

      const searchResults = await response.json();

      console.log("Search Results:", searchResults);

      // อัปเดต state users ด้วยผลลัพธ์การค้นหา
      setUsers(searchResults);

      //   console.log("status:", searchResults.status);
    } catch (error) {
      console.error("Error fetching search results:", error.message);
    }
  };

  const handlePerPage = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users?page=1&per_page=10`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleSort = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/users?sort_column=id&sort_order=desc`,
        {
          method: "GET",
          headers: {
            // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
          },
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleSPS = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc`,
        {
          method: "GET",
          headers: {
            // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
          },
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleDetail = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users/1`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleCreateUser = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users/create`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fname: "tong",
          lname: "Chat",
          username: "cat.chat@melivecode.com",
          password: "1234",
          email: "cat.chat@melivecofde.com",
          avatar: "https://www.melivecode.com/users/cat.png",
        }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

      const createdUser = await response.json();
      console.log("User created:", createdUser);
    } catch (error) {
      console.error("Error creating user:", error.message);
    }
  };

  const handleDeleteUser = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users/delete`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userIdToDelete,
        }),
      });
      //   console.log(response);

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

      const result = await response.json();
      setDeleteStatus(result);
      console.log(result);
    } catch (error) {
      console.error("Error deleting user:", error.message);
    }
  };

  const handleUpdateUser = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/users/update`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userIdupdate,
          lname: "SENSO",
        }),
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(errorData.message);
      }

      const createdUser = await response.json();
      console.log("User Update !!:", createdUser);
    } catch (error) {
      console.error("Error Upadte user:", error.message);
    }
  };

  return (
    <Container maxWidth="md">
      <Typography variant="h4" align="center" gutterBottom>
        API List
      </Typography>

      <Container maxWidth="fixed" sx={{ backgroundColor: "", color: "" }}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <TextField
                label="Search"
                variant="outlined"
                value={searchTerm}
                onChange={(e) => setSearchTerm(e.target.value)}
                sx={{ mr: "10px" }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={handleSearch}
                sx={{ width: "100%" }}
              >
                Search api
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleFetchUser}
              sx={{ width: "100%" }}
            >
              User List api
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant="contained"
              color="primary"
              onClick={handlePerPage}
              sx={{ width: "100%" }}
            >
              User List Pagination api
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant="contained" color="primary" onClick={handleSort} sx={{ width: "100%" }}>
              User List Sort api
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant="contained" color="primary" onClick={handleSPS} sx={{ width: "100%" }}>
              User List Search+Pagination+Sort api
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleDetail}
              sx={{ width: "100%" }}
            >
              User Detail api
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleCreateUser}
              sx={{ width: "100%" }}
            >
              User Create api
            </Button>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <TextField
                type="number"
                value={userIdupdate}
                onChange={(e) => setUserIdUpdate(e.target.value)}
                label="ID"
                variant="outlined"
                sx={{ mr: "10px" }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={handleUpdateUser}
                sx={{ width: "100%" }}
              >
                User Update api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <TextField
                type="number"
                value={userIdToDelete}
                onChange={(e) => setUserIdToDelete(e.target.value)}
                label="ID"
                variant="outlined"
                sx={{ mr: "10px" }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={handleDeleteUser}
                sx={{ width: "100%" }}
              >
                User Delete api
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Container>

      <br />
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Avatar</TableCell>
              <TableCell>First Name</TableCell>
              <TableCell>Last Name</TableCell>
              <TableCell>Username</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {users.map((user) => (
              <TableRow key={user.id}>
                <TableCell>{user.id}</TableCell>
                <TableCell>
                  <img
                    src={user.avatar}
                    alt={`Avatar ${user.id}`}
                    style={{ width: "50px", height: "50px", borderRadius: "50%" }}
                  />
                </TableCell>
                <TableCell>{user.fname}</TableCell>
                <TableCell>{user.lname}</TableCell>
                <TableCell>{user.username}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default Users;
