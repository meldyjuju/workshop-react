// pages/login.js
import { useState } from "react";
import {
  Container,
  Typography,
} from "@mui/material";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";

const Attractions = () => {
  const [userData, setUserData] = useState(null);
  const [searchItem, setSearchItem] = useState("");

  const handleAttractions = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/attractions`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleALS = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/attractions?search=${searchItem}`,
        {
          method: "GET",
          headers: {},
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleALP = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/attractions?page=1&per_page=10`,
        {
          method: "GET",
          headers: {
            // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
          },
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleAttractSort = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/attractions?sort_column=id&sort_order=desc`,
        {
          method: "GET",
          headers: {
            // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
          },
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleAttractSPS = async () => {
    try {
      const response = await fetch(
        `${process.env.NEXT_PUBLIC_APP_URL}/attractions?search=island&page=1&per_page=10&sort_column=id&sort_order=desc`,
        {
          method: "GET",
          headers: {
            // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
          },
        }
      );

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleLanguage = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/th/attractions`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleDetail = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/attractions/1`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleDetailLanguage = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/attractions/1`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  const handleStaticPaths = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/attractions/static_paths`, {
        method: "GET",
        headers: {
          // สามารถเพิ่ม headers อื่น ๆ ตามความต้องการ
        },
      });

      if (!response.ok) {
        throw new Error("Failed to fetch user data");
      }

      const usersData = await response.json();
      setUserData(usersData);
      console.log(usersData);
    } catch (error) {
      console.error("Error fetching user data:", error.message);
    }
  };

  return (
    <Container maxWidth="md">
      <Typography variant="h4" align="center" gutterBottom>
        Attractions
      </Typography>

      <Container maxWidth="fixed" sx={{ backgroundColor: "", color: "" }}>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAttractions}
                sx={{ width: "100%" }}
              >
                Attractions api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <TextField
                label="Search"
                variant="outlined"
                value={searchItem}
                onChange={(e) => setSearchItem(e.target.value)}
                sx={{ mr: "10px" }}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={handleALS}
                sx={{ width: "100%" }}
              >
                Attractions List SEARCH api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleALP}
                sx={{ width: "100%" }}
              >
                Attractions List Pagination api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAttractSort}
                sx={{ width: "100%" }}
              >
                Attractions List Sort api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleAttractSPS}
                sx={{ width: "100%" }}
              >
                Attractions Search+Pagination+Sort api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleLanguage}
                sx={{ width: "100%" }}
              >
                Attractions Language api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleDetail}
                sx={{ width: "100%" }}
              >
                Attractions Detail api
              </Button>
            </Grid>
          </Grid>
          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleDetailLanguage}
                sx={{ width: "100%" }}
              >
                Attractions Detail + Language api
              </Button>
            </Grid>
          </Grid>

          <Grid item xs={4}>
            <Grid sx={{ md: "5", display: "flex" }}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleStaticPaths}
                sx={{ width: "100%" }}
              >
                Attractions Static Paths api
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Container>
  );
};

export default Attractions;
