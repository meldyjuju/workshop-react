import * as React from "react";
import Card from "@mui/material/Card";
import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

function Calculator() {
  const [input, setInput] = React.useState();

  // ฟังก์ชันนี้ถูกเรียกเมื่อคลิกปุ่มตัวเลขหรือตัวดำเนินการ มันอัปเดตสถานะ input โดยการต่อตัวเลขที่คลิกกับ input ที่มีอยู่
  const handleClick = (value) => {
    if (value === "%" && input !== "") {
      // คำนวณเครื่องหมาย %
      const percentage = parseFloat(input) / 100;
      setInput(percentage.toString());
    } else {
      setInput((prevInput) => prevInput + value);
    }
  };

  // ฟังก์ชัน handleCalculate ถูกเรียกเมื่อคลิกปุ่ม "=" มันใช้ฟังก์ชัน eval เพื่อคิดเลขที่ป้อนและอัปเดตสถานะ
  //  input ด้วยผลลัพธ์ หากเกิดข้อผิดพลาดในระหว่างการประเมิน จะตั้งค่าสถานะ input เป็น "Error".
  const handleCalculate = () => {
    try {
      let result = eval(input);
      // ถ้าผลลัพธ์เป็นตัวเลขและไม่มีเครื่องหมาย %
      if (!isNaN(result) && input.indexOf("%") === -1) {
        result = result.toString();
      }
      setInput(result);
    } catch (error) {
      setInput("Error");
    }
  };

  const handleClear = () => {
    setInput("");
  };

  return (
    <>
      <Container>
        <Card>
          <Typography variant="h5" align="right" sx={{ margin: 6 }}>
            {input}
          </Typography>

          <Grid container spacing={3}>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("1")}>1</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("2")}>2</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("3")}>3</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("+")}>+</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("4")}>4</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("5")}>5</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("6")}>6</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("-")}>-</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("7")}>7</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("8")}>8</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("9")}>9</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("0")}>0</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={handleClear}>C</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("*")}>*</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("/")}>/</Button>
            </Grid>
            <Grid item xs={3}>
              <Button variant="outlined" onClick={handleCalculate}>
                =
              </Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick(".")}>.</Button>
            </Grid>
            <Grid item xs={3}>
              <Button onClick={() => handleClick("%")}>%</Button>
            </Grid>
          </Grid>
        </Card>
      </Container>
    </>
  );
}

export default Calculator;
