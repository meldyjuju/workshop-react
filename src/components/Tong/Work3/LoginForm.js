// pages/login.js
import { useState, useEffect } from "react";
import { TextField, Button, Container, Typography } from "@mui/material";
import { useRouter } from "next/router";

const LoginForm = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  // const [userData, setUserData] = useState("");
  const router = useRouter();

  const handleLogin = async () => {
    try {
      // ตรวจสอบว่ามีข้อมูลผู้ใช้และรหัสผ่านหรือไม่
      if (!username || !password) {
        throw new Error("Please enter both username and password");
      }

      const response = await fetch(`${process.env.NEXT_PUBLIC_APP_URL}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });

      if (!response.ok) {
        throw new Error("Login failed");
      }

      const data = await response.json();

      // Log info
      console.log("status:", data.status);
      console.log("message:", data.message);
      console.log("Name:", data.user.fname, data.user.lname);

      // Save JWT to localStorage
      localStorage.setItem("jwt", data.accessToken);

      // หาก Login สำเร็จ และ status เป็น ok ให้ทำการ redirect ไปยัง /workShop/tong/proFile
      if (data.status === "ok") {
        router.push("/workShop/tong/ProfilePage");
      }
    } catch (error) {
      // Log error
      console.error("error:", error.message);
    }
  };

  return (
    <Container maxWidth="sm">
      <Typography variant="h4" align="center" gutterBottom>
        Login
      </Typography>
      <form>
        <TextField
          label="Username"
          variant="outlined"
          fullWidth
          margin="normal"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <TextField
          label="Password"
          variant="outlined"
          fullWidth
          margin="normal"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button variant="contained" color="primary" fullWidth onClick={handleLogin}>
          Login
        </Button>
      </form>
    </Container>
  );
};

export default LoginForm;
