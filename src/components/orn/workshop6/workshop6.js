import { Typography, Box, Avatar } from '@mui/material';
import { GetData } from '../Fetch';
import NavbarWorkshop6 from './navbarWorkshop6'
import Grid from '@mui/material/Grid';
import * as React from 'react';
import Image from 'next/image';
import Paper from '@mui/material/Paper';
import { styled } from '@mui/system';
import Link from 'next/link';



const StyledImage = styled('img')({
    objectFit: 'contain',
    width: '200px',
    height: '200px'
});
const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    display: 'flex',
    flexDirection: 'column',
}));

const Data = () => {
    const [product, setProduct] = React.useState(null);
    React.useEffect(() => {
        GetData(`${process.env.NEXT_PUBLIC_PRODUCT}`)
            .then(data => {
                setProduct(data);
            })
            .catch(error => {
                console.error('Error fetching product data:', error);
            });
    }, []);

    React.useEffect(() => {
        console.log(product);
    }, [product]);
   
   

    return (
            product != null && product.products.map((item) => (
                <Grid item key={item.id} xs={6} sm={4} md={3} lg={2}>
                    <Link href={`/workShop/onrain/workshop6/ProductDetail?id=${item.id}`}>
                    <Item sx={{ width: '200px', height: '320px', border: '1px solid grey', p: 2 }}>
                        <StyledImage src={item.thumbnail} alt='img' />
                        <Typography sx={{color:'#27374D'}} variant='h6' component='div'>{item.title}</Typography>
                        <Typography sx={{color:'#27374D'}} component='p'>price: ${item.price}</Typography>
                        <Typography sx={{fontSize:'0.8em',color:'#526D82'}} component='div'>rating: {item.rating}</Typography>
                    </Item>
                    </Link>
                </Grid>
            ))
       
    );
}

const Workshop6Compo = () => {
    

    return (
        <>
            <NavbarWorkshop6 />
            <Typography variant='h5' component='div' sx={{ display: 'flex', justifyContent: 'center' }} m={6}>
                PRODUCTS
            </Typography>
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={3}>
                    <Grid item xs>

                    </Grid>
                    <Grid item xs={10}>
                        <Box sx={{ flexGrow: 1 }}>
                            <Grid container spacing={1}>
                                <Grid container item spacing={3}>
                                    <Data />
                                </Grid>
                              
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid item xs>

                    </Grid>
                </Grid>
            </Box>
        </>
    )
}

export default Workshop6Compo;