import Navbar from "./navbar";
import * as React from 'react';
import {
    Button, Box, Typography, TextField, Dialog, DialogActions, DialogContent, DialogContentText,
    DialogTitle
} from "@mui/material";
import swal from "sweetalert";
import { GetData, Crud } from "./Fetch";



const createUser = {
    "fname": "Cat",
    "lname": "Chat",
    "username": "cat.chat@melivecode.com",
    "password": "12345",
    "email": "cat.chat@melivecode.com",
    "avatar": "https://www.melivecode.com/users/cat.png"
}

const updateUser = {
    "id": 15,
    "lname": "Gato"
}

const deleteUser = {
    "id": 15
}

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Workshop5Compo = () => {

    const [userListData, setUserListData] = React.useState(null);
    const [userPage, setUserPage] = React.useState(null)
    const [userDetail, setUserDetail] = React.useState(null)
    const [userData, setUserData] = React.useState(null);

    const [userCreate, setUserCreate] = React.useState({});
    const [userUpdate, setUserUpdate] = React.useState({});
    const [openDelete, setOpenDelete] = React.useState(false);
    const [idToDelete, setIdToDelete] = React.useState()

    const fetchData = (api) => {
        GetData(api).then(data => {
            setUserPage(null)
            setUserListData(data);
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }

    const userList = (api) => {
        GetData(api).then(data => {
            setUserPage(null)
            setUserData(data);
            setUserDetail(null)
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }


    const crudData = (api, body, method) => {
        Crud(api, body, method).then(data => {
            setUserPage(null)
            setUserListData(data)
            setUserDetail(null)
            setUserData(null);
        }).catch(error => {
            console.error('Error fetching user data:', error);
        });
    }

    const dataPage = (api) => {
        GetData(api).then(data => {
            setUserListData(data);
            setUserPage(data)
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });

    }

    const dataSearchPageSort = () => {
        GetData(`${process.env.NEXT_PUBLIC_SEARCH_PAGE_SORT}`).then(data => {
            setUserListData(data);
            setUserPage(data)
            setUserDetail(null)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });

    }

    const dataDetail = () => {
        GetData(`${process.env.NEXT_PUBLIC_USER_DETAIL}`).then(data => {
            setUserPage(null)
            setUserListData(data);
            setUserDetail(data)
            setUserData(null);
        })
            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }

    React.useEffect(() => {
        console.log(userListData);

        // log ข้อมูล user
        if (userData != null) {
            console.log(userData);
        }
        // log ข้อมูล user pagination
        if (userPage != null) {
            if (Array.isArray(userPage.data)) {
                const hasUserName = userPage.data.some(user => user.name !== undefined);
                if (hasUserName) {
                    const names = userPage.data.map((user) => user.name);
                    console.log(names);
                } else if (!hasUserName) {
                    const names = userPage.data.map((user) => user.fname + ' ' + user.lname);
                    console.log(names);
                }

            }
        }
        if (userDetail != null) {
            const names = userDetail.user.fname + ' ' + userDetail.user.lname
            console.log(names);
        }

    }, [userListData, userPage, userDetail, userData]);


    const handleInputChange = (key, value) => {
        setUserCreate(prevState => ({
            ...prevState,
            [key]: value,
            avatar: 'https://www.melivecode.com/users/12.png'
        }));
    };

    const handleSubmit = async e => {
        e.preventDefault();

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_CREATE}`, userCreate, 'POST');
            console.log(res);
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }

        } catch (error) {
            console.error("Error:", error);
        }

    }
    const [open, setOpen] = React.useState(false);

    const handleClickOpen = (data) => {
        setOpen(true)
        setUserUpdate(prevState => ({
            ...prevState,
            'id': data.id,
            'fname': data.fname,
            'lname': data.lname,
            'username': data.username,
            avatar: 'https://www.melivecode.com/users/12.png'
        }));
        console.log(userUpdate)
    };
    const handleUpdateChange = (key, value, id) => {
        setUserUpdate(prevState => ({
            ...prevState,
            [key]: value,
        }));
    };

    const handleSubmitUpdate = async e => {
        e.preventDefault();

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_UPDATE}`, userUpdate, 'PUT');
            console.log(res);
            setOpen(false)
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                }).then((value) => {
                    window.location.reload();
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }
            
        } catch (error) {
            console.error("Error:", error);
        }

    }


    const handleClose = () => setOpen(false);

    const handleOpenDelete = (id) => {
        setOpenDelete(true);
        setIdToDelete(id)
    };

    const handleCloseDelete = () => {
        setOpenDelete(false);
    };
    
    const handleDelete = async (id) => {

        try {
            const res = await Crud(`${process.env.NEXT_PUBLIC_USER_DELETE}`, { "id": id }, 'DELETE');
            console.log(res);
            setOpenDelete(false);
            if (res.status == 'ok') {
                swal("Success", res.message, "success", {
                    buttons: false,
                    timer: 2000,
                }).then((value) => {
                    window.location.reload();
                })
            } else {
                swal("Failed", res.message, "error", {
                    buttons: false,
                    timer: 2000,
                })
            }

        } catch (error) {
            console.error("Error:", error);
        }

    }

    return (
        <>
            <Navbar />
            <Box>
                <Typography m={1} variant="h6" component="div">USER</Typography>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => userList(`${process.env.NEXT_PUBLIC_USER_LIST}`)}>user list</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_USER_SEARCH}`)}>user search</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => dataPage(`${process.env.NEXT_PUBLIC_PER_PAGE}`)}>page</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_SORT}`)}>user sort</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => dataSearchPageSort()}>user search+page+sort</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => dataDetail()}>user detail</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_CREATE}`, createUser, 'POST')}>user create</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_UPDATE}`, updateUser, 'PUT')}>user update</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => crudData(`${process.env.NEXT_PUBLIC_USER_DELETE}`, deleteUser, 'DELETE')}>user delete</Button>
            </Box>
            <Box>
                <Typography variant="h6" m={1} component="div">ATTRACTIONS</Typography>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LIST}`)}>attractions list</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_SEARCH}`)}>attractions search</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => dataPage(`${process.env.NEXT_PUBLIC_ATTRACTION_PAGE}`)}>attractions page</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LANGUAGE}`)}>attractions language</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_DETAIL}`)}>attractions detail</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_LANGUAGE_DETAIL}`)}>attractions language detail</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_ATTRACTION_STATIC_PATH}`)}>attractions static paths</Button>
            </Box>
            <Box>
                <Typography variant="h6" m={1} component="div">PET SALES</Typography>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_PET_SALES_LIST}`)}>pet sales summary</Button>
                <Button variant="outlined" sx={{ m: 1 }} onClick={() => fetchData(`${process.env.NEXT_PUBLIC_PET_SALES_DAILY_LIST}`)}>pet sales daily</Button>
            </Box>
            <Box sx={{ display: 'flex', width: '100%' }}>
                <Box
                    component="form"
                    noValidate
                    onSubmit={handleSubmit}
                    sx={{ width: '30%', p: 2 }}
                >
                    <Typography variant="h6" component="div" sx={{ textAlign: 'center' }}>create user</Typography>
                    <TextField fullWidth sx={{ mt: 2 }} id="outlined-basic" label="firstname" variant="outlined"
                        onChange={e => handleInputChange('fname', e.target.value)}
                    />
                    <TextField fullWidth sx={{ mt: 2 }} id="outlined-basic" label="lastname" variant="outlined"
                        onChange={e => handleInputChange('lname', e.target.value)}
                    />
                    <TextField fullWidth sx={{ mt: 2 }} id="outlined-basic" label="username" variant="outlined"
                        onChange={e => handleInputChange('username', e.target.value)}
                    />
                    <TextField fullWidth sx={{ mt: 2 }} id="outlined-basic" label="email" variant="outlined"
                        onChange={e => handleInputChange('email', e.target.value)}
                    />

                    <Button fullWidth sx={{ mt: 2 }} type="submit" variant="outlined">submit</Button>
                </Box>
                <Box sx={{ width: '100%', p: 2 }}>
                    <Button variant="outlined" sx={{ m: 1 }} onClick={() => userList(`${process.env.NEXT_PUBLIC_USER_LIST}`)}>user list ที่ลบ/แก้ไขได้</Button>
                    
                    {userData != null && (
                        <ol>
                            {userData.map((item) => (
                                item.id > 12 && (
                                    <li key={item.id}>{item.fname}  {item.lname}
                                        <Button onClick={() => handleClickOpen(item)}>แก้ไข</Button>
                                        <Button onClick={() => handleOpenDelete(item.id)}>ลบ</Button>
                                        {/* onClick={()=>handleDelete(item.id)} */}
                                    </li>

                                )
                            ))}
                        </ol>
                    )}
                    <Dialog
                        open={openDelete}
                        onClose={handleCloseDelete}
                        aria-labelledby="alert-delete-title"
                        aria-describedby="alert-delete-description"
                    >
                        <DialogTitle id="alert-delete-title">
                            {"ลบ"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                ต้องการลบหรือไม่
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleCloseDelete}>ยกเลิก</Button>
                            <Button onClick={() => handleDelete(idToDelete)} autoFocus>
                                ลบ
                            </Button>
                        </DialogActions>
                    </Dialog>

                    <Dialog open={open} onClose={handleClose}>
                        <DialogTitle>update user</DialogTitle>
                        <DialogContent>

                            <TextField
                                autoFocus
                                margin="dense"
                                id="firstname"
                                label="firstname"
                                type="text"
                                fullWidth
                                variant="standard"
                                value={userUpdate.fname}
                                onChange={e => handleUpdateChange('fname', e.target.value)}
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                id="lastname"
                                label="lastname"
                                type="text"
                                fullWidth
                                variant="standard"
                                value={userUpdate.lname}
                                onChange={e => handleUpdateChange('lname', e.target.value)}
                            />
                            <TextField
                                autoFocus
                                margin="dense"
                                id="username"
                                label="username"
                                type="text"
                                fullWidth
                                variant="standard"
                                value={userUpdate.username}
                                onChange={e => handleUpdateChange('username', e.target.value)}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleClose}>cancel</Button>
                            <Button onClick={handleSubmitUpdate}>update</Button>
                        </DialogActions>
                    </Dialog>

                </Box>

            </Box>

        </>
    )
}
export default Workshop5Compo;
