import * as React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import { Facebook, Instagram, Twitter } from "@mui/icons-material";
import { Box } from "@mui/material";
import Divider from "@mui/material/Divider";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import EmailIcon from "@mui/icons-material/Email";
import FmdGoodIcon from "@mui/icons-material/FmdGood";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import Stack from "@mui/material/Stack";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

export default function Footer() {
  return (
    <Box
      component="footer"
      sx={{
        backgroundColor: "#000",
        color: "#fff",
        p: 6,
      }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={5}>
          <Grid item xs={12} sm={4} md={4} sx={{ marginLeft: { xs: 0, sm: "-10em" } }}>
            <Typography
              variant="h6"
              sx={{ color: "white", fontWeight: "bold", fontSize: "2em" }}
              gutterBottom
            >
              INFORMATION
            </Typography>

            <Typography variant="body2" color="white">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec
              ullamcorper mattis, pulvinar dapibus leo.
            </Typography>
            <Typography
              variant="body2"
              color="white"
              sx={{ color: "white", fontWeight: "bold", fontSize: "2em" }}
            >
              <Link href="https://www.facebook.com/" color="inherit">
                <Facebook />
              </Link>
              <Link href="https://www.instagram.com/" color="inherit" sx={{ pl: 1, pr: 1 }}>
                <Instagram />
              </Link>
              <Link href="https://www.twitter.com/" color="inherit">
                <Twitter />
              </Link>
              <Link href="https://www.twitter.com/" color="inherit">
                <LinkedInIcon></LinkedInIcon>
              </Link>
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4} sx={{ marginLeft: { xs: 0, sm: "10em" } }}>
            <Typography
              variant="h6"
              sx={{ color: "white", fontWeight: "bold", fontSize: "2em" }}
              gutterBottom
            >
              NAVIGATION
            </Typography>
            <Typography variant="body2" color="white">
              <ChevronRightIcon></ChevronRightIcon>Homepage
            </Typography>
            <Typography variant="body2" color="white">
              <ChevronRightIcon></ChevronRightIcon>About Us
            </Typography>
            <Typography variant="body2" color="white">
              <ChevronRightIcon></ChevronRightIcon> Services
            </Typography>
            <Typography variant="body2" color="white">
              <ChevronRightIcon></ChevronRightIcon>Project
            </Typography>
          </Grid>
          <Grid item xs={12} sm={4} md={4}>
            <Typography
              variant="h6"
              sx={{ color: "white", fontWeight: "bold", fontSize: "2em" }}
              gutterBottom
            >
              CONTACT US
            </Typography>
            <Typography variant="body2" color="white" sx={{ marginTop: { xs: "1em", sm: "0" } }}>
              <FmdGoodIcon></FmdGoodIcon> Lumbung Hidup East Java
            </Typography>
            <Typography variant="body2" color="white">
              <EmailIcon></EmailIcon> Hello@Homco.com
            </Typography>
            <Stack direction="row" alignItems="center">
              <TextField
                hiddenLabel
                id="filled-hidden-label-normal"
                defaultValue="Email"
                variant="filled"
                sx={{ width: { xs: "100%", sm: "80%" }, borderRadius: 0, mt: "1em" }}
              />
            
            </Stack>
            <Button
                variant="outlined"
                sx={{ borderRadius: 0, color: "white", backgroundColor: "gray", mt: "1em" }}
              >
                SUBSCRIBE
              </Button>
          </Grid>
        </Grid>

        <Box mt={5}>
          <Divider />
          <Typography variant="body2" color="white" align="center" marginTop={5}>
            {"Copyright © "}
            <Link color="inherit" href="https://your-website.com/">
              Your Website
            </Link>{" "}
            {new Date().getFullYear()}
            {"."}
          </Typography>
        </Box>
      </Container>
    </Box>
  );
}
